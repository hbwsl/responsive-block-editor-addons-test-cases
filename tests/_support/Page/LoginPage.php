<?php
namespace Page;

class LoginPage
{
    public $userLoginField='#user_login';
    public $userPasswordField='#user_pass';
    public $loginBtn='#wp-submit';
    public $wordpressProfile='//*[@id="wp-admin-bar-my-account"]/a/span';
    public $logOutLink='//*[@id="wp-admin-bar-logout"]/a';
    public $wordPressBtn='#wp-admin-bar-site-name > a';
    public $wordpressBtn2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[1]/div/a';
    public function userLogin($I)
    {
        $I->amOnPage('/wp-admin');
        $I->wait(2);
        $I->fillField($this->userLoginField,'root');
        $I->fillField($this->userPasswordField,'admin123');
        $I->click($this->loginBtn);
        $I->wait(2);
        $I->click($this->wordPressBtn);

    }

    public function userLogout($I)
    {
        $I->wait(4);
        $I->waitForElement($this->wordpressBtn2,20);
        $I->click($this->wordpressBtn2);
        $I->waitForElement($this->wordpressProfile,20);
        $I->moveMouseOver($this->wordpressProfile);
        $I->click($this->logOutLink);
        $I->wait(2);
    }



}