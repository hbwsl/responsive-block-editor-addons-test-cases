<?php

namespace Page;
class BlockEditorAdOns
{

    //pages selectors
    public $dividerPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-5 > a';
    public $dividerClass='.responsive-block-editor-addons-divider-content';

    public $googleMapPage='/html/body/header/div[1]/div[2]/nav/ul/li[2]';
    public $googleMapClass='.place-card place-card-large';

    public $videoPopPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-53 > a';
    public $videoPopClass='.responsive-block-editor-addons-video-popup__wrapper';

    public $pricingListPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-78 > a';
    public $pricingListClass='.responsive-block-editior-addons-pricing-list-outer-wrap';

    public $pricingTablePage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-133 > a';
    public $pricingTableButton1='/html/body/main/article/div[1]/div/div/div[2]/div[1]/a';

    public $expandPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-196 > a';
    public $expandPageSomeShortTextClass='.responsive-block-editor-addons-expand-less-text';

    public $teamPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-240 > a';
    public $teamPageClass='//*[@id="post-240"]/div[1]/div';

    public $advanceHeadingPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-358 > a';
    public $advanceHeadingClass='/html/body/main/article/header/div/h1';

    public $accordionPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-441 > a';
    public $accordionClass='.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap';

    public $blockquotePage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-443 > a';
    public $blockquoteClass='.responsive-block-editor-addons-block-blockquote-item';

    public $advanceColumnPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-763 > a';
    public $advanceColumnPageClass='.responsive-columns-wrap.responsive-block-editor-addons-block-columns.responsive-columns__gap-default.responsive-columns__stack-mobile.responsive-columns__content-width-theme.overlay-type-color.linear';

    public $advanceColumnParentPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-1248 > a';
    public $advanceColumnParentPageClass='.responsive-block-editor-addons-advanced-column.alignundefined';

    public $infoBlockPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-886 > a';
    public $infoBlockPageClass='.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team';

    public $sectionPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-1906 > a';
    public $sectionPageClass='div.post-inner.thin > div > div';

    public $countUpPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-2020 > a';
    public $countUpPageClass='div.post-inner.thin > div > div > div';

    public $flipBoxPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-2260 > a';
    public $flipBoxPageClass='.wp-block-responsive-block-editor-addons-flip-box';

    public $calltoactionPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-629 > a';
    public $calltoactionClass='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div';

    public $shapeDividerPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-1042 > a';
    public $shapeDividerPageClass='.wp-block-responsive-block-editor-addons-shape-divider.alignfull';

    public $imageSliderPage='#site-header > div.header-inner.section-inner > div.header-navigation-wrapper > nav > ul > li.page_item.page-item-325 > a';
    public $imageSliderPageClass='.has-carousel.has-carousel-lrg.flickity-enabled.is-draggable';

    public $editPageLink='#wp-admin-bar-edit > a';
    public $cancelBtnForWelcomeToBlockEditor='body > div:nth-child(8) > div > div > div > div > div > div > div > div.components-modal__header > button';
    public $updateBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__header > div > div.edit-post-header__settings > button.components-button.editor-post-publish-button.editor-post-publish-button__button.is-primary';


    // Divider Block Selectors
    public $dividerBlockSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[1]/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/div/div[1]/div';
    public $dividerBlockVerticalMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div/div/span/div/div/input';
    public $dividerBlockHeight='#inspector-input-control-1';
    public $divoderBlockWidth='#inspector-input-control-2';


    // Google-Map Block Selector
    public $googleMapSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[1]/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/div/div[1]/div';
    public $googleMapZoom='#inspector-input-control-2';
    public $googleMapWidth='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(3) > div > input[type=number]';

    // Videop Pop-Up Select
    public $videoPopUpSelect='.responsive-block-editor-addons-video-popup__wrapper';
    public $videoPopUpOptionsOpenBtn='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $videoPopUpContainerOpenBtn='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $videoPopUpPlayButtonOpenBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(4) > h2 > button';
    public $videoPopUpBorderOpenBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/h2/button';
    public $videoPopUpAdvancedOpenBtn='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div/h2/button';
    public $videoPopUpContainerHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div/span/div/div/input';
    public $videoPopUpContainerWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/span/div/div/input';

    public $videoPopUpBackgroundOptionsOpenBtn='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';

    public $videoPopUpPlayButtonWithCircle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[2]';
    public $videoPopUpOutlinePlayButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[3]';
    public $videoPopUpVideoPlayBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[4]';
    public $videoPopUpplayButtonSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $videoPopUpplayButtonOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/span/div/div/input';

    public $videoPopUpBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[2]';
    public $videoPopUpBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[3]';
    public $videoPopUpBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[4]';
    public $videoPopUpBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[5]';
    public $videoPopUpBorderStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[6]';
    public $videoPopUpBorderStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[7]';
    public $videoPopUpBorderStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[8]';
    public $videoPopUpBorderStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[9]';

    public $videoPopUpBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/span/div/div/input';
    public $videoPopUpBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/span/div/div/input';


    // Pricing-List
    public $pricingListSelect='.responsive-block-editior-addons-pricing-list-outer-wrap';

    public $pricingListGeneral='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(1) > h2 > button';
    public $pricingListNumberOfItems='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $pricingListColumns='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/span/div/div/input';

    public $pricingListSeparatorBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(3) > h2 > button';
    public $pricingListSeparatorStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[1]';
    public $pricingListSeparatorStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[2]';
    public $pricingListSeparatorStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[3]';
    public $pricingListSeparatorStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[4]';
    public $pricingListSeparatorStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[5]';
    public $pricingListSeparatorStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[6]';
    public $pricingListSeparatorStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[7]';
    public $pricingListSeparatorStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[8]';
    public $pricingListSeparatorStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[9]';

    public $pricingListSeparatorWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $pricingListSeparatorThickness='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';

    public $pricingListSpacingBtn='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/h2/button';
    public $pricingListRowGap='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/span/div/div/input';
    public $pricingListColumnGap='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/span/div/div/input';
    public $pricingListTitleBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/span/div/div/input';
    public $pricingListTopPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[4]/div/span/div/div/input';
    public $pricingListBottomPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[5]/div/span/div/div/input';
    public $pricingListLeftPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[6]/div/span/div/div/input';
    public $pricingListRightPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[7]/div/span/div/div/input';

    public $pricingListColorSettingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $pricingListTitleColorSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(3) > div:nth-child(1) > button';
    public $pricingListContentColorSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(5) > div:nth-child(3) > button';
    public $pricingListPriceColorSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(7) > div:nth-child(4) > button';
    public $pricingListTitleColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(3) > div:nth-child(2) > button';
    public $pricingListContentColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(5) > div:nth-child(2) > button';
    public $pricingListPriceColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(7) > div:nth-child(2) > button';


    // Pricing-Table
    public $pricingTableSelect='.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined';

    public $pricingTableGeneralBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(1) > h2 > button';
    public $pricingTableButtonSettingBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(3) > h2 > button';
    public $pricingTableColumnBackgroundBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(4) > h2 > button';
    public $pricingTableTypographyBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(5) > h2 > button';
    public $pricingTableBorderBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(6) > h2 > button';
    public $pricingTableColorSettingsBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings > h2 > button';
    public $pricingTableSpacingBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(8) > h2 > button';

    public $pricingTableNumberOfTables='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $pricingTableGutterSmall='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[2]';
    public $pricingTableGutterMedium='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[3]';
    public $pricingTableGutterLarge='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[4]';
    public $pricingTableGutterHuge='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[5]';

    public $pricingTableAlignmentLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[1]';
    public $pricingTableAlignmentCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[2]';
    public $pricingTableAlignmentRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[3]';

    public $pricingTableTitleToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/span/input';
    public $pricingTablePricePrefixToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[5]/div/span/input';
    public $pricingTablePriceToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[6]/div/span/input';
    public $pricingTablePriceSuffixToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div/span/input';
    public $pricingTableSubPriceToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[8]/div/span/input';
    public $pricingTableFeaturesToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[9]/div/span/input';
    public $pricingTableButtonToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[10]/div/span/input';


    public $pricingTableTitleSelector='.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus';
    public $pricingTablePriceToggleSelector='.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__amount.keep-placeholder-on-focus';
    public $pricingTableSubPriceSelector='.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price.keep-placeholder-on-focus';
    public $pricingTableButtonSelector='.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__button.keep-placeholder-on-focus';
    public $pricingTablePrefixToggleSelector='.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__currency.keep-placeholder-on-focus';
    public $pricingTablePriceSuffixSelector='rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__price_suffix.keep-placeholder-on-focus';
    public $pricingTableFeaturesSelector='div > div.wp-block-responsive-block-editor-addons-pricing-table__inner.has-columns.has-2-columns.has-responsive-columns.has-small-gutter > div:nth-child(1) > ul > li > span';

    public $pricingTableTextColorRedSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(2) > div > fieldset > div > div:nth-child(1) > button';
    public $pricingTableTitleColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(3) > div > fieldset > div > div:nth-child(2) > button';
    public $pricingTablePricePrefixColorBrownSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(4) > div > fieldset > div > div:nth-child(3) > button';
    public $pricingTablePriceColorRedSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(5) > div > fieldset > div > div:nth-child(1) > button';
    public $pricingTablePriceSuffixColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(6) > div > fieldset > div > div:nth-child(2) > button';
    public $pricingTableSubPriceColorBrownSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(7) > div > fieldset > div > div:nth-child(3) > button';
    public $pricingTableFeaturesColorRedSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(8) > div > fieldset > div > div:nth-child(1) > button';

    public $pricingTableTextColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(2) > div > fieldset > div > div:nth-child(2) > button';
    public $pricingTablePricePrefixColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(4) > div > fieldset > div > div:nth-child(2) > button';
    public $pricingTablePriceColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(5) > div > fieldset > div > div:nth-child(2) > button';
    public $pricingTableSubPriceColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(7) > div > fieldset > div > div:nth-child(2) > button';
    public $pricingTableFeaturesColorBlackSelect='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(8) > div > fieldset > div > div:nth-child(2) > button';

    public $pricingTableTextColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(2) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';
    public $pricingTableTitleColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(3) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';
    public $pricingTablePricePrefixColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(4) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';
    public $pricingTablePriceColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(5) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';
    public $pricingTablePriceSuffixColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(6) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';
    public $pricingTableSubPriceColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(7) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';
    public $pricingTableFeaturesColorclear='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings.is-opened > div:nth-child(8) > div > fieldset > div > div.components-circular-option-picker__custom-clear-wrapper > button';

    public $pricingTableTitleSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[8]/div[3]/div/span/div/div/input';
    public $pricingTablePriceSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[8]/div[4]/div/span/div/div/input';
    public $pricingTableSubPriceSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[8]/div[5]/div/span/div/div/input';
    public $pricingTableButtonSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[8]/div[6]/div/span/div/div/input';
    public $pricingTableFeaturesSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[8]/div[7]/div/span/div/div/input';

    public $priceTableSolidBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[2]';
    public $priceTableDottedBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[3]';
    public $priceTableDashedBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[4]';
    public $priceTableDoubleBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[5]';
    public $priceTableGrooveBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[6]';
    public $priceTableInsetBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[7]';
    public $priceTableOutsetBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[8]';
    public $priceTableRidgeBorder='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/select/option[9]';

    public $priceTableBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div/span/div/div/input';
    public $priceTableBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[3]/div/span/div/div/input';
    public $priceTableBorderColorRed='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[4]/div[1]/button';

    public $priceTableBorderWidthResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div/span/span[2]/button';
    public $priceTableBorderRadiusResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[3]/div/span/span[2]/button';
    public $priceTableBorderColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[4]/div[6]/button';

    public $priceTableColorBackGroundType='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div/div/select/option[2]';
    public $priceTableBackgroundColorGrey='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div[4]/button';
    public $priceTableBackgroundColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div[6]/button';
    public $priceTableColorOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/div/div/input';

    public $priceTableGradientBackGroundType='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div/div/select/option[3]';
    public $priceTableGradientColor_1GreySelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div[4]/button';
    public $priceTableGradientColor_2RedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div[1]/button';
    public $priceTableGradientColor_1Location='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/span/div/div/input';
    public $priceTableGradientColor_2Location='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/span/div/div/input';
    public $priceTableGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/span/div/div/input';
    public $priceTableGradientOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[7]/div/span/div/div/input';

    public $priceTableImageBackGroundType='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div/div/select/option[4]';

    public $priceTableHorizontalPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $priceTableVerticalPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';



    // Expand
    public $expandSelectClass='.responsive-block-editor-addons-block-expand';
    public $expandGenaralBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(1) > h2 > button';
    public $expandGeneralTitleToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div/div/span/input';
    public $expandTitleBlockClassSelector='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-title.keep-placeholder-on-focus';

    public $expandTypographyBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(2) > h2 > button';
    public $expandTitleTypography='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/h2/button';

    public $expandSpacingBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(3) > h2 > button';
    public $expandSpacingTitleMarginBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/div/div/input';
    public $expandSpacingTitleMarginResetBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/span[2]/button';
    public $expandSpacingTextMarginBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $expandSpacingTextMarginResetBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/span[2]/button';
    public $expandSpacingLinkMarginBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $expandSpacingLinkMarginResetBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';

    public $expandColorSettingBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings > h2 > button > span.block-editor-panel-color-gradient-settings__panel-title';
    public $expandColorSettingTextColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/fieldset/div/div[1]/button';
    public $expandColorSettingLinkColorBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/fieldset/div/div[3]/button';
    public $expandColorSettingTitleColorBlackSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[3]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/fieldset/div/div[3]/button';


    // Team
    public $teamSelectClass='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div';

    public $teamGeneralBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(1) > h2 > button';
    public $teamGeneralNumberOfTeamMemberBoxes='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';

    public $teamGeneralGutterNoSelected='#inspector-select-control-8 > option:nth-child(1)';
    public $teamGeneralGutterSmallSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[2]';
    public $teamGeneralGutterSmallClass='.wp-block-responsive-block-editor-addons-team-wrapper.has-columns.has-2-columns.has-responsive-columns.has-small-gutter';
    public $teamGeneralGutterMediumSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[3]';
    public $teamGeneralGutterMediumClass='.wp-block-responsive-block-editor-addons-team-wrapper.has-columns.has-2-columns.has-responsive-columns.has-medium-gutter';
    public $teamGeneralGutterLargeSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[4]';
    public $teamGeneralGutterLargeClass='.wp-block-responsive-block-editor-addons-team-wrapper.has-columns.has-2-columns.has-responsive-columns.has-large-gutter';
    public $teamGeneralGutterHugeSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[5]';
    public $teamGeneralGutterHugeClass='.wp-block-responsive-block-editor-addons-team-wrapper.has-columns.has-2-columns.has-responsive-columns.has-huge-gutter';

    public $teamGeneralBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/span/div/div/input';

    public $teamGeneralBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[5]/div/span/div/div/input';

    public $teamGeneralPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[6]/div/span/div/div/input';

    public $teamGeneralColumnBackgroundBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div.components-panel__body.block-editor-panel-color-gradient-settings > h2 > button';
    public $teamGeneralLightGrayBackgroundColorSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[1]/div/fieldset/div/div[4]/button';
    public $teamGeneralLightGrayBackgroundColorResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[1]/div/fieldset/div/div[6]/button';

    public $teamGeneralBackGroundColorOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[2]/div/span/div/div/input';
    public $teamGeneralBackGroundColorOpacityResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[2]/div/span/span[2]/button';
    public $teamGeneralBackGroundPositionLeftTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[1]';
    public $teamGeneralBackGroundPositionLeftCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[2]';
    public $teamGeneralBackGroundPositionLeftBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[3]';
    public $teamGeneralBackGroundPositionRightTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[4]';
    public $teamGeneralBackGroundPositionRightCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[5]';
    public $teamGeneralBackGroundPositionRightBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[6]';
    public $teamGeneralBackGroundPositionCenterTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[7]';
    public $teamGeneralBackGroundPositionCenterCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[8]';
    public $teamGeneralBackGroundPositionCenterBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[5]/div/select/option[9]';

    public $teamGeneralBackGroundRepeatInitial='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[1]';
    public $teamGeneralBackGroundRepeatRepeat='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[2]';
    public $teamGeneralBackGroundRepeatNoRepeat='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[3]';
    public $teamGeneralBackGroundRepeatRound='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[4]';
    public $teamGeneralBackGroundRepeatInherit='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[5]';
    public $teamGeneralBackGroundRepeatSpace='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[6]';
    public $teamGeneralBackGroundRepeatRepeatY='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[7]';
    public $teamGeneralBackGroundRepeatRepeatX='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[6]/div/select/option[8]';

    public $teamGeneralBackGroundSizeAttachmentScroll='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[7]/div/select/option[2]';
    public $teamGeneralBackGroundSizeAttachmentFixed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[7]/div/select/option[1]';

    public $teamGeneralBackGroundSizeInitial='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[8]/div/select/option[1]';
    public $teamGeneralBackGroundSizeCover='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[8]/div/select/option[2]';
    public $teamGeneralBackGroundSizeContain='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[8]/div/select/option[3]';
    public $teamGeneralBackGroundSizeAuto='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[8]/div/select/option[4]';
    public $teamGeneralBackGroundSizeInherit='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div[8]/div/select/option[5]';


    public $teamTypographyBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(3) > h2 > button';


    public $teamSpacingBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(4) > h2 > button';
    public $teamTitleBottomSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/span/div/div/input';
    public $teamDesignationBottomSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $teamDescriptionBottomSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/div/div/input';
    public $teamInterSocialIconSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/span/div/div/input';
    public $teamImageMarginTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/span/div/div/input';
    public $teamImageMarginBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/span/div/div/input';


    public $teamSocialBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(5) > h2 > button';
    public $teamSocialTwitterToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[1]/div/span/input';
    public $teamSocialFacebookToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[2]/div/span/input';
    public $teamSocialLinkedinToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[3]/div/span/input';
    public $teamSocialInstagramToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[4]/div/span/input';
    public $teamSocialEmailToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[5]/div/span/input';
    public $teamSocialYoutubeToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[6]/div/span/input';
    public $teamSocialPintrestToggleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[7]/div/span/input';
    public $teamSocialIconColorBlackSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/div/fieldset/div/div[2]/button';

    public $teamSocialIconSizeNormal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[1]';
    public $teamSocialIconSizeSmall='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[2]';
    public $teamSocialIconSizeMedium='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[3]';
    public $teamSocialIconSizeLarge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[4]';
    public $teamSocialIconSizeTiny='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[5]';

    public $teamSocialIconRedColorSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/div/fieldset/div/div[1]/button';
    public $teamSocialIconColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/div/fieldset/div/div[6]/button';

    public $teamSocialTwitterClass='.dashicons.dashicons-twitter';
    public $teamSocialFacebookClass='.dashicons.dashicons-facebook';
    public $teamSocialLinkedinClass='.dashicons.dashicons-linkedin';
    public $teamSocialInstagramClass='.dashicons.dashicons-instagram';
    public $teamSocialEmailClass='.dashicons.dashicons-email';
    public $teamSocialYoutubeClass='.dashicons.dashicons-youtube';

    public $teamSocialPintrestClass='.dashicons.dashicons-pinterest';

    public $teamColorSettings='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.block-editor-panel-color-gradient-settings > h2 > button > span.block-editor-panel-color-gradient-settings__panel-title';
    public $teamColorSettingsBorderColorRedSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/fieldset/div/div[1]/button';
    public $teamColorSettingsBorderColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div/fieldset/div/div[6]/button';
    public $teamColorSettingsTitleColorBrownSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div/fieldset/div/div[3]/button';
    public $teamColorSettingsTitleColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div/fieldset/div/div[6]/button';
    public $teamColorSettingsDesignationColorLightBrownSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[3]/div/fieldset/div/div[4]/button';
    public $teamColorSettingsDesignationColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[3]/div/fieldset/div/div[6]/button';
    public $teamColorSettingsDescriptionColorBlackSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[4]/div/fieldset/div/div[2]/button';
    public $teamColorSettingsDescriptionColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[4]/div/fieldset/div/div[6]/button';


    // Image Slider
    public $imageSliderSelectClass='.block-editor-block-list__layout.is-root-container';
    public $imageSliderImageCarausalBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(1) > h2 > button';
    public $imageSliderImageCarausalSizeSmall='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div/button[1]';
    public $imageSliderImageCarausalSizeMedium='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div/button[2]';
    public $imageSliderImageCarausalSizeLarge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div/button[3]';
    public $imageSliderImageCarausalSizeExtraLarge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div/button[4]';
    public $imageSliderImageCarausalSizeResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/button';
    public $imageSliderImageCarausalGutter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div[2]/div/div/span/div/div/input';
    public $imageSliderImageCarausalHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/input';


    public $imageSliderArrowBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(2) > h2 > button';
    public $imageSliderArrowColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/fieldset/div/div[1]/button';
    public $imageSliderArrowColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/fieldset/div/div[6]/button';
    public $imageSliderArrowBackGroundColorBlackSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div/fieldset/div/div[2]/button';
    public $imageSliderArrowBackGroundClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div/fieldset/div/div[6]/button';
    public $imageSliderArrowBackGroundOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/span/div/div/input';
    public $imageSliderArrowBackGroundRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[4]/div/span/div/div/input';

    public $imageSliderBorderBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(3) > h2 > button';
    public $imageSliderBorderColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/fieldset/div/div[1]/button';
    public $imageSliderBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/fieldset/div/div[6]/button';
    public $imageSliderBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/select/option[1]';
    public $imageSliderBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/select/option[2]';
    public $imageSliderBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/select/option[3]';
    public $imageSliderBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/select/option[4]';

    public $imageSliderSliderSettingsBtn='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div:nth-child(4) > h2 > button';
    public $imageSliderSliderSettingsAutoplayBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/span/input';
    public $imageSliderSliderSettingsDragableBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/input';
    public $imageSliderSliderSettingsFressScrollBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/input';
    public $imageSliderSliderSettingsArrowNavigationBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/span/input';
    public $imageSliderSliderSettingsDotNavigationBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/span/input';
    public $imageSliderSliderSettingsAlignCellsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/span/input';


    // Advance-Heading
    public $advanceHeadingSelectClass='.block-editor-block-list__block.wp-block.wp-block';

    public $advanceHeadingGeneralButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $advanceHeadingGeneralHeadingToggleButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/input';
    public $advanceHeadingGeneralHeadingClass='.rich-text.block-editor-rich-text__editable.responsive-heading-title-text';
    public $advanceHeadingGeneralSubHeadingToggleButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/span/input';
    public $advanceHeadingGeneralSubHeadingClass='.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text';
    public $advanceHeadingGeneralSeparatorButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/span/input';
    public $advanceHeadingGeneralSeparatorClass='.responsive-heading-seperator';

    public $advanceHeadingGeneralHeadingTagH1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[1]';
    public $advanceHeadingGeneralHeadingTagH1Class='.responsive-block-editor-addons-block-advanced-heading > h1';

    public $advanceHeadingGeneralHeadingTagH2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[2]';
    public $advanceHeadingGeneralHeadingTagH2Class='.responsive-block-editor-addons-block-advanced-heading > h2';

    public $advanceHeadingGeneralHeadingTagH3='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[3]';
    public $advanceHeadingGeneralHeadingTagH3Class='.responsive-block-editor-addons-block-advanced-heading > h3';

    public $advanceHeadingGeneralHeadingTagH4='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[4]';
    public $advanceHeadingGeneralHeadingTagH4Class='.responsive-block-editor-addons-block-advanced-heading > h4';

    public $advanceHeadingGeneralHeadingTagH5='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[5]';
    public $advanceHeadingGeneralHeadingTagH5Class='.responsive-block-editor-addons-block-advanced-heading > h5';

    public $advanceHeadingGeneralHeadingTagH6='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[6]';
    public $advanceHeadingGeneralHeadingTagH6Class='.responsive-block-editor-addons-block-advanced-heading > h6';

    public $advanceHeadingSpacingButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $advanceHeadingBottomSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/span/div/div/input';
    public $advanceHeadingBottomSpacingClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/span/span[2]/button';
    public $advanceHeadingSepratorBottomSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $advanceHeadingSepratorBottomSpacingClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/span[2]/button';

    public $advanceHeadingSeparatorButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $advanceHeadingSeparatorStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[2]';
    public $advanceHeadingSeparatorStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[3]';
    public $advanceHeadingSeparatorStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[4]';
    public $advanceHeadingSeparatorStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[5]';
    public $advanceHeadingSeparatorStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[6]';
    public $advanceHeadingSeparatorStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[7]';
    public $advanceHeadingSeparatorStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[8]';
    public $advanceHeadingSeparatorStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[9]';

    public $advanceHeadingSeparatorThickness='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $advanceHeadingSeparatorThicknessReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/span[2]/button';
    public $advanceHeadingSeparatorWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $advanceHeadingSeparatorWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';
    public $advanceHeadingSeparatorColorRed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div[1]/button';
    public $advanceHeadingSeparatorColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div[6]/button';

    public $advanceHeadingTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';

    public $advanceHeadingHeadingTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/h2/button';
    public $advanceHeadingHeadingTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $advanceHeadingHeadingTypographyFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[1]';
    public $advanceHeadingHeadingTypographyFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[2]';
    public $advanceHeadingHeadingTypographyFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[3]';
    public $advanceHeadingHeadingTypographyFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[4]';
    public $advanceHeadingHeadingTypographyFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[5]';
    public $advanceHeadingHeadingTypographyFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[6]';
    public $advanceHeadingHeadingTypographyFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[7]';
    public $advanceHeadingHeadingTypographyFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[8]';
    public $advanceHeadingHeadingTypographyFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[9]';

    public $advanceHeadingHeadingTypographyLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[3]/div/span/div/div/input';
    public $advanceHeadingHeadingTypographyLeterSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[4]/div/span/div/div/input';
    public $advanceHeadingHeadingTypographyRedColorSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[5]/div[1]/button';
    public $advanceHeadingHeadingTypographyColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[5]/div[6]/button';

    public $advanceHeadingSubHeadingTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/h2/button';
    public $advanceHeadingSubHeadingTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/div/span/div/div/input';
    public $advanceHeadingSubHeadingTypographyFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[1]';
    public $advanceHeadingSubHeadingTypographyFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[2]';
    public $advanceHeadingSubHeadingTypographyFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[3]';
    public $advanceHeadingSubHeadingTypographyFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[4]';
    public $advanceHeadingSubHeadingTypographyFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[5]';
    public $advanceHeadingSubHeadingTypographyFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[6]';
    public $advanceHeadingSubHeadingTypographyFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[7]';
    public $advanceHeadingSubHeadingTypographyFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[8]';
    public $advanceHeadingSubHeadingTypographyFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[9]';

    public $advanceHeadingSubHeadingTypographyLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[3]/div/span/div/div/input';
    public $advanceHeadingSubHeadingTypographyLeterSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[4]/div/span/div/div/input';
    public $advanceHeadingSubHeadingTypographyBrownColorSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[5]/div[3]/button';
    public $advanceHeadingSubHeadingTypographyColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[5]/div[6]/button';


    //Accordion
    public $accordionSelectClass='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-title';
    public $accordionStyleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/h2/button';
    public $accordionBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div/select/option[1]';
    public $accordionBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div/select/option[2]';
    public $accordionBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div/select/option[3]';
    public $accordionBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div/select/option[4]';
    public $accordionBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div/select/option[5]';
    public $accordionBorderThickness='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[2]/div/span/div/div/input';
    public $accordionBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[3]/div/span/div/div/input';
    public $accordionBorderColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[4]/div[1]/button';
    public $accordionBorderColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[4]/div[6]/button';

    public $accordionSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $accordionSpacingRowGap='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/div/div/input';
    public $accordionSpacingVerticalMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $accordionSpacingHorizontalMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';

    //Blockquote
    public $blockquoteSelectClass='.responsive-block-editor-addons-block-blockquote-item';
    public $blockquoteGeneralBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $blockquoteGeneralFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $blockquoteGeneralFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[1]';
    public $blockquoteGeneralFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[2]';
    public $blockquoteGeneralFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[3]';
    public $blockquoteGeneralFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[4]';
    public $blockquoteGeneralFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[5]';
    public $blockquoteGeneralFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[6]';
    public $blockquoteGeneralFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[7]';
    public $blockquoteGeneralFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[8]';
    public $blockquoteGeneralFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[9]';
    public $blockquoteGeneralLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/span/div/div/input';
    public $blockquoteGeneralAlignmentLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[1]';
    public $blockquoteGeneralAlignmentCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[2]';
    public $blockquoteGeneralAlignmentRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/select/option[3]';

    public $blockquoteColorSettingsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/h2/button';
    public $blockquoteTextColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div/div/fieldset/div/div[1]/button';
    public $blockquoteTextColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div/div/fieldset/div/div[6]/button';

    public $blockquoteSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/h2/button';
    public $blockquoteTopPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/span/div/div/input';
    public $blockquoteBottomPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/span/div/div/input';
    public $blockquoteLeftPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/span/div/div/input';
    public $blockquoteRightPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[4]/div/span/div/div/input';

    public $blockquoteBorderSettingsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $blockquoteBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[1]';
    public $blockquoteBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[2]';
    public $blockquoteBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[3]';
    public $blockquoteBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[4]';
    public $blockquoteBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[5]';
    public $blockquoteBorderStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[6]';
    public $blockquoteBorderStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[7]';
    public $blockquoteBorderStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[8]';
    public $blockquoteBorderStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[9]';

    public $blockquoteBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $blockquoteBorderWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/span[2]/button';

    public $blockquoteBorderColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div[1]/button';
    public $blockquoteBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div[6]/button';

    public $blockquoteBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/span/div/div/input';
    public $blockquoteBorderRadiusResetButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/span/span[2]/button';

    public $blockquoteBackgroundBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $blockquoteBackgroundTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[1]';
    public $blockquoteBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[2]';
    public $blockquoteBackgroundColorBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[4]/button';
    public $blockquoteBackgroundColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[6]/button';

    public $blockquoteBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[3]';
    public $blockquoteBackgroundTypeImage='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[4]';
    public $blockquoteBackgroundTypeVideo='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[5]';
    public $blockquoteBackgroundOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $blockquoteBackgroundOpacityReset='//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';



    // CallToAction
    public $callToActionSelectClass='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[1]/div[4]/div[2]/div/div/div[2]/div[2]/div[2]/div/div[1]/div';
    public $callToActionGeneralButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $callToActionGeneralBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $callToActionGeneralBorderRadiusResetButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/span[2]/button';
    public $callToActionGeneralBoxShadowIcon='.dashicon.dashicons-admin-tools';
    public $callToActionGeneralBoxShadowColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[1]/div[1]/button';
    public $callToActionGeneralBoxShadowColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[1]/div[6]/button';
    public $callToActionGeneralBoxShadowHorizontal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[2]/div/span/div/div/input';
    public $callToActionGeneralBoxShadowHorizontalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[2]/div/span/span[2]/button';
    public $callToActionGeneralBoxShadowVertical='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[3]/div/span/div/div/input';
    public $callToActionGeneralBoxShadowVerticalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[3]/div/span/span[2]/button';
    public $callToActionGeneralBoxShadowBlur='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[4]/div/span/div/div/input';
    public $callToActionGeneralBoxShadowBlurReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[4]/div/span/span[2]/button';
    public $callToActionGeneralBoxShadowSpread='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[5]/div/span/div/div/input';
    public $callToActionGeneralBoxShadowSpreadReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[5]/div/span/span[2]/button';
    public $callToActionGeneralBoxShadowPositionInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[6]/div/select/option[1]';
    public $callToActionGeneralBoxShadowPositionOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/div[6]/div/select/option[2]';
    public $callToActionGeneralTextAlignButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[1]/div[4]/div[1]/div/div/div/div/div[2]/div/div/div[4]/div[2]/button';
    public $callToActionGeneralTextAlignLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[2]/div/div/div/div/div/div/div/button[1]';
    public $callToActionGeneralTextAlignCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[2]/div/div/div/div/div/div/div/button[2]';
    public $callToActionGeneralTextAlignRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[2]/div/div/div/div/div/div/div/button[3]';

    public $callToActionTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $callToActionTitleTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/h2/button';
    public $callToActionTitleTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/div/div/span/div/div/input';
    public $callToActionFontSizeFrontEndSelector='.responsive-block-editor-addons-cta-title.responsive-block-editor-addons-font-size-25';
    public $callToActionFontSizeOnPageSelector='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-title.responsive-block-editor-addons-font-size-25.keep-placeholder-on-focus';
    public $callToActionTitleFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[1]';
    public $callToActionTitleFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[2]';
    public $callToActionTitleFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[3]';
    public $callToActionTitleFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[4]';
    public $callToActionTitleFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[5]';
    public $callToActionTitleFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[6]';
    public $callToActionTitleFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[7]';
    public $callToActionTitleFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[8]';
    public $callToActionTitleFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/select/option[9]';
    public $callToActionTitleLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[3]/div/span/div/div/input';

    public $callToActionDescriptionTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/h2/button';
    public $callToActionDescriptionTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[1]/div[2]/div/div/div/span/div/div/input';
    public $callToActionDescriptionFontSizeFrontEndSelector='.responsive-block-editor-addons-cta-title.responsive-block-editor-addons-font-size-25';
    public $callToActionDescriptionFontSizeOnPageSelector='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-title.responsive-block-editor-addons-font-size-25.keep-placeholder-on-focus';
    public $callToActionDescriptionFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[1]';
    public $callToActionDescriptionFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[2]';
    public $callToActionDescriptionFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[3]';
    public $callToActionDescriptionFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[4]';
    public $callToActionDescriptionFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[5]';
    public $callToActionDescriptionFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[6]';
    public $callToActionDescriptionFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[7]';
    public $callToActionDescriptionFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[8]';
    public $callToActionDescriptionFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/select/option[9]';
    public $callToActionDescriptionLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[3]/div/span/div/div/input';

    public $callToActionTextColorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/h2/button';
    public $callToActionTextColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/div/fieldset/div/div[1]/button';
    public $callToActionTextColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/div/fieldset/div/div[6]/button';

    public $callToActionBackgroundOptionsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $callToActionBackgroundTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[1]';
    public $callToActionBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[2]';
    public $callToActionBackgroundColorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/h2/button';
    public $callToActionBackgroundColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/div/fieldset/div/div[1]/button';
    public $callToActionBackgroundColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/div/fieldset/div/div[6]/button';
    public $callToActionBackgroundColorOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/span/div/div/input';
    public $callToActionBackgroundColorOpacityClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/span/span[2]/button';

    public $callToActionBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[3]';
    public $callToActionBackgroundTypeGradientColor1RedSelected='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/button';
    public $callToActionBackgroundTypeGradientColor1ClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[6]/button';
    public $callToActionBackgroundTypeGradientColor2LightBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[4]/button';
    public $callToActionBackgroundTypeGradientColor2ClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[6]/button';
    public $callToActionBackgroundTypeGradientColorLocation1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/div/div/input';
    public $callToActionBackgroundTypeGradientColorLocation2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/div/div/input';
    public $callToActionBackgroundTypeGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/div/div/input';

    public $callToActionBackgroundTypeImage='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[4]';

    public $callToActionSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/h2/button';
    public $callToActionSpacingSpacingTab='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/h2/button';
    public $callToActionSpacingTopPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[1]/div/span/div/div/input';
    public $callToActionSpacingTopPaddingResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[1]/div/span/span[2]/button';
    public $callToActionSpacingBottomPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[2]/div/span/div/div/input';
    public $callToActionSpacingBottomPaddingResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[2]/div/span/span[2]/button';
    public $callToActionSpacingLeftPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[3]/div/span/div/div/input';
    public $callToActionSpacingLeftPaddingResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[3]/div/span/span[2]/button';
    public $callToActionSpacingRightPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[4]/div/span/div/div/input';
    public $callToActionSpacingRightPaddingResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[1]/div[4]/div/span/span[2]/button';

    public $callToActionSpacingMarginTab='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/h2/button';
    public $callToActionSpacingMarginTitleBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div[1]/div/span/div/div/input';
    public $callToActionSpacingMarginTitleBottomMarginResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div[1]/div/span/span[2]/button';
    public $callToActionSpacingMarginDiscriptionBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div[2]/div/span/div/div/input';
    public $callToActionSpacingMarginDiscriptionBottomMarginResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div[2]/div/span/span[2]/button';
    public $callToActionSpacingMarginButtonBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div[3]/div/span/div/div/input';
    public $callToActionSpacingMarginButtonBottomMarginResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[6]/div[2]/div[3]/div/span/span[2]/button';

    public $callToActionButtonOptions='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $callToActionButtonOptionsOpneLinkInNewWindow='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/span/input';
    public $callToActionFrontEndButtonSelector='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium';
    public $callToActionFrontEndSubHeadingSelector='.responsive-block-editor-addons-cta-content > div';
    public $callToActionButtonOptionsButtonSizeSmall='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/select/option[1]';
    public $callToActionButtonOptionsButtonSizeSmallClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-small';
    public $callToActionButtonOptionsButtonSizeMedium='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/select/option[2]';
    public $callToActionButtonOptionsButtonSizeMediumClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium';
    public $callToActionButtonOptionsButtonSizeLarge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/select/option[3]';
    public $callToActionButtonOptionsButtonSizeLargeClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-large';
    public $callToActionButtonOptionsButtonSizeExtraLarge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/select/option[4]';
    public $callToActionButtonOptionsButtonSizeExtraLargeClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-extralarge';

    public $callToActionButtonOptionsButtonShapeSquare='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/select/option[1]';
    public $callToActionButtonOptionsButtonShapeSquareClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-square.responsive-block-editor-addons-cta-button-size-medium';
    public $callToActionButtonOptionsButtonShapeRoundedSquare='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/select/option[2]';
    public $callToActionButtonOptionsButtonShapeRoundedSquareClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium';
    public $callToActionButtonOptionsButtonShapeCircular='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/select/option[3]';
    public $callToActionButtonOptionsButtonShapeCircularClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-circular.responsive-block-editor-addons-cta-button-size-medium';

    public $callToActionButtonOptionsTypeText='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/select/option[1]';
    public $callToActionButtonOptionsTypeTextClass='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-link-text';
    public $callToActionButtonOptionsTypeTextFrontEndClass='.responsive-block-editor-addons-cta-link-text';
    public $callToActionButtonOptionsTypeButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/select/option[2]';
    public $callToActionButtonOptionsTypeButtonClass='.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium';

    public $callToActionButtonOptionsBorderBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/h2/button';

    public $callToActionButtonOptionsBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[1]/div/select/option[1]';
    public $callToActionButtonOptionsBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[1]/div/select/option[2]';
    public $callToActionButtonOptionsBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[1]/div/select/option[3]';
    public $callToActionButtonOptionsBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[1]/div/select/option[4]';
    public $callToActionButtonOptionsBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[1]/div/select/option[5]';
    public $callToActionButtonOptionsBorderThickness='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[2]/div/span/div/div/input';
    public $callToActionButtonOptionsBorderVerticalPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[3]/div/span/div/div/input';
    public $callToActionButtonOptionsBorderHorizontalPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div[4]/div/span/div/div/input';

    public $callToActionButtonOptionsButtonTextColorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[7]/h2/button';
    public $callToActionButtonOptionsButtonTextColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[7]/div/div/fieldset/div/div[1]/button';
    public $callToActionButtonOptionsButtonTextColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[7]/div/div/fieldset/div/div[6]/button';

    public $callToActionButtonOptionsButtonTextHoverColorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[8]/h2/button';
    public $callToActionButtonOptionsButtonTextHoverColorLightBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[8]/div/div/fieldset/div/div[4]/button';
    public $callToActionButtonOptionsButtonTextHoverColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[8]/div/div/fieldset/div/div[6]/button';

    public $callToActionButtonOptionsBackgroundColorSettingsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/h2/button';
    public $callToActionButtonOptionsBackgroundColorSettingsColorOptions='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/h2/button';
    public $callToActionButtonOptionsBackgroundColorSettingsBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[1]/div/select/option[2]';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonColorDropdown='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[2]/h2/button';

    public $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[2]/div/div/fieldset/div/div[1]/button';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[2]/div/div/fieldset/div/div[6]/button';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonHoverColorDropdown='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[3]/h2/button';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonHoverColorLightBrownSelect='';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonHoverColorClear='';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorDropdown='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[4]/h2/button';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorBlackSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[4]/div/div/fieldset/div/div[2]/button';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[4]/div/div/fieldset/div/div[6]/button';
    public $callToActionButtonOptionsBackgroundColorSettingsButtonBorderHoverColorDropdown='';

    public $callToActionButtonOptionsBackgroundColorSettingsBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[1]/div/select/option[3]';
    public $callToActionButtonOptionsBackgroundTypeGradientColor1RedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[2]/div[1]/button';
    public $callToActionButtonOptionsBackgroundTypeGradientColor1Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[2]/div[6]/button';
    public $callToActionButtonOptionsBackgroundTypeGradientColor2RedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[3]/div[4]/button';
    public $callToActionButtonOptionsBackgroundTypeGradientColor2Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[3]/div[6]/button';
    public $callToActionButtonOptionsBackgroundTypeGradientLocation1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[4]/div/span/div/div/input';
    public $callToActionButtonOptionsBackgroundTypeGradientLocation2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[5]/div/span/div/div/input';
    public $callToActionButtonOptionsBackgroundTypeGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[6]/div/div[6]/div/span/div/div/input';

    public $callToActionIconSettings='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/h2/button';
    public $callToActionIconSettingsSelectIconDropdown='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div';
    public $callToActionIconSettingsSelectIcon='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div[2]/div/div[2]/div[2]/span[3]/span';
    public $callToActionIconSettingsIconColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div[1]/button';
    public $callToActionIconSettingsIconColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div[6]/button';
    public $callToActionIconSettingsIconPositionBeforeText='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[1]';
    public $callToActionIconSettingsIconPositionBeforeTextClass='.responsive-block-editor-addons-cta-button__icon.responsive-block-editor-addons-cta-button__icon-position-before';
    public $callToActionIconSettingsIconPositionAfterTextClass='.responsive-block-editor-addons-cta-button__icon.responsive-block-editor-addons-cta-button__icon-position-after';
    public $callToActionIconSettingsDeleteIcon='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/div[1]/span/span[2]';
    public $callToActionIconSettingsIconPositionAfterText='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/select/option[2]';
    public $callToActionIconSettingsIconSpacing='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[4]/div/span/div/div/input';
    public $callToActionIconSettingsIconSpacingResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[4]/div/span/span[2]/button';

    //ShapeDivider
    public $shapeDividerSelectClass='.wp-block-responsive-block-editor-addons-shape-divider';
    public $shapeDividerShapeColorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[3]/h2/button';
    public $shapeDividerShapeColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[3]/div/div/fieldset/div/div[1]/button';
    public $shapeDividerShapeColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[3]/div/div/fieldset/div/div[6]/button';

    public $shapeDividerBackgroundBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/h2/button';
    public $shapeDividerBackgroundTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div/div/select/option[1]';
    public $shapeDividerBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div/div/select/option[2]';
    public $shapeDividerBackgroundColorDropDownBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[2]/h2/button';
    public $shapeDividerBackgroundColorBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[2]/div/div/fieldset/div/div[3]/button';
    public $shapeDividerBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div/div/select/option[3]';
    public $shapeDividerBackgroundTypeGradientColor1Red='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[2]/div[1]/button';
    public $shapeDividerBackgroundTypeGradientColor1Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[2]/div[6]/button';
    public $shapeDividerBackgroundTypeGradientColor2LightBrown='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[3]/div[4]/button';
    public $shapeDividerBackgroundTypeGradientColor2Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[3]/div[6]/button';
    public $shapeDividerBackgroundTypeGradientColorLocation1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[4]/div/span/div/div/input';
    public $shapeDividerBackgroundTypeGradientColorLocation2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[5]/div/span/div/div/input';
    public $shapeDividerBackgroundTypeGradientColorGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[2]/div[6]/div/span/div/div/input';

    public $shapeDividerDividerSettingsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[1]/h2/button';
    public $shapeDividerShapeHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[1]/div[1]/div/div[2]/div/div/div/input';
    public $shapeDividerBackgroundHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[3]/div[1]/div[2]/div/div[2]/div/div/div/input';

    public $shapeDividerStyleWavy='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[1]';
    public $shapeDividerStyleWavyClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-wavy';
    public $shapeDividerStyleWavyClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-wavy';
    public $shapeDividerStyleHills='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[2]';
    public $shapeDividerStyleHillsClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-hills';
    public $shapeDividerStyleHillsClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-hills';
    public $shapeDividerStyleWaves='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[3]';
    public $shapeDividerStyleWavesClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-waves';
    public $shapeDividerStyleWavesClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-waves';
    public $shapeDividerStyleAngled='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[4]';
    public $shapeDividerStyleAngledClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-angled';
    public $shapeDividerStyleAngledClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-angled';
    public $shapeDividerStyleSloped='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[5]';
    public $shapeDividerStyleSlopedClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-sloped';
    public $shapeDividerStyleSlopedClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-sloped';
    public $shapeDividerStyleRounded='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[6]';
    public $shapeDividerStyleRoundedClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-rounded';
    public $shapeDividerStyleRoundedClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-rounded';
    public $shapeDividerStyleTriangle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[7]';
    public $shapeDividerStyleTriangleClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-triangle';
    public $shapeDividerStyleTriangleClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-triangle';
    public $shapeDividerStylePointed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div/div[1]/div[8]';
    public $shapeDividerStylePointedClassSelector='.wp-block-responsive-block-editor-addons-shape-divider.is-style-pointed';
    public $shapeDividerStylePointedClassSelectorFrontEnd='.wp-block-responsive-block-editor-addons-shape-divider.alignfull.is-style-pointed';

    // Advance Parent Column Selectors
    public $AdvanceColumnParentSelectClass='.block-editor-block-list__layout.is-root-container > div:first-child';

    public $AdvanceColumnParentGeneralBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $AdvanceColumnParentGeneralNumberOfColumns='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $AdvanceColumnParentGeneralNumberOfColumnsClass='.responsive-columns-inner-wrap.responsive-columns-columns-4';
    public $AdvanceColumnParentGeneralNumberOfColumnsReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/span[2]/button';
    public $AdvanceColumnParentGeneralColumnGapDefault10px='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[1]';
    public $AdvanceColumnParentGeneralColumnGapDefault10pxClass='.responsive-block-editor-addons-block-columns.responsive-columns-wrap.responsive-columns__gap-default';
    public $AdvanceColumnParentGeneralColumnGapNoGap='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[2]';
    public $AdvanceColumnParentGeneralColumnGapNoGapClass='.responsive-block-editor-addons-block-columns.responsive-columns-wrap.responsive-columns__gap-nogap';
    public $AdvanceColumnParentGeneralColumnGapNarrow='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[3]';
    public $AdvanceColumnParentGeneralColumnGapNarrowClass='.responsive-block-editor-addons-block-columns.responsive-columns-wrap.responsive-columns__gap-narrow';
    public $AdvanceColumnParentGeneralColumnGapExtended='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[4]';
    public $AdvanceColumnParentGeneralColumnGapExtendedClass='.responsive-block-editor-addons-block-columns.responsive-columns-wrap.responsive-columns__gap-extended';
    public $AdvanceColumnParentGeneralColumnGapWide='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[5]';
    public $AdvanceColumnParentGeneralColumnGapWideClass='.responsive-block-editor-addons-block-columns.responsive-columns-wrap.responsive-columns__gap-wide';
    public $AdvanceColumnParentGeneralColumnGapWider='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/select/option[6]';
    public $AdvanceColumnParentGeneralColumnGapWiderClass='.responsive-block-editor-addons-block-columns.responsive-columns-wrap.responsive-columns__gap-wider';

    public $AdvanceColumnParentGeneralStackOnNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[1]';
    public $AdvanceColumnParentGeneralStackOnNoneClass='.responsive-columns__stack-none';
    public $AdvanceColumnParentGeneralStackOnTablet='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[2]';
    public $AdvanceColumnParentGeneralStackOnTabletClass='.responsive-columns__stack-tablet';
    public $AdvanceColumnParentGeneralStackOnMobile='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[3]';
    public $AdvanceColumnParentGeneralStackOnMobileClass='.responsive-columns__stack-mobile';

    public $AdvanceColumnParentGeneralHeightNormal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[5]/div/select/option[1]';
    public $AdvanceColumnParentGeneralHeightHalfScreen='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[5]/div/select/option[2]';
    public $AdvanceColumnParentGeneralHeightFullScreen='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[5]/div/select/option[3]';
    public $AdvanceColumnParentGeneralHeightCustom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[5]/div/select/option[4]';

    public $AdvanceColumnParentGeneralHorizontalAlignLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[6]/div/select/option[1]';
    public $AdvanceColumnParentGeneralHorizontalAlignRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[6]/div/select/option[2]';
    public $AdvanceColumnParentGeneralHorizontalAlignCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[6]/div/select/option[3]';

    public $AdvanceColumnParentGeneralVerticalAlignTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div/select/option[1]';
    public $AdvanceColumnParentGeneralVerticalAlignBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div/select/option[2]';
    public $AdvanceColumnParentGeneralVerticalAlignCenter='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[7]/div/select/option[3]';

    public $AdvanceColumnParentGeneralZIndex='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[8]/div/span/div/div/input';
    public $AdvanceColumnParentGeneralZIndexReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[8]/div/span/span[2]/button';

    public $AdvanceColumnParentBackgroundBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $AdvanceColumnParentBackgroundTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[1]';
    public $AdvanceColumnParentBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[2]';
    public $AdvanceColumnParentBackgroundBackgroundColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/button';
    public $AdvanceColumnParentBackgroundBackgroundColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[6]/button';
    public $AdvanceColumnParentBackgroundOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/span/div/div/input';
    public $AdvanceColumnParentBackgroundOpacityReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/span/span[2]/button';

    public $AdvanceColumnParentBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[3]';
    public $AdvanceColumnParentBackgroundTypeGradientColor1Red='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/button';
    public $AdvanceColumnParentBackgroundTypeGradientColor1Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[6]/button';
    public $AdvanceColumnParentBackgroundTypeGradientColor2LightBrown='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div[4]/button';
    public $AdvanceColumnParentBackgroundTypeGradientColor2Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div[6]/button';
    public $AdvanceColumnParentBackgroundTypeGradientColorLocation1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[4]/div/span/div/div/input';
    public $AdvanceColumnParentBackgroundTypeGradientColorLocation2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[5]/div/span/div/div/input';
    public $AdvanceColumnParentBackgroundTypeGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[6]/div/span/div/div/input';
    public $AdvanceColumnParentBackgroundTypeImage='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[4]';

    public $AdvanceColumnParentSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $AdvanceColumnParentTopPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/div/div/input';
    public $AdvanceColumnParentTopPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/span[2]/button';
    public $AdvanceColumnParentBottomPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $AdvanceColumnParentBottomPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/span[2]/button';
    public $AdvanceColumnParentLeftPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $AdvanceColumnParentLeftPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';
    public $AdvanceColumnParentRightPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/div/div/input';
    public $AdvanceColumnParentRightPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/span[2]/button';
    public $AdvanceColumnParentTopMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/div/div/input';
    public $AdvanceColumnParentTopMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/span[2]/button';
    public $AdvanceColumnParentBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/div/div/input';
    public $AdvanceColumnParentBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/span[2]/button';

    public $AdvanceColumnParentBorderBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $AdvanceColumnParentBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[1]';
    public $AdvanceColumnParentBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[2]';
    public $AdvanceColumnParentBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[3]';
    public $AdvanceColumnParentBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[4]';
    public $AdvanceColumnParentBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[5]';
    public $AdvanceColumnParentBorderStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[6]';
    public $AdvanceColumnParentBorderStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[7]';
    public $AdvanceColumnParentBorderStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[8]';
    public $AdvanceColumnParentBorderStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[9]';
    public $AdvanceColumnParentBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $AdvanceColumnParentBorderWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/span[2]/button';
    public $AdvanceColumnParentBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/div/div/input';
    public $AdvanceColumnParentBorderRadiusReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/span[2]/button';
    public $AdvanceColumnParentBorderColorResdSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div[1]/button';
    public $AdvanceColumnParentBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div[6]/button';

    public $AdvanceColumnParentBorderBoxShadowBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/button[1]';
    public $AdvanceColumnParentBorderBoxShadowColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[1]/div[1]/button';
    public $AdvanceColumnParentBorderBoxShadowColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[1]/div[6]/button';
    public $AdvanceColumnParentBorderBoxHorizontal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[2]/div/span/div/div/input';
    public $AdvanceColumnParentBorderBoxHorizontalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[2]/div/span/span[2]/button';
    public $AdvanceColumnParentBorderBoxVertical='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[3]/div/span/div/div/input';
    public $AdvanceColumnParentBorderBoxVerticalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[3]/div/span/span[2]/button';
    public $AdvanceColumnParentBorderBoxBlur='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[4]/div/span/div/div/input';
    public $AdvanceColumnParentBorderBoxBlurReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[4]/div/span/span[2]/button';
    public $AdvanceColumnParentBorderBoxSpread='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[5]/div/span/div/div/input';
    public $AdvanceColumnParentBorderBoxBlurSpread='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[5]/div/span/span[2]/button';
    public $AdvanceColumnParentBorderBoxPositionOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[6]/div/select/option[2]';
    public $AdvanceColumnParentBorderBoxPositionInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[6]/div/select/option[1]';


    // Advance Inner Column Selectors
    public $AdvanceColumnSelectClass='.block-editor-block-list__layout.is-root-container > div:first-child';
    public $AdvanceInnerColumnSelectClass='.block-editor-block-list__layout > div:nth-child(1)';
    public $AdvanceInnerColumnLayoutBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $AdvanceInnerColumnContentWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div/div/span/div/div/input';
    public $AdvanceInnerColumnContentWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div/div/span/span[2]/button';

    public $AdvanceInnerColumnBackgroundBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $AdvanceInnerColumnBackgroundTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[1]';
    public $AdvanceInnerColumnBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[2]';
    public $AdvanceInnerColumnBackgroundColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/button';
    public $AdvanceInnerColumnBackgroundColorClearBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[6]/button';
    public $AdvanceInnerColumnBackgroundOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/span/div/div/input';
    public $AdvanceInnerColumnBackgroundOpacityResetBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/span/span[2]/button';
    public $AdvanceInnerColumnBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[3]';
    public $AdvanceInnerColumnBackgroundGradientColor1RedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/button';
    public $AdvanceInnerColumnBackgroundGradientColor1Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[6]/button';
    public $AdvanceInnerColumnBackgroundGradientColor2LightBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div[4]/button';
    public $AdvanceInnerColumnBackgroundGradientColor2Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div[6]/button';
    public $AdvanceInnerColumnBackgroundGradientColorLocation1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[4]/div/span/div/div/input';
    public $AdvanceInnerColumnBackgroundGradientColorLocation2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[5]/div/span/div/div/input';
    public $AdvanceInnerColumnBackgroundGradientColorGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[6]/div/span/div/div/input';
    public $AdvanceInnerColumnBackgroundTypeImage='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/select/option[4]';

    public $AdvanceInnerColumnSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $AdvanceInnerColumnSpacingTopPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingTopPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingBottomPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingBottomPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingLeftPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingLeftPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingRightPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingRightPaddingReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingTopMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingTopMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingLeftMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[7]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingLeftMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[7]/div/span/span[2]/button';
    public $AdvanceInnerColumnSpacingRightMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[8]/div/span/div/div/input';
    public $AdvanceInnerColumnSpacingRightMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[8]/div/span/span[2]/button';

    public $AdvanceInnerColumnBorderBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $AdvanceInnerColumnBorderTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[1]';
    public $AdvanceInnerColumnBorderTypeSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[2]';
    public $AdvanceInnerColumnBorderTypeDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[3]';
    public $AdvanceInnerColumnBorderTypeDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[4]';
    public $AdvanceInnerColumnBorderTypeDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[5]';
    public $AdvanceInnerColumnBorderTypeGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[6]';
    public $AdvanceInnerColumnBorderTypeInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[7]';
    public $AdvanceInnerColumnBorderTypeOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[8]';
    public $AdvanceInnerColumnBorderTypeRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[9]';
    public $AdvanceInnerColumnBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $AdvanceInnerColumnBorderRadiusReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/span[2]/button';

    public $AdvanceInnerColumnBoxShadowBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/button[1]';
    public $AdvanceInnerColumnBoxShadowColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[1]/div[1]/button';
    public $AdvanceInnerColumnBoxShadowColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[1]/div[6]/button';
    public $AdvanceInnerColumnBoxShadowHorizontal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[2]/div/span/div/div/input';
    public $AdvanceInnerColumnBoxShadowHorizontalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[2]/div/span/span[2]/button';
    public $AdvanceInnerColumnBoxShadowVertical='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[3]/div/span/div/div/input';
    public $AdvanceInnerColumnBoxShadowVerticalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[3]/div/span/span[2]/button';
    public $AdvanceInnerColumnBoxShadowBlur='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[4]/div/span/div/div/input';
    public $AdvanceInnerColumnBoxShadowBlurReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[4]/div/span/span[2]/button';
    public $AdvanceInnerColumnBoxShadowSpread='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[5]/div/span/div/div/input';
    public $AdvanceInnerColumnBoxShadowSpreadReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[5]/div/span/span[2]/button';

    public $AdvanceInnerColumnBoxShadowPositionInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[6]/div/select/option[1]';
    public $AdvanceInnerColumnBoxShadowPositionOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/div[6]/div/select/option[2]';


    // Selectors for Info-block
    public $infoBlockSelectClass='.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team';
    public $infoBlockIconBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $infoBlockIconPositionAboveTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[1]';
    public $infoBlockIconPositionAboveTitleClassOnPage='.responsive-block-editor-addons-infobox__content-wrap.responsive-block-editor-addons-infobox.responsive-block-editor-addons-infobox-has-icon.responsive-block-editor-addons-infobox-icon-above-title';
    public $infoBlockIconPositionBelowTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[2]';
    public $infoBlockIconPositionBelowTitleClass='.responsive-block-editor-addons-infobox__content-wrap.responsive-block-editor-addons-infobox.responsive-block-editor-addons-infobox-has-icon.responsive-block-editor-addons-infobox-icon-below-title';
    public $infoBlockIconPositionLeftOfTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[3]';
    public $infoBlockIconPositionLeftOfTitleClass='.responsive-block-editor-addons-infobox__content-wrap.responsive-block-editor-addons-infobox.responsive-block-editor-addons-infobox-has-icon.responsive-block-editor-addons-infobox-icon-left-title';
    public $infoBlockIconPositionRightOfTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[4]';
    public $infoBlockIconPositionRightOfTitleClass='.responsive-block-editor-addons-infobox__content-wrap.responsive-block-editor-addons-infobox.responsive-block-editor-addons-infobox-has-icon.responsive-block-editor-addons-infobox-icon-right-title';
    public $infoBlockIconPositionLeftOfTextAndTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[5]';
    public $infoBlockIconPositionLeftOfTextAndTitleClass='.responsive-block-editor-addons-infobox__content-wrap.responsive-block-editor-addons-infobox.responsive-block-editor-addons-infobox-has-icon.responsive-block-editor-addons-infobox-icon-left.responsive-block-editor-addons-infobox-left';
    public $infoBlockIconPositionRightOfTextAndTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[6]';
    public $infoBlockIconPositionRightOfTextAndTitleClass='.responsive-block-editor-addons-infobox__content-wrap.responsive-block-editor-addons-infobox.responsive-block-editor-addons-infobox-has-icon.responsive-block-editor-addons-infobox-icon-right.responsive-block-editor-addons-infobox-right';
    public $infoBlockIconSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/span/div/div/input';
    public $infoBlockIconSizeReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/span/span[2]/button';

    public $infoBlockBackgroundOptionsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $infoBlockBackgroundOptionsBackgroundColorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/h2/button';
    public $infoBlockBackgroundOptionsBackgroundColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/div/fieldset/div/div[1]/button';
    public $infoBlockBackgroundOptionsBackgroundColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[1]/div/fieldset/div/div[6]/button';
    public $infoBlockBackgroundOptionsOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/span/div/div/input';
    public $infoBlockBackgroundOptionsOpacityReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/span/span[2]/button';

    public $infoBlockBorderBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';

    public $infoBlockBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[1]';
    public $infoBlockBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[2]';
    public $infoBlockBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[3]';
    public $infoBlockBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[4]';
    public $infoBlockBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[5]';
    public $infoBlockBorderStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[6]';
    public $infoBlockBorderStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[7]';
    public $infoBlockBorderStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[8]';
    public $infoBlockBorderStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[9]';
    public $infoBlockBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/div/div/input';

    public $infoBlockBorderRadiusReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/span[2]/button';
    public $infoBlockBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';

    public $infoBlockBorderWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/span[2]/button';
    public $infoBlockBorderColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div[1]/button';
    public $infoBlockBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div[6]/button';
    public $infoBlockBorderBoxShadowBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/button[1]';

    public $infoBlockBorderBoxShadowColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[1]/div[1]/button';
    public $infoBlockBorderBoxShadowColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[1]/div[6]/button';
    public $infoBlockBorderBoxShadowHorizontal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[2]/div/span/div/div/input';
    public $infoBlockBorderBoxShadowHorizontalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[2]/div/span/span[2]/button';
    public $infoBlockBorderBoxShadowVertical='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[3]/div/span/div/div/input';
    public $infoBlockBorderBoxShadowVerticalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[3]/div/span/span[2]/button';
    public $infoBlockBorderBoxShadowBlur='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[4]/div/span/div/div/input';
    public $infoBlockBorderBoxShadowBlurReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[4]/div/span/span[2]/button';
    public $infoBlockBorderBoxShadowSpread='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[5]/div/span/div/div/input';
    public $infoBlockBorderBoxShadowSpreadReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[5]/div/span/span[2]/button';
    public $infoBlockBorderBoxShadowPositionInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[6]/div/select/option[1]';
    public $infoBlockBorderBoxShadowPositionOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[6]/div/select/option[2]';
    public $infoBlockSeparatorBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/h2/button';

    public $infoBlockSeparatorPositionAfterImage='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[1]';
    public $infoBlockSeparatorPositionAfterImageClassOnPage='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(2) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfterImageClassOnFrontEnd='div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfterPrefix='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[2]';
    public $infoBlockSeparatorPositionAfterPrefixClassOnPage='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div.responsive-block-editor-addons-ifb-title-wrap > div:nth-child(2) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfterPrefixClassOnFrontEnd='div.post-inner.thin > div > div > div > div > div > div.responsive-block-editor-addons-ifb-title-wrap > div > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfteTitle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[3]';
    public $infoBlockSeparatorPositionAfteTitleClassOnPage='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfteTitleClassOnFrontEnd='div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfterDescription='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[4]';
    public $infoBlockSeparatorPositionAfterDescriptionClassOnPage='#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.wp-block > div > div > div > div > div.responsive-block-editor-addons-ifb-text-wrap > div:nth-child(2) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorPositionAfterDescriptionClassOnFrontEnd='div.post-inner.thin > div > div > div > div > div > div.responsive-block-editor-addons-ifb-text-wrap > div:nth-child(2) > div.responsive-block-editor-addons-ifb-separator';
    public $infoBlockSeparatorStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/select/option[1]';

    public $infoBlockSeparatorStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/select/option[2]';
    public $infoBlockSeparatorStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/select/option[3]';
    public $infoBlockSeparatorStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/select/option[4]';
    public $infoBlockSeparatorStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/select/option[5]';
    public $infoBlockSeparatorThickness='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/span/div/div/input';

    public $infoBlockSeparatorThicknessReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/span/span[2]/button';
    public $infoBlockSeparatorWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[5]/div/span/div/div/input';

    public $infoBlockSeparatorWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[5]/div/span/span[2]/button';
    public $infoBlockSeparatorColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[6]/div[1]/button';

    public $infoBlockSeparatorColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[6]/div[6]/button';
    public $infoBlockContentBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/h2/button';

    public $infoBlockContentEnablePrefixBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[1]/div/span/input';
    public $infoBlockContentPrefixClass='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title-prefix';
    public $infoBlockContentPrefixColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[2]/div[1]/button';
    public $infoBlockContentPrefixColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[2]/div[6]/button';
    public $infoBlockContentEnableTitleBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[3]/div/span/input';

    public $infoBlockContentTitleClass='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title';
    public $infoBlockContentTitleTagH1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[4]/div/select/option[1]';
    public $infoBlockContentTitleTagH2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[4]/div/select/option[2]';
    public $infoBlockContentTitleTagH3='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[4]/div/select/option[3]';
    public $infoBlockContentTitleTagH4='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[4]/div/select/option[4]';
    public $infoBlockContentTitleTagH5='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[4]/div/select/option[5]';
    public $infoBlockContentTitleTagH6='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[4]/div/select/option[6]';
    public $infoBlockContentTitleFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[5]/div[2]/div/div/div/span/div/div/input';
    public $infoBlockContentTitleColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[6]/div[1]/button';
    public $infoBlockContentTitleColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[6]/div[6]/button';
    public $infoBlockContentEnableDescriptionBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[7]/div/span/input';

    public $infoBlockContentDescriptionClass='.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-desc';
    public $infoBlockContentDescriptionColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[8]/div[1]/button';
    public $infoBlockContentDescriptionColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[7]/div[8]/div[6]/button';

    public $infoBlockSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $infoBlockContentPadding='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/span/div/div/input';
    public $infoBlockPrefixBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/div/div/input';
    public $infoBlockPrefixBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div/span/span[2]/button';
    public $infoBlockTitleBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $infoBlockTitleBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';
    public $infoBlockSeparatorBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/div/div/input';
    public $infoBlockSeparatorBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/span[2]/button';
    public $infoBlockDescriptionBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/div/div/input';
    public $infoBlockDescriptionBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/span[2]/button';
    public $infoBlockImageTopMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/div/div/input';
    public $infoBlockImageBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[7]/div/span/div/div/input';
    public $infoBlockImageLeftMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[8]/div/span/div/div/input';
    public $infoBlockImageRightMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[9]/div/span/div/div/input';


    //Section Block Selectors
    public $selectorfordividerblock='.wp-block-responsive-block-editor-addons-divider.responsive-block-editor-addons-block-spacer.solid.responsive-block-editor-addons-spacer-divider';
    public $selectorForDividerOuterClass='.block-editor-block-list__block.wp-block';
    public $sectionSelectClass='.block-editor-block-list__block.wp-block.is-selected.rich-text.block-editor-rich-text__editable.wp-block';
    public $sectionGeneralBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $sectionGeneralContentWidthBoxed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[1]';
    public $sectionGeneralContentWidthFull='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/select/option[2]';
    public $sectionGeneralWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/span/div/div/input';
    public $sectionGeneralZIndex='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/span/div/div/input';
    public $sectionGeneralZIndexReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/span/span[2]/button';

    public $sectionSpacingBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $sectionSpacingPaddingTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/div/span/div/div/input';
    public $sectionSpacingPaddingTopReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[1]/div/span/span[2]/button';
    public $sectionSpacingPaddingBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div/span/div/div/input';
    public $sectionSpacingPaddingBottomReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[2]/div/span/span[2]/button';
    public $sectionSpacingPaddingLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[3]/div/span/div/div/input';
    public $sectionSpacingPaddingLeftReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[3]/div/span/span[2]/button';
    public $sectionSpacingPaddingRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[4]/div/span/div/div/input';
    public $sectionSpacingPaddingRightReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div[2]/div/div[4]/div/span/span[2]/button';
    public $sectionSpacingMarginTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[1]/div/span/div/div/input';
    public $sectionSpacingMarginTopReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[1]/div/span/span[2]/button';
    public $sectionSpacingMarginBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[2]/div/span/div/div/input';
    public $sectionSpacingMarginBottomReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[2]/div/span/span[2]/button';
    public $sectionSpacingMarginLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[3]/div/span/div/div/input';
    public $sectionSpacingMarginLeftReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[3]/div/span/span[2]/button';
    public $sectionSpacingMarginRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[4]/div/span/div/div/input';
    public $sectionSpacingMarginRightReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div[2]/div/div[4]/div/span/span[2]/button';

    public $sectionBackgroundBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $sectionBackgroundTypeNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[1]';
    public $sectionBackgroundTypeColor='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[2]';
    public $sectionBackgroundTypeColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/button';
    public $sectionBackgroundTypeColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[6]/button';

    public $sectionBackgroundTypeGradient='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[3]';
    public $sectionBackgroundColor1RedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/button';
    public $sectionBackgroundColor1Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[6]/button';
    public $sectionBackgroundColor2LightBrownSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[4]/button';
    public $sectionBackgroundColor2Clear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[6]/button';
    public $sectionBackgroundColorLocation1='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[4]/div/span/div/div/input';
    public $sectionBackgroundColorLocation2='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[5]/div/span/div/div/input';
    public $sectionBackgroundGradientDirection='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[6]/div/span/div/div/input';
    public $sectionBackgroundTypeImage='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[4]';
    public $sectionBackgroundTypeVideo='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div/select/option[5]';
    public $sectionBackgroundOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/div/div/input';
    public $sectionBackgroundOpacityReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div/span/span[2]/button';

    public $sectionBorderBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $sectionBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[1]';
    public $sectionBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[2]';
    public $sectionBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[3]';
    public $sectionBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[4]';
    public $sectionBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[5]';
    public $sectionBorderStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[6]';
    public $sectionBorderStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[7]';
    public $sectionBorderStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[8]';
    public $sectionBorderStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/select/option[9]';

    public $sectionBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/div/div/input';
    public $sectionBorderWidthReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/span/span[2]/button';
    public $sectionBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/div/div/input';
    public $sectionBorderRadiusReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/span/span[2]/button';
    public $sectionBorderColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div[1]/button';
    public $sectionBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div[6]/button';

    public $sectionBoxShadowBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/button[1]';
    public $sectionBoxShadowColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[1]/div[1]/button';
    public $sectionBoxShadowColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[1]/div[6]/button';
    public $sectionBoxShadowHorizontal='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[2]/div/span/div/div/input';
    public $sectionBoxShadowHorizontalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[2]/div/span/span[2]/button';
    public $sectionBoxShadowVertical='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[3]/div/span/div/div/input';
    public $sectionBoxShadowVerticalReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[3]/div/span/span[2]/button';
    public $sectionBoxShadowBlur='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[4]/div/span/div/div/input';
    public $sectionBoxShadowBlurReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[4]/div/span/span[2]/button';
    public $sectionBoxShadowSpread='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[5]/div/span/div/div/input';
    public $sectionBoxShadowSpreadReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[5]/div/span/span[2]/button';
    public $sectionBoxShadowPositionInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[6]/div/select/option[1]';
    public $sectionBoxShadowPositionOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/div[6]/div/select/option[2]';


    //Selectors for Count Up Block
    public $countUpSelectClass='.responsive-count-item';
    public $countUpGeneralButton='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $countUpGeneralAlignmentLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div[1]/button';
    public $countUpGeneralAlignmentLeftClassOnPage='.responsive-count.has-text-align-left';
    public $countUpGeneralAlignmentLeftClassOnFrontEnd='.has-text-align-left';
    public $countUpGeneralAlignmentMiddle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div[2]/button';
    public $countUpGeneralAlignmentMiddleClassOnPage='.responsive-count.has-text-align-center';
    public $countUpGeneralAlignmentMiddleClassOnFrontEnd='.has-text-align-center';
    public $countUpGeneralAlignmentRight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/div/div[3]/button';
    public $countUpGeneralAlignmentRightClassOnPage='.responsive-count.has-text-align-right';
    public $countUpGeneralAlignmentRightClassOnFrontEnd='.has-text-align-right';
    public $countUpColumns='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/span/div/div/input';

    public $countUpContentBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/h2/button';
    public $countUpContentEnableTitleToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[1]/div/span/input';
    public $countUpContentEnableTitleToggleClassOnPage='.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus';
    public $countUpContentEnableTitleToggleClassOnFrontEnd='.responsive-count-item__title';
    public $countUpContentEnableNumberToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[2]/div/span/input';
    public $countUpContentEnableNumberToggleClassOnPage='.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus';
    public $countUpContentEnableNumberToggleClassOnFrontEnd='.responsive-count-item__price-wrapper';
    public $countUpContentEnableDiscriptionToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[3]/div/span/input';
    public $countUpContentEnableDiscriptionToggleClassOnPage='.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus';
    public $countUpContentEnableDiscriptionToggleClassFrontEnd='.responsive-count-item__features';
    public $countUpContentEnableIconToggle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[2]/div[4]/div/span/input';
    public $countUpContentEnableIconToggleClassOnPage='.responsive-block-editor-addons-count-up__source-icon';
    public $countUpContentEnableIconToggleClassOnFrontEnd='.responsive-block-editor-addons-count-up__source-icon';

    public $countUpBorderBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/h2/button';
    public $countUpBorderStyleNone='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[1]';
    public $countUpBorderStyleSolid='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[2]';
    public $countUpBorderStyleDotted='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[3]';
    public $countUpBorderStyleDashed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[4]';
    public $countUpBorderStyleDouble='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[5]';
    public $countUpBorderStyleGroove='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[6]';
    public $countUpBorderStyleInset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[7]';
    public $countUpBorderStyleOutset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[8]';
    public $countUpBorderStyleRidge='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[1]/div/select/option[9]';
    public $countUpBorderWidth='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[2]/div/span/div/div/input';
    public $countUpBorderRadius='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[3]/div/span/div/div/input';
    public $countUpBorderColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[4]/div[1]/button';
    public $countUpBorderColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[5]/div[4]/div[6]/button';

    public $countUpColorSettingsBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/h2/button';
    public $countUpColorTitleColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/fieldset/div/div[1]/button';
    public $countUpColorTitleColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[1]/div/fieldset/div/div[6]/button';
    public $countUpColorNumberColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/fieldset/div/div[1]/button';
    public $countUpColorNumberColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[2]/div/fieldset/div/div[6]/button';
    public $countUpColorTextColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/fieldset/div/div[1]/button';
    public $countUpColorTextColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[3]/div/fieldset/div/div[6]/button';
    public $countUpColorBackgroundColorRedSelect='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/fieldset/div/div[1]/button';
    public $countUpColorBackgroundColorClear='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[4]/div/fieldset/div/div[6]/button';
    public $countUpColorOpacity='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/span/div/div/input';
    public $countUpColorOpacityReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[4]/div[5]/div/span/span[2]/button';

    public $countUpTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/h2/button';
    public $countUpNumberTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/h2/button';
    public $countUpNumberTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[1]/div/span/div/div/input';
    public $countUpNumberTypographyFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[1]';
    public $countUpNumberTypographyFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[2]';
    public $countUpNumberTypographyFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[3]';
    public $countUpNumberTypographyFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[4]';
    public $countUpNumberTypographyFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[5]';
    public $countUpNumberTypographyFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[6]';
    public $countUpNumberTypographyFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[7]';
    public $countUpNumberTypographyFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[8]';
    public $countUpNumberTypographyFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[2]/div/select/option[9]';
    public $countUpNumberTypographyLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[3]/div/span/div/div/input';
    public $countUpNumberTypographyNumberBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[4]/div/span/div/div/input';
    public $countUpNumberTypographyNumberBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[1]/div[4]/div/span/span[2]/button';

    public $countUpHeadingTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/h2/button';
    public $countUpHeadingTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[1]/div[2]/div/div/div/span/div/div/input';
    public $countUpHeadingTypographyFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[1]';
    public $countUpHeadingTypographyFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[2]';
    public $countUpHeadingTypographyFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[3]';
    public $countUpHeadingTypographyFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[4]';
    public $countUpHeadingTypographyFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[5]';
    public $countUpHeadingTypographyFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[6]';
    public $countUpHeadingTypographyFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[7]';
    public $countUpHeadingTypographyFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[8]';
    public $countUpHeadingTypographyFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[2]/div/select/option[9]';
    public $countUpHeadingTypographyLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[3]/div/span/div/div/input';
    public $countUpHeadingTypographyBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[4]/div/span/div/div/input';
    public $countUpHeadingTypographyBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[2]/div[4]/div/span/span[2]/button';

    public $countUpDescriptionTypographyBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/h2/button';
    public $countUpDescriptionTypographyFontSize='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[1]/div/span/div/div/input';
    public $countUpDescriptionTypographyFontWeight100='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[1]';
    public $countUpDescriptionTypographyFontWeight200='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[2]';
    public $countUpDescriptionTypographyFontWeight300='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[3]';
    public $countUpDescriptionTypographyFontWeight400='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[4]';
    public $countUpDescriptionTypographyFontWeight500='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[5]';
    public $countUpDescriptionTypographyFontWeight600='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[6]';
    public $countUpDescriptionTypographyFontWeight700='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[7]';
    public $countUpDescriptionTypographyFontWeight800='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[8]';
    public $countUpDescriptionTypographyFontWeight900='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[2]/div/select/option[9]';
    public $countUpDescriptionTypographyLineHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[3]/div/span/div/div/input';
    public $countUpDescriptionTypographyBottomMargin='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[4]/div/span/div/div/input';
    public $countUpDescriptionTypographyBottomMarginReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[3]/div[3]/div[4]/div/span/span[2]/button';


    //Selectors for Flip-Box
    public $flipBoxSelectClass='.block-editor-block-list__layout.is-root-container > div:first-child';
    public $flipBoxGeneralBtn='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button';
    public $flipBoxGeneralNumberOfFlipBoxes='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[1]/div/span/div/div/input';
    public $flipBoxGeneralNumberOfFlipBoxesClassOnPage='.wp-block-responsive-block-editor-addons-flipbox__inner.has-columns.has-3-columns';
    public $flipBoxGeneralHeight='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/span/div/div/input';
    public $flipBoxGeneralHeightReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[2]/div/span/span[2]/button';
    public $flipBoxGeneralFlipStyleRightToStyle='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[1]';
    public $flipBoxGeneralFlipStyleBottomToTop='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[2]';
    public $flipBoxGeneralFlipStyleRightToLeft='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[3]';
    public $flipBoxGeneralFlipStyleTopToBottom='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[3]/div/select/option[4]';
    public $flipBoxGeneralTransitionSpeed='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/span/div/div/input';
    public $flipBoxGeneralTransitionSpeedReset='/html/body/div[1]/div[2]/div[2]/div[1]/div[4]/div[1]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/div[4]/div/span/span[2]/button';


}