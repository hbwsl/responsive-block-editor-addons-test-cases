<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class AdvanceColumnParentBlockCest
{/*
    //Test Case for Number Of Columns
    public function NumberOfColumnsChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->pressKey($blockEditorAdOns->AdvanceColumnParentGeneralNumberOfColumns, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceColumnParentGeneralNumberOfColumns, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralNumberOfColumnsClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralNumberOfColumnsClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralNumberOfColumnsClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralNumberOfColumnsReset);
        $I->click($blockEditorAdOns->updateBtn);
        $I->

    }

    //Test Case for Column Gap Default
    public function ColumnGapDefaultChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10px);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10pxClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10pxClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10pxClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Column Gap No-Gap
    public function ColumnGapNoGapChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNoGap);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNoGapClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNoGapClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNoGapClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10px);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Column Gap Narrow
    public function ColumnGapNarrowChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNarrow);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNarrowClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNarrowClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapNarrowClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10px);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Column Gap Extended
    public function ColumnGapExtendedChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapExtended);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapExtendedClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapExtendedClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapExtendedClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10px);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Column Gap Wide
    public function ColumnGapWideChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                       Page\LoginPage $loginPage,
                                                                       Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWide);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWideClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWideClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWideClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10px);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Column Gap Wider
    public function ColumnGapWiderChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWider);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWiderClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWiderClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapWiderClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralColumnGapDefault10px);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Stack On Mobile
    public function StackOnMobileChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralStackOnMobile);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralStackOnMobileClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralStackOnMobileClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralStackOnMobileClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralStackOnMobile);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Stack On Tablet
    public function StackOnTabletChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralStackOnTablet);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralStackOnTabletClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralStackOnTabletClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->seeElement($blockEditorAdOns->AdvanceColumnParentGeneralStackOnTabletClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralStackOnMobile);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Height Normal
    public function HeightNormalChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightNormal);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('108px', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('height');
            });
        $I->assertEquals('60px', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('108px', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Height Half-Screen
    public function HeightHalfScreenChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightHalfScreen);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('251px', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('height');
            });
        $I->assertEquals('251px', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('251px', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightNormal);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Height Full-Screen
    public function HeightFullScreenChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightFullScreen);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('462px', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('height');
            });
        $I->assertEquals('462px', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('462px', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightNormal);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Height Custom
    public function HeightCustomChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightCustom);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('108px', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('height');
            });
        $I->assertEquals('90px', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('height');
            });
        $I->assertEquals('108px', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHeightNormal);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Horizontal Align Left
    public function HorizontalAlignLeftChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHorizontalAlignLeft);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHorizontalAlignCenter);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Horizontal Align Center
    public function HorizontalAlignCenterChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHorizontalAlignCenter);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHorizontalAlignCenter);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Horizontal Align Right
    public function HorizontalAlignRightChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHorizontalAlignRight);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $AdvanceColumnParentGeneralHeightNormalOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralHeightNormalOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $AdvanceColumnParentGeneralHeightNormalOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralHeightNormalOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $AdvanceColumnParentGeneralHeightNormalOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralHorizontalAlignCenter);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Vertical Align Top
    public function VerticalAlignTopChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralVerticalAlignTop);

        $AdvanceColumnParentGeneralVerticalAlignTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div > div:nth-child(1) > div'))->getCSSValue('align-items');
            });
        $I->assertEquals('flex-start', $AdvanceColumnParentGeneralVerticalAlignTopOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralVerticalAlignTopOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div.responsive-block-editor-addons-advanced-column-child > div'))->getCSSValue('align-items');
            });
        $I->assertEquals('flex-start', $AdvanceColumnParentGeneralVerticalAlignTopOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralVerticalAlignTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div > div:nth-child(1) > div'))->getCSSValue('align-items');
            });
        $I->assertEquals('flex-start', $AdvanceColumnParentGeneralVerticalAlignTopOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
    //Test Case for Vertical Align Bottom
    public function VerticalAlignBottomChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralVerticalAlignBottom);

        $AdvanceColumnParentGeneralVerticalAlignBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div > div:nth-child(1) > div'))->getCSSValue('align-items');
            });
        $I->assertEquals('flex-end', $AdvanceColumnParentGeneralVerticalAlignBottomOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralVerticalAlignBottomOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div.responsive-block-editor-addons-advanced-column-child > div'))->getCSSValue('align-items');
            });
        $I->assertEquals('flex-end', $AdvanceColumnParentGeneralVerticalAlignBottomOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralVerticalAlignBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div > div:nth-child(1) > div'))->getCSSValue('align-items');
            });
        $I->assertEquals('flex-end', $AdvanceColumnParentGeneralVerticalAlignBottomOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralVerticalAlignTop);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
 /*
    //Test Case for Z-Index
    public function ZIndexChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                            Page\LoginPage $loginPage,
                                                            Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->pressKey($blockEditorAdOns->AdvanceColumnParentGeneralZIndex, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceColumnParentGeneralZIndex, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceColumnParentGeneralZIndexOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('z-index');
            });
        $I->assertEquals('3', $AdvanceColumnParentGeneralZIndexOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
        $I->click($blockEditorAdOns->advanceColumnParentPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

        $AdvanceColumnParentGeneralZIndexOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div'))->getCSSValue('z-index');
            });
        $I->assertEquals('3', $AdvanceColumnParentGeneralZIndexOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

        $AdvanceColumnParentGeneralZIndexOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('z-index');
            });
        $I->assertEquals('3', $AdvanceColumnParentGeneralZIndexOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralBtn);
        $I->click($blockEditorAdOns->AdvanceColumnParentGeneralZIndexReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        /*
        //Test Case for Background Color And Opacity
        public function BackgroundTypeIsChangedToColorShouldAlsoChangedForFrontEnd(AcceptanceTester $I,
                                                                                   Page\LoginPage $loginPage,
                                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeColor);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundBackgroundColorRedSelect);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $AdvanceColumnParentBackgroundTypeColorOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-color');
                });
            $I->assertEquals('rgba(205, 38, 83, 0.3)', $AdvanceColumnParentBackgroundTypeColorOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBackgroundTypeColorOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('background-color');
                });
            $I->assertEquals('rgba(205, 38, 83, 0.3)', $AdvanceColumnParentBackgroundTypeColorOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBackgroundTypeColorOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-color');
                });
            $I->assertEquals('rgba(205, 38, 83, 0.3)', $AdvanceColumnParentBackgroundTypeColorOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeColor);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundBackgroundColorClear);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundOpacityReset);
            $I->click($blockEditorAdOns->updateBtn);
        }

        //Test for Background Type - Gradient
        public function BackgroundTypeIsChangedToGradientShouldChangedForFrontEndToo(AcceptanceTester $I,
                                                                                     Page\LoginPage $loginPage,
                                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradient);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColor1Red);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColor2LightBrown);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $AdvanceColumnParentBackgroundTypeGradientOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-image');
                });
            $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $AdvanceColumnParentBackgroundTypeGradientOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBackgroundTypeGradientOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('background-image');
                });
            $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $AdvanceColumnParentBackgroundTypeGradientOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBackgroundTypeGradientOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-image');
                });
            $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $AdvanceColumnParentBackgroundTypeGradientOnPage);

            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradient);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColor1Red);
            $I->click($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColor2LightBrown);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBackgroundTypeGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->updateBtn);
        }

        //Test Case for Padding
        public function AllSidePaddingChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentSpacingBtn);

            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->AdvanceColumnParentLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->AdvanceColumnParentRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $AdvanceColumnParentPaddingOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('padding');
                });
            $I->assertEquals('24px 23px 23px 24px', $AdvanceColumnParentPaddingOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentPaddingOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('padding');
                });
            $I->assertEquals('24px 23px 23px 24px', $AdvanceColumnParentPaddingOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentPaddingOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('padding');
                });
            $I->assertEquals('24px 23px 23px 24px', $AdvanceColumnParentPaddingOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentSpacingBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentTopPaddingReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBottomPaddingReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentLeftPaddingReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentRightPaddingReset);
            $I->click($blockEditorAdOns->updateBtn);
        }

        //Test Case for Top-Margin
        public function TopMarginChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentSpacingBtn);

            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentTopMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $AdvanceColumnParentTopMarginOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('margin-top');
                });
            $I->assertEquals('4px', $AdvanceColumnParentTopMarginOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentTopMarginOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('margin-top');
                });
            $I->assertEquals('4px', $AdvanceColumnParentTopMarginOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentTopMarginOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('margin-top');
                });
            $I->assertEquals('4px', $AdvanceColumnParentTopMarginOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentSpacingBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentTopMarginReset);
            $I->click($blockEditorAdOns->updateBtn);
        }

        //Test Case for Bottom-Margin
        public function BottomMarginChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentSpacingBtn);

            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $AdvanceColumnParentBottomMarginOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('4px', $AdvanceColumnParentBottomMarginOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBottomMarginOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('4px', $AdvanceColumnParentBottomMarginOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBottomMarginOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('4px', $AdvanceColumnParentBottomMarginOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentSpacingBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBottomMarginReset);
            $I->click($blockEditorAdOns->updateBtn);
        }

        //Test Case for Border Style-Solid,Border-Width,Border-radius,Border-Color
        public function BorderStyleSolidChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleSolid);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleSolidOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('solid', $AdvanceColumnParentBorderStyleSolidOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleSolidOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('solid', $AdvanceColumnParentBorderStyleSolidOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleSolidOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('solid', $AdvanceColumnParentBorderStyleSolidOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Dotted,Border-Width,Border-radius,Border-Color
        public function BorderStyleDottedChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleDotted);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleDottedOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $AdvanceColumnParentBorderStyleDottedOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleDottedOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $AdvanceColumnParentBorderStyleDottedOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleDottedOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $AdvanceColumnParentBorderStyleDottedOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Dashed,Border-Width,Border-radius,Border-Color
        public function BorderStyleDashedChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleDashed);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleDashedOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('dashed', $AdvanceColumnParentBorderStyleDashedOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleDashedOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('dashed', $AdvanceColumnParentBorderStyleDashedOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleDashedOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('dashed', $AdvanceColumnParentBorderStyleDashedOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Double,Border-Width,Border-radius,Border-Color
        public function BorderStyleDoubleChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleDouble);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleDoubleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('double', $AdvanceColumnParentBorderStyleDoubleOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleDoubleOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('double', $AdvanceColumnParentBorderStyleDoubleOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleDoubleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('double', $AdvanceColumnParentBorderStyleDoubleOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Groove,Border-Width,Border-radius,Border-Color
        public function BorderStyleGrooveChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleGroove);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleGrooveOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('groove', $AdvanceColumnParentBorderStyleGrooveOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleGrooveOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('groove', $AdvanceColumnParentBorderStyleGrooveOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleGrooveOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('groove', $AdvanceColumnParentBorderStyleGrooveOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Inset,Border-Width,Border-radius,Border-Color
        public function BorderStyleInsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleInset);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleInsetOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('inset', $AdvanceColumnParentBorderStyleInsetOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleInsetOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('inset', $AdvanceColumnParentBorderStyleInsetOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleInsetOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('inset', $AdvanceColumnParentBorderStyleInsetOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Outset,Border-Width,Border-radius,Border-Color
        public function BorderStyleOutsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleOutset);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleOutsetOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('outset', $AdvanceColumnParentBorderStyleOutsetOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleOutsetOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('outset', $AdvanceColumnParentBorderStyleOutsetOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleOutsetOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('outset', $AdvanceColumnParentBorderStyleOutsetOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Style-Ridge,Border-Width,Border-radius,Border-Color
        public function BorderStyleRidgeChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderStyleRidge);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
    //        $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorResdSelect);

            $AdvanceColumnParentBorderStyleRidgeOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('ridge', $AdvanceColumnParentBorderStyleRidgeOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleRidgeOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-style');
                });
            $I->assertEquals('ridge', $AdvanceColumnParentBorderStyleRidgeOnFrontEnd);

            $AdvanceColumnParentBorderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnFrontEnd);

    //        $AdvanceColumnParentBorderRadiusOnFrontEnd = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnFrontEnd);

            $AdvanceColumnParentBorderColorResdSelectOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleRidgeOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-style');
                });
            $I->assertEquals('ridge', $AdvanceColumnParentBorderStyleRidgeOnPage);

            $AdvanceColumnParentBorderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-width');
                });
            $I->assertEquals('3px', $AdvanceColumnParentBorderWidthOnPage);

    //        $AdvanceColumnParentBorderRadiusOnPage = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
    //            });
    //        $I->assertEquals('2px', $AdvanceColumnParentBorderRadiusOnPage);

            $AdvanceColumnParentBorderColorResdSelectOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $AdvanceColumnParentBorderColorResdSelectOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderWidthReset);
            //$I->click($blockEditorAdOns->AdvanceColumnParentBorderRadiusReset);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderColorClear);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Shadow Position-Inset
        public function BorderShadowAndPositionInsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                      Page\LoginPage $loginPage,
                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowColorRedSelect);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxPositionInset);

            $AdvanceColumnParentBorderStyleRidgeOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
                });
            $I->assertEquals('rgb(205, 38, 83) 24px 24px 54px 24px inset', $AdvanceColumnParentBorderStyleRidgeOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleRidgeOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('box-shadow');
                });
            $I->assertEquals('rgb(205, 38, 83) 24px 24px 54px 24px inset', $AdvanceColumnParentBorderStyleRidgeOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleRidgeOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
                });
            $I->assertEquals('rgb(205, 38, 83) 24px 24px 54px 24px inset', $AdvanceColumnParentBorderStyleRidgeOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowColorClear);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxPositionInset);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }

        //Test Case for Border Shadow Position-Outset
        public function BorderShadowAndPositionOutsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)

        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);
            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowColorRedSelect);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxPositionOutset);

            $AdvanceColumnParentBorderStyleRidgeOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
                });
            $I->assertEquals('rgb(205, 38, 83) 24px 24px 54px 24px', $AdvanceColumnParentBorderStyleRidgeOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);
            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPage, 20);
            $I->click($blockEditorAdOns->advanceColumnParentPage);
            $I->waitForElement($blockEditorAdOns->advanceColumnParentPageClass,20);

            $AdvanceColumnParentBorderStyleRidgeOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-advanced-column.alignundefined > div:first-child'))->getCSSValue('box-shadow');
                });
            $I->assertEquals('rgb(205, 38, 83) 24px 24px 54px 24px', $AdvanceColumnParentBorderStyleRidgeOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
            $I->click($blockEditorAdOns->AdvanceColumnParentSelectClass);

            $AdvanceColumnParentBorderStyleRidgeOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
                });
            $I->assertEquals('rgb(205, 38, 83) 24px 24px 54px 24px', $AdvanceColumnParentBorderStyleRidgeOnPage);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowBtn);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxShadowColorClear);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->AdvanceColumnParentBorderBoxSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->click($blockEditorAdOns->AdvanceColumnParentBorderBoxPositionOutset);
            $I->click($blockEditorAdOns->updateBtn);
            $loginPage->userLogout($I);
        }
    */
    }