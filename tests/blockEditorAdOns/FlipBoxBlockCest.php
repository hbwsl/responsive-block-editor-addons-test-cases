<?php


class FlipBoxBlockCest
{
    //Test Case for Number Of Flip-Box
    public function NumberOfFlipBoxChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->flipBoxPage, 20);
        $I->click($blockEditorAdOns->flipBoxPage);
        $I->waitForElement($blockEditorAdOns->flipBoxPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        //$I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        //$I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->flipBoxSelectClass,20);
        $I->click($blockEditorAdOns->flipBoxSelectClass);
        $I->click($blockEditorAdOns->flipBoxGeneralBtn);
        $I->pressKey($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxes, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxes, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->seeElement($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxesClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->flipBoxPage, 20);
        $I->click($blockEditorAdOns->flipBoxPage);
        $I->waitForElement($blockEditorAdOns->flipBoxPageClass,20);
        $I->seeElement($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxesClassOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->flipBoxSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->flipBoxSelectClass);
        $I->click($blockEditorAdOns->flipBoxSelectClass);
        $I->seeElement($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxesClassOnPage);
        $I->click($blockEditorAdOns->flipBoxGeneralBtn);
        $I->pressKey($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxes, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->flipBoxGeneralNumberOfFlipBoxes, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
}