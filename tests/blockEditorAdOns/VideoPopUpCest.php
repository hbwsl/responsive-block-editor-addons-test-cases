<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class VideoPopUpCest
{

 /*   public function ChangingContainerWidthShouldAlsoApperInTheFrontEnd(AcceptanceTester $I,
                                                              Page\LoginPage $loginPage,
                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);
        //$I->waitForElement($blockEditorAdOns->googleMapClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click('//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button');
        $I->click($blockEditorAdOns->videoPopUpContainerOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        // Checking value on current page
        $heightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__wrapper'))->getCSSValue('max-width');
            });
        $I->assertEquals('210px', $heightOnPage);
        $I->click($blockEditorAdOns->updateBtn);

        }

    public function ChangingContainerheightShouldAlsoApperInTheFrontEnd(AcceptanceTester $I,
                                                                       Page\LoginPage $loginPage,
                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);
        //$I->waitForElement($blockEditorAdOns->googleMapClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click('//*[@id="editor"]/div/div/div[1]/div/div[2]/div[2]/div/div[3]/div/div[2]/div[1]/h2/button');
        $I->click($blockEditorAdOns->videoPopUpContainerOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpContainerHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        // Checking value on current page
        $heightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__wrapper'))->getCSSValue('height');
            });
        $I->assertEquals('310px', $heightOnPage);
        $I->click($blockEditorAdOns->updateBtn);

    }
*/
    public function SelectedBackgroundImageShouldApperInTheFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->videoPopUpOptionsOpenBtn);
        $I->click($blockEditorAdOns->videoPopUpBackgroundOptionsOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->click('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__sidebar > div > div.components-panel > div > div:nth-child(2) > div.components-panel__body.is-opened > div:nth-child(3) > button');
        $I->wait(3);
        //$I->waitForElement('#menu-item-upload',20);
        $I->click('#menu-item-upload');
        $I->wait(2);
        $I->attachFile('#__wp-uploader-id-1','spacall01.png');
        $I->wait(3);
        $I->click('#menu-item-browse');
        //$I->click('/html/body/div[4]/div[1]/div/div/div[3]/div[2]/div/ul/li[2]/div');
        //$I->wait(2);
        $I->click('#__wp-uploader-id-0 > div.media-frame-toolbar > div > div.media-toolbar-primary.search-form > button');
        $I->wait(10);
        //$I->click($blockEditorAdOns->updateBtn);
    }

   /* public function PlayBtnWithCircleStyleShouldChangeAsPerSelection(AcceptanceTester $I,
                                                           Page\LoginPage $loginPage,
                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->videoPopUpOptionsOpenBtn);
        $I->click($blockEditorAdOns->videoPopUpPlayButtonOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->videoPopUpPlayButtonWithCircle);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__play-button > svg'))->getCSSValue('width');
            });
        $I->assertEquals('33px', $sizeOnPage);

        $opacityOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__play-button > svg'))->getCSSValue('opacity');
            });
        $I->assertEquals('0.94', $opacityOnPage);
        $I->click($blockEditorAdOns->updateBtn);

        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->wait(2);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

    }

    public function PlayBtnWithOutlineStyleShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->videoPopUpOptionsOpenBtn);
        $I->click($blockEditorAdOns->videoPopUpPlayButtonOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->videoPopUpOutlinePlayButton);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__play-button > svg'))->getCSSValue('width');
            });
        $I->assertEquals('33px', $sizeOnPage);

        $opacityOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__play-button > svg'))->getCSSValue('opacity');
            });
        $I->assertEquals('0.94', $opacityOnPage);
        //$I->click($blockEditorAdOns->updateBtn);

    }

    public function PlayBtnWithVideoPlayStyleShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->videoPopUpOptionsOpenBtn);
        $I->click($blockEditorAdOns->videoPopUpPlayButtonOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->videoPopUpVideoPlayBtn);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->videoPopUpplayButtonOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__play-button > svg'))->getCSSValue('width');
            });
        $I->assertEquals('33px', $sizeOnPage);

        $opacityOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__play-button > svg'))->getCSSValue('opacity');
            });
        $I->assertEquals('0.94', $opacityOnPage);
        //$I->click($blockEditorAdOns->updateBtn);

    }
 */
   /* public function BorderDottedStyleShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->videoPopPage, 20);
        $I->click($blockEditorAdOns->videoPopPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->videoPopUpSelect, 20);
        $I->click($blockEditorAdOns->videoPopUpSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->videoPopUpOptionsOpenBtn);
        $I->click($blockEditorAdOns->videoPopUpBorderOpenBtn);
        $I->scrollTo($blockEditorAdOns->videoPopUpAdvancedOpenBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->videoPopUpBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->videoPopUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->videoPopUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        $I->fillField($blockEditorAdOns->videoPopUpBorderRadius,20);
        $I->wait(2);

        $sizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__wrapper'))->getCSSValue('border-width');
            });
        $I->assertEquals('13px', $sizeOnPage);

        $opacityOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-video-popup__wrapper'))->getCSSValue('border-radius');
            });
        $I->assertEquals('20px', $opacityOnPage);
        //$I->click($blockEditorAdOns->updateBtn);

    }
*/
}