<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;

class AccordionBlockCest
{
    ////Accordian Item Tests

    //$I->pressKey($blockEditorAdOns->accordionSelectClass,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
 /*   //$I->wait(5);
    public function ChangesMadeInSolidBorderStyleShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->waitForElement($blockEditorAdOns->accordionClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);

        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->accordionBorderColorRedSelect);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->wait(3);

        $accordionBorderStyleSolidFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $accordionBorderStyleSolidFrontEnd);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->wait(2);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->accordionBorderColorClearBtn);

        $I->click($blockEditorAdOns->updateBtn);


    }

    public function ChangesMadeInDottedBorderStyleShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->waitForElement($blockEditorAdOns->accordionClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);

        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleDotted);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        //$I->click($blockEditorAdOns->accordionBorderColorRedSelect);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->wait(3);

        $accordionBorderStyleSolidFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $accordionBorderStyleSolidFrontEnd);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->wait(2);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->accordionBorderColorClearBtn);

        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInDashedBorderStyleShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                              Page\LoginPage $loginPage,
                                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->waitForElement($blockEditorAdOns->accordionClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);

        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleDashed);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        //$I->click($blockEditorAdOns->accordionBorderColorRedSelect);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->wait(3);

        $accordionBorderStyleSolidFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $accordionBorderStyleSolidFrontEnd);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->wait(2);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->accordionBorderColorClearBtn);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInDoubleBorderStyleShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                              Page\LoginPage $loginPage,
                                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->waitForElement($blockEditorAdOns->accordionClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);

        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleDouble);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        //$I->click($blockEditorAdOns->accordionBorderColorRedSelect);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->wait(3);

        $accordionBorderStyleSolidFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $accordionBorderStyleSolidFrontEnd);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-accordion__wrap.responsive-block-editor-addons-buttons-layout-wrap > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->wait(2);

        $accordionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $accordionBorderStyleSolidOnPage);

        $accordionBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $accordionBorderThicknessOnPage);

        $accordionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $accordionBorderRadiusOnPage);

//        $accordionBorderColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
//            });
//        $I->assertEquals('rgb(205, 38, 83)', $accordionBorderColorRedSelectOnPage);
        $I->click($blockEditorAdOns->accordionSelectClass);
        $I->click($blockEditorAdOns->accordionStyleBtn);
        $I->click($blockEditorAdOns->accordionBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->accordionBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->accordionBorderColorClearBtn);
        $I->click($blockEditorAdOns->updateBtn);
    }
*/
    ////Accordian Settings Tests

    public function ChangesMadeInSpacingShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->accordionPage, 20);
        $I->click($blockEditorAdOns->accordionPage);
        $I->waitForElement($blockEditorAdOns->accordionClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->accordionSelectClass, 20);
        $I->pressKey($blockEditorAdOns->accordionSelectClass,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->accordionSpacingBtn);
        $I->pressKey($blockEditorAdOns->accordionSpacingRowGap,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->accordionSpacingRowGap,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->accordionSpacingVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->accordionSpacingVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->accordionSpacingHorizontalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->accordionSpacingHorizontalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $accordionSpacingRowGapOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div:nth-child(2) > div'))->getCSSValue('margin-bottom');
            });
        echo $accordionSpacingRowGapOnPage;
        $I->assertEquals('12px', $accordionSpacingRowGapOnPage);        //.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div:nth-child(2) > div

//        $accordionSpacingVerticalMarginOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
//            });
//        $I->assertEquals('3px', $accordionSpacingVerticalMarginOnPage);
//
//        $accordionSpacingHorizontalMarginOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
//            });
//        $I->assertEquals('4px', $accordionSpacingHorizontalMarginOnPage);

    }
}