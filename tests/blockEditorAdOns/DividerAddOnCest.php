<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class DividerAddOnCest
{
   /* public function ChangingMarginShouldApperAtFrontEnd(AcceptanceTester $I,
                                                        Page\LoginPage $loginPage,
                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->dividerPage,20);
        $I->click($blockEditorAdOns->dividerPage);
        $I->waitForElement($blockEditorAdOns->dividerClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor,20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);
        $I->click($blockEditorAdOns->dividerBlockSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->dividerBlockVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->dividerBlockVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->dividerBlockVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        // Checking value on current page
        $verticalMarginTop = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider-content'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('33px',$verticalMarginTop);
        $I->click($blockEditorAdOns->updateBtn);

        $I->wait(5);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->dividerPage,20);
        $I->click($blockEditorAdOns->dividerPage);
        $I->waitForElement($blockEditorAdOns->dividerClass,20);

        // Checking the value on Front-End
        $verticalMarginTopFrontEnd = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider-content'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('33px',$verticalMarginTopFrontEnd);
        $I->wait(2);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);

        $verticalMarginTop = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider-content'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('33px',$verticalMarginTop);

        $I->click($blockEditorAdOns->dividerBlockSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->dividerBlockVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->dividerBlockVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->dividerBlockVerticalMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->wait(2);
        $I->click($blockEditorAdOns->updateBtn);
    }


    public function ChangingHeightShouldAlsoApperInTheFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->dividerPage,20);
        $I->click($blockEditorAdOns->dividerPage);
        $I->waitForElement($blockEditorAdOns->dividerClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor,20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);
        $I->click($blockEditorAdOns->dividerBlockSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->dividerBlockHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->dividerBlockHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->dividerBlockHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        // Checking value on current page
        $heightOnPage = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider_hr'))->getCSSValue('height');
            });
        $I->assertEquals('10px',$heightOnPage);
        $I->click($blockEditorAdOns->updateBtn);

        $I->wait(5);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->dividerPage,20);
        $I->click($blockEditorAdOns->dividerPage);
        $I->waitForElement($blockEditorAdOns->dividerClass,20);

        // Checking the value on Front-End
        $heightOnFrontEnd = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider-content > hr'))->getCSSValue('height');
            });
        $I->assertEquals('10px',$heightOnFrontEnd);
        $I->wait(2);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);

        $heightOnPageEdit = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider_hr'))->getCSSValue('height');
            });
        $I->assertEquals('10px',$heightOnPageEdit);

        $I->click($blockEditorAdOns->dividerBlockSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->dividerBlockHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->dividerBlockHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->dividerBlockHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->wait(2);
        $I->click($blockEditorAdOns->updateBtn);
    }
*/
    public function ChangingWidthShouldAlsoApperInTheFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->dividerPage,20);
        $I->click($blockEditorAdOns->dividerPage);
        $I->waitForElement($blockEditorAdOns->dividerClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor,20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);
        $I->click($blockEditorAdOns->dividerBlockSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        // Checking value on current page
        $widthtOnPage = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider-content > hr'))->getCSSValue('width');
            });
        $I->assertEquals('395.188px',$widthtOnPage);
        $I->click($blockEditorAdOns->updateBtn);

        $I->wait(5);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->dividerPage,20);
        $I->click($blockEditorAdOns->dividerPage);
        $I->waitForElement($blockEditorAdOns->dividerClass,20);

        // Checking the value on Front-End
        $widthOnFrontEnd = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider-content > hr'))->getCSSValue('width');
            });
        $I->assertEquals('377px',$widthOnFrontEnd);
        $I->wait(2);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->dividerBlockSelect,20);

        $widthOnPageEdit = $I->executeInSelenium(
            function(\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-divider_hr'))->getCSSValue('width');
            });
        $I->assertEquals('395.188px',$widthOnPageEdit);

        $I->click($blockEditorAdOns->dividerBlockSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->divoderBlockWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->wait(2);
        $I->click($blockEditorAdOns->updateBtn);
    }
}