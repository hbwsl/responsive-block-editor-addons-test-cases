<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class SectionBlockCest
{
    //Test Case for Width
    public function WidthChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                        Page\LoginPage $loginPage,
                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionGeneralBtn);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $sectionGeneralZIndexOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-section-outer-wrap.background-type-gradient'))->getCSSValue('max-width');
            });
        $I->assertEquals('910px', $sectionGeneralZIndexOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionGeneralZIndexOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-section-outer-wrap.background-type-gradient'))->getCSSValue('max-width');
            });
        $I->assertEquals('910px', $sectionGeneralZIndexOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionGeneralZIndexOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-section-outer-wrap.background-type-gradient'))->getCSSValue('max-width');
            });
        $I->assertEquals('910px', $sectionGeneralZIndexOnPage);
        $I->click($blockEditorAdOns->sectionGeneralBtn);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionGeneralWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
/*
    //Test Case for Z-Index
    public function ZIndexShouldChangeForFrontEnd(AcceptanceTester $I,
                                                  Page\LoginPage $loginPage,
                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionGeneralBtn);
        $I->pressKey($blockEditorAdOns->sectionGeneralZIndex,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionGeneralZIndex,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $sectionGeneralZIndexOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-section-outer-wrap.background-type-gradient'))->getCSSValue('z-index');
            });
        $I->assertEquals('3', $sectionGeneralZIndexOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionGeneralZIndexOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-section-outer-wrap.background-type-gradient'))->getCSSValue('z-index');
            });
        $I->assertEquals('3', $sectionGeneralZIndexOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionGeneralZIndexOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-section-outer-wrap.background-type-gradient'))->getCSSValue('z-index');
            });
        $I->assertEquals('3', $sectionGeneralZIndexOnPage);
        $I->click($blockEditorAdOns->sectionGeneralBtn);
        $I->click($blockEditorAdOns->sectionGeneralZIndexReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Padding All Sides
    public function PaddingOnAllSidesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                             Page\LoginPage $loginPage,
                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionSpacingBtn);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingRight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingRight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingRight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $sectionTopPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-top');
            });
        $I->assertEquals('24px', $sectionTopPaddingOnPage);

        $sectionBottomPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('23px', $sectionBottomPaddingOnPage);

        $sectionLeftPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-left');
            });
        $I->assertEquals('24px', $sectionTopPaddingOnPage);

        $sectionRightPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-right');
            });
        $I->assertEquals('23px', $sectionRightPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionTopPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-top');
            });
        $I->assertEquals('24px', $sectionTopPaddingOnPage);

        $sectionBottomPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('23px', $sectionBottomPaddingOnPage);

        $sectionLeftPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-left');
            });
        $I->assertEquals('24px', $sectionTopPaddingOnPage);

        $sectionRightPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-right');
            });
        $I->assertEquals('23px', $sectionRightPaddingOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionTopPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-top');
            });
        $I->assertEquals('24px', $sectionTopPaddingOnPage);

        $sectionBottomPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('23px', $sectionBottomPaddingOnPage);

        $sectionLeftPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-left');
            });
        $I->assertEquals('24px', $sectionTopPaddingOnPage);

        $sectionRightPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('padding-right');
            });
        $I->assertEquals('23px', $sectionRightPaddingOnPage);

        $I->click($blockEditorAdOns->sectionSpacingBtn);
        //$I->click($blockEditorAdOns->sectionSpacingPaddingTopReset);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionSpacingPaddingTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionSpacingPaddingBottomReset);
        $I->click($blockEditorAdOns->sectionSpacingPaddingLeftReset);
        $I->click($blockEditorAdOns->sectionSpacingPaddingRightReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
/*
    //Test Case for Margin All Sides
    public function MarginOnAllSidesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                             Page\LoginPage $loginPage,
                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionSpacingBtn);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->sectionSpacingMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->sectionSpacingMarginLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginLeft,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->sectionSpacingMarginRight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginRight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginRight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $sectionTopMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-top');
            });
        $I->assertEquals('4px', $sectionTopMarginOnPage);

        $sectionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('3px', $sectionBottomMarginOnPage);

        $sectionLeftMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-left');
            });
        $I->assertEquals('4px', $sectionTopMarginOnPage);

        $sectionRightMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-right');
            });
        $I->assertEquals('3px', $sectionRightMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionTopMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-top');
            });
        $I->assertEquals('4px', $sectionTopMarginOnPage);

        $sectionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('3px', $sectionBottomMarginOnPage);

        $sectionLeftMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-left');
            });
        $I->assertEquals('4px', $sectionTopMarginOnPage);

        $sectionRightMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-right');
            });
        $I->assertEquals('3px', $sectionRightMarginOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionTopMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-top');
            });
        $I->assertEquals('4px', $sectionTopMarginOnPage);

        $sectionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('3px', $sectionBottomMarginOnPage);

        $sectionLeftMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-left');
            });
        $I->assertEquals('4px', $sectionTopMarginOnPage);

        $sectionRightMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('margin-right');
            });
        $I->assertEquals('3px', $sectionRightMarginOnPage);

        $I->click($blockEditorAdOns->sectionSpacingBtn);
        //$I->click($blockEditorAdOns->sectionSpacingMarginTopReset);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionSpacingMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionSpacingMarginBottomReset);
        $I->click($blockEditorAdOns->sectionSpacingMarginLeftReset);
        $I->click($blockEditorAdOns->sectionSpacingMarginRightReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Background Type-Color and Opacity
    public function BackgroundColorAndOpacityShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBackgroundBtn);
        $I->click($blockEditorAdOns->sectionBackgroundTypeColor);
        $I->click($blockEditorAdOns->sectionBackgroundTypeColorRedSelect);
        $I->pressKey($blockEditorAdOns->sectionBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $sectionBackgroundTypeColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.24)', $sectionBackgroundTypeColorOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBackgroundTypeColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.24)', $sectionBackgroundTypeColorOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBackgroundTypeColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.24)', $sectionBackgroundTypeColorOnPage);

        $I->click($blockEditorAdOns->sectionBackgroundBtn);
        $I->click($blockEditorAdOns->sectionBackgroundTypeColor);
        $I->click($blockEditorAdOns->sectionBackgroundTypeColorClear);
        $I->click($blockEditorAdOns->sectionBackgroundOpacityReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Background Type-Gradient and Opacity
    public function BackgroundTypeColorAndOpacityShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBackgroundBtn);
        $I->click($blockEditorAdOns->sectionBackgroundTypeGradient);
        $I->click($blockEditorAdOns->sectionBackgroundColor1RedSelect);
        $I->click($blockEditorAdOns->sectionBackgroundColor2LightBrownSelect);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBackgroundTypeGradientOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $sectionBackgroundTypeGradientOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBackgroundTypeGradientOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $sectionBackgroundTypeGradientOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBackgroundTypeGradientOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $sectionBackgroundTypeGradientOnPage);

        $I->click($blockEditorAdOns->sectionBackgroundBtn);
        $I->click($blockEditorAdOns->sectionBackgroundTypeGradient);
        $I->click($blockEditorAdOns->sectionBackgroundColor1Clear);
        $I->click($blockEditorAdOns->sectionBackgroundColor2Clear);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBackgroundGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Solid And Width And Radius
    public function ChangeBorderStyleSolidShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $sectionBorderStyleSolidOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $sectionBorderStyleSolidOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $sectionBorderStyleSolidOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleSolid);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Dotted And Width And Radius
    public function ChangeBorderStyleDottedShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleDotted);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $sectionBorderStyleDottedOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $sectionBorderStyleDottedOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $sectionBorderStyleDottedOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleDotted);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Dashed And Width And Radius
    public function ChangeBorderStyleDashedShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleDashed);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $sectionBorderStyleDashedOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $sectionBorderStyleDashedOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $sectionBorderStyleDashedOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleDashed);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Double And Width And Radius
    public function ChangeBorderStyleDoubleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleDouble);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $sectionBorderStyleDoubleOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $sectionBorderStyleDoubleOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $sectionBorderStyleDoubleOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleDouble);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Groove And Width And Radius
    public function ChangeBorderStyleGrooveShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleGroove);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $sectionBorderStyleGrooveOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $sectionBorderStyleGrooveOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $sectionBorderStyleGrooveOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleGroove);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Inset And Width And Radius
    public function ChangeBorderStyleInsetShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleInset);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $sectionBorderStyleInsetOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $sectionBorderStyleInsetOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $sectionBorderStyleInsetOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleInset);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Outset And Width And Radius
    public function ChangeBorderStyleOutsetShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleOutset);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $sectionBorderStyleOutsetOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $sectionBorderStyleOutsetOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $sectionBorderStyleOutsetOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleOutset);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Ridge And Width And Radius
    public function ChangeBorderStyleRidgeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleRidge);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBorderColorRedSelect);

        $sectionBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $sectionBorderStyleRidgeOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $sectionBorderStyleRidgeOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $sectionBorderStyleRidgeOnPage);

        $sectionBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $sectionBorderWidthOnPage);

        $sectionBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $sectionBorderRadiusOnPage);

        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBorderStyleRidge);
        $I->click($blockEditorAdOns->sectionBorderWidthReset);
        $I->click($blockEditorAdOns->sectionBorderRadiusReset);
        $I->click($blockEditorAdOns->sectionBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Border Shadow Position-Inset
    public function BorderShadowAndPositionInsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBoxShadowBtn);
        $I->click($blockEditorAdOns->sectionBoxShadowColorRedSelect);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->sectionBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->sectionBoxShadowPositionInset);

        $sectionBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 24px 24px inset', $sectionBoxShadowOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->sectionPage, 20);
        $I->click($blockEditorAdOns->sectionPage);
        $I->waitForElement($blockEditorAdOns->sectionPageClass,20);

        $sectionBoxShadowOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 24px 24px inset', $sectionBoxShadowOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->selectorfordividerblock);
        $I->click($blockEditorAdOns->selectorfordividerblock);
        $I->pressKey($blockEditorAdOns->selectorForDividerOuterClass,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $sectionBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-section-wrap.responsive-block-editor-addons-block-section.overlay-type-color.linear'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 24px 24px inset', $sectionBoxShadowOnPage);
        $I->click($blockEditorAdOns->sectionBorderBtn);
        $I->click($blockEditorAdOns->sectionBoxShadowBtn);
        $I->click($blockEditorAdOns->sectionBoxShadowColorClear);
        $I->click($blockEditorAdOns->sectionBoxShadowHorizontalReset);
        $I->click($blockEditorAdOns->sectionBoxShadowVerticalReset);
        $I->click($blockEditorAdOns->sectionBoxShadowBlurReset);
        $I->click($blockEditorAdOns->sectionBoxShadowSpreadReset);
        $I->click($blockEditorAdOns->sectionBoxShadowPositionOutset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
}