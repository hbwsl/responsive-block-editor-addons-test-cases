<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class InfoBlockAddonCest
{/*
    //Test for Change In Icon/Image Position-Above Title
    public function ChangeInIconPositionToAboveTitleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                               Page\LoginPage $loginPage,
                                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionAboveTitle);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionAboveTitleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionAboveTitleClassOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionAboveTitleClassOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Change In Icon/Image Position-Below Title
    public function ChangeInIconPositionToBelowTitleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                               Page\LoginPage $loginPage,
                                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionBelowTitle);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionBelowTitleClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionBelowTitleClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionBelowTitleClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionAboveTitle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Change In Icon/Image Position-Left of Title
    public function ChangeInIconPositionToLeftOfTitleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionLeftOfTitle);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTitleClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTitleClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTitleClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionAboveTitle);
        $I->dontSeeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTitleClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Change In Icon/Image Position-Right of Title
    public function ChangeInIconPositionToRightOfTitleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionRightOfTitle);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionRightOfTitleClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionRightOfTitleClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionRightOfTitleClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionAboveTitle);
        $I->dontSeeElement($blockEditorAdOns->infoBlockIconPositionRightOfTitleClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Change In Icon/Image Position-Left of Text and Title
    public function ChangeInIconPositionToLeftOfTextAndTitleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionRightOfTextAndTitle);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionRightOfTextAndTitleClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionRightOfTextAndTitleClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionRightOfTextAndTitleClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionAboveTitle);
        $I->dontSeeElement($blockEditorAdOns->infoBlockIconPositionRightOfTextAndTitleClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Change In Icon/Image Position-Right of Text and Title
    public function ChangeInIconPositionToRightOfTextAndTitleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                        Page\LoginPage $loginPage,
                                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionLeftOfTextAndTitle);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTextAndTitleClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTextAndTitleClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTextAndTitleClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconPositionAboveTitle);
        $I->dontSeeElement($blockEditorAdOns->infoBlockIconPositionLeftOfTextAndTitleClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Icon Size
    public function ChangeInIconSizeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->pressKey($blockEditorAdOns->infoBlockIconSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockIconSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->infoBlockIconSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->infoBlockIconSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockIconSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('span.responsive-block-editor-addons-ifb-icon'))->getCSSValue('width');
            });
        $I->assertEquals('42px', $infoBlockIconSizeOnPage);

//        $I->click($blockEditorAdOns->updateBtn);
//        $I->wait(4);
//        $I->amOnPage('/');
//        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
//        $I->click($blockEditorAdOns->infoBlockPage);
//        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
//        $infoBlockIconSizeOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-icon'))->getCSSValue('width');
//            });
//        $I->assertEquals('44px', $infoBlockIconSizeOnPage);
//
//        $I->click($blockEditorAdOns->editPageLink);
//        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
//        $I->reloadPage();
//        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
//        $I->click($blockEditorAdOns->infoBlockSelectClass);
//        $infoBlockIconSizeOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-icon'))->getCSSValue('width');
//            });
//        $I->assertEquals('44px', $infoBlockIconSizeOnPage);
//        $I->click($blockEditorAdOns->infoBlockIconSizeReset);
//        $I->click($blockEditorAdOns->updateBtn);
//        $loginPage->userLogout($I);
    }

    //Test for Background Color and Opacity
    public function ChangeInBackgroundColorShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass,20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsBtn);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsBackgroundColorBtn);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsBackgroundColorRedSelect);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBackgroundOptionsOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $infoBlockIconSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.9)', $infoBlockIconSizeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockIconSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.9)', $infoBlockIconSizeOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockIconSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.9)', $infoBlockIconSizeOnPage);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsBtn);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsBackgroundColorBtn);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsBackgroundColorClear);
        $I->click($blockEditorAdOns->infoBlockBackgroundOptionsOpacityReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Solid And Width
    public function ChangeBorderStyleSolidShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockBorderStyleSolidOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockBorderStyleSolidOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockBorderStyleSolidOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleSolid);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Dotted And Width
    public function ChangeBorderStyleDottedShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleDotted);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockBorderStyleDottedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockBorderStyleDottedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockBorderStyleDottedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleDotted);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Dashed And Width
    public function ChangeBorderStyleDashedShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleDashed);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockBorderStyleDashedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockBorderStyleDashedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockBorderStyleDashedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleDashed);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Double And Width
    public function ChangeBorderStyleDoubleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleDouble);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockBorderStyleDoubleOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockBorderStyleDoubleOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockBorderStyleDoubleOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleDouble);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Groove And Width
    public function ChangeBorderStyleGrooveShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleGroove);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $infoBlockBorderStyleGrooveOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $infoBlockBorderStyleGrooveOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $infoBlockBorderStyleGrooveOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleGroove);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Inset And Width
    public function ChangeBorderStyleInsetShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleInset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $infoBlockBorderStyleInsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $infoBlockBorderStyleInsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $infoBlockBorderStyleInsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleInset);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Outset And Width
    public function ChangeBorderStyleOutsetShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleOutset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $infoBlockBorderStyleOutsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $infoBlockBorderStyleOutsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $infoBlockBorderStyleOutsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleOutset);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Ridge And Width
    public function ChangeBorderStyleRidgeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleRidge);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderColorRedSelect);

        $infoBlockBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $infoBlockBorderStyleRidgeOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $infoBlockBorderStyleRidgeOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $infoBlockBorderStyleRidgeOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderStyleRidge);
        $I->click($blockEditorAdOns->infoBlockBorderWidthReset);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->infoBlockBorderRadiusReset);
        $I->click($blockEditorAdOns->infoBlockBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Border Shadow Position-Inset
    public function BorderShadowAndPositionInsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowColorRedSelect);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowPositionInset);

        $infoBlockBorderBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 4px 4px inset', $infoBlockBorderBoxShadowOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderBoxShadowOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 4px 4px inset', $infoBlockBorderBoxShadowOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 4px 4px inset', $infoBlockBorderBoxShadowOnPage);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowColorClear);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowHorizontalReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowVerticalReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowBlurReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowSpreadReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowPositionOutset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Border Shadow Position-Outset
    public function BorderShadowAndPositionOutsetChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowColorRedSelect);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockBorderBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowPositionOutset);

        $infoBlockBorderBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 4px 4px', $infoBlockBorderBoxShadowOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);

        $infoBlockBorderBoxShadowOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 4px 4px', $infoBlockBorderBoxShadowOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);

        $infoBlockBorderBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-info-block.responsive-block-editor-addons-infobox__outer-wrap.responsive-blocks-block-team'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 4px 4px 4px 4px', $infoBlockBorderBoxShadowOnPage);
        $I->click($blockEditorAdOns->infoBlockBorderBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowBtn);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowColorClear);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowHorizontalReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowVerticalReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowBlurReset);
        $I->click($blockEditorAdOns->infoBlockBorderBoxShadowSpreadReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Position-After Title
    public function SeparatorPositionAfterTitleChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfteTitleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfteTitleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfteTitleClassOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Position-After Image
    public function SeparatorPositionAfterImageChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfterImage);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterImageClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterImageClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterImageClassOnPage);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Position-After Prefix
    public function SeparatorPositionAfterPrefixChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfterPrefix);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterPrefixClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterPrefixClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterPrefixClassOnPage);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Position-After Description
    public function SeparatorPositionAfterDescriptionChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfterDescription);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterDescriptionClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterDescriptionClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->seeElement($blockEditorAdOns->infoBlockSeparatorPositionAfterDescriptionClassOnPage);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Style-Solid
    public function SeparatorStyleSolidChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);

        $infoBlockSeparatorStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockSeparatorStyleSolidOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockSeparatorStyleSolidOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockSeparatorStyleSolidOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Style-Double
    public function SeparatorStyleDoubleChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleDouble);

        $infoBlockSeparatorStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockSeparatorStyleDoubleOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockSeparatorStyleDoubleOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockSeparatorStyleDoubleOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Style-Dotted
    public function SeparatorStyleDottedChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleDotted);

        $infoBlockSeparatorStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockSeparatorStyleDottedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockSeparatorStyleDottedOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockSeparatorStyleDottedOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Style-Dashed
    public function SeparatorStyleDashedChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleDashed);

        $infoBlockSeparatorStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockSeparatorStyleDashedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorStyleDashedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockSeparatorStyleDashedOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockSeparatorStyleDashedOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Thickness
    public function SeparatorThicknessChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorThickness, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorThickness, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockSeparatorThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-width');
            });
        $I->assertEquals('4px', $infoBlockSeparatorThicknessOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorThicknessOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-width');
            });
        $I->assertEquals('4px', $infoBlockSeparatorThicknessOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-width');
            });
        $I->assertEquals('4px', $infoBlockSeparatorThicknessOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->infoBlockSeparatorThicknessReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Width
    public function SeparatorWidthChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockSeparatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('width');
            });
        $I->assertEquals('200.594px', $infoBlockSeparatorWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('width');
            });
        $I->assertEquals('190.391px', $infoBlockSeparatorWidthOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('width');
            });
        $I->assertEquals('200.594px', $infoBlockSeparatorWidthOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->infoBlockSeparatorWidthReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Separator Color
    public function SeparatorColorChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->infoBlockSeparatorColorRedSelect);
        $infoBlockSeparatorColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $infoBlockSeparatorColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $infoBlockSeparatorColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div > div > div > div:nth-child(3) > div.responsive-block-editor-addons-ifb-separator'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $infoBlockSeparatorColorRedSelectOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorPositionAfteTitle);
        $I->click($blockEditorAdOns->infoBlockSeparatorStyleSolid);
        $I->click($blockEditorAdOns->infoBlockSeparatorColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Content Prefix Color
    public function PrefixColorChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockContentBtn);
        $I->click($blockEditorAdOns->infoBlockContentEnablePrefixBtn);
        $I->dontSeeElement($blockEditorAdOns->infoBlockContentPrefixClass);
        $I->click($blockEditorAdOns->infoBlockContentEnablePrefixBtn);
        $I->click($blockEditorAdOns->infoBlockContentPrefixColorRedSelect);
        $infoBlockPrefixColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title-prefix'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockPrefixColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockPrefixColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-title-prefix'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockPrefixColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockPrefixColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title-prefix'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockPrefixColorRedSelectOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockContentBtn);
        $I->click($blockEditorAdOns->infoBlockContentPrefixColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Content Description Color
    public function DescriptionColorChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockContentBtn);
        $I->click($blockEditorAdOns->infoBlockContentEnableDescriptionBtn);
        $I->dontSeeElement($blockEditorAdOns->infoBlockContentDescriptionClass);
        $I->click($blockEditorAdOns->infoBlockContentEnableDescriptionBtn);
        $I->click($blockEditorAdOns->infoBlockContentDescriptionColorRedSelect);
        $infoBlockDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-desc'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockDescriptionColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-desc'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockDescriptionColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-desc'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockContentBtn);
        $I->click($blockEditorAdOns->infoBlockContentDescriptionColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Content Title Color
    public function TitleColorChangesShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockContentBtn);
        $I->click($blockEditorAdOns->infoBlockContentEnableTitleBtn);
        $I->dontSeeElement($blockEditorAdOns->infoBlockContentTitleClass);
        $I->click($blockEditorAdOns->infoBlockContentEnableTitleBtn);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->infoBlockContentTitleColorRedSelect);
        $infoBlockTitleColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockTitleColorRedSelectOnPage);

        $infoBlockContentTitleFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title'))->getCSSValue('font-size');
            });
        $I->assertEquals('30px', $infoBlockContentTitleFontSizeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockTitleColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-title'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockTitleColorRedSelectOnFrontEnd);

        $infoBlockContentTitleFontSizeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-title'))->getCSSValue('font-size');
            });
        $I->assertEquals('30px', $infoBlockContentTitleFontSizeOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockTitleColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $infoBlockTitleColorRedSelectOnPage);

        $infoBlockContentTitleFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title'))->getCSSValue('font-size');
            });
        $I->assertEquals('30px', $infoBlockContentTitleFontSizeOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockContentBtn);
        $I->click($blockEditorAdOns->infoBlockContentTitleColorClear);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockContentTitleFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Prefix Bottom Margin
    public function PrefixBottomMarginShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->pressKey($blockEditorAdOns->infoBlockPrefixBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockPrefixBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockPrefixBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockPrefixBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockPrefixBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title-prefix'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('9px', $infoBlockPrefixBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockPrefixBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-title-prefix'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('9px', $infoBlockPrefixBottomMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockPrefixBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title-prefix'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('9px', $infoBlockPrefixBottomMarginOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->click($blockEditorAdOns->infoBlockPrefixBottomMarginReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Title Bottom Margin
    public function TitleBottomMarginShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->pressKey($blockEditorAdOns->infoBlockTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockTitleBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockTitleBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockTitleBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-title'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockTitleBottomMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockTitleBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-ifb-title'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockTitleBottomMarginOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->click($blockEditorAdOns->infoBlockTitleBottomMarginReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Separator Bottom Margin
    public function SeparatorBottomMarginShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockSeparatorBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockSeparatorBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-separator'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockSeparatorBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockSeparatorBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-separator'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockSeparatorBottomMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockSeparatorBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-separator'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockSeparatorBottomMarginOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->click($blockEditorAdOns->infoBlockSeparatorBottomMarginReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
    //Test Case for Image all Side Margin
    public function ImageAllSideMarginShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->pressKey($blockEditorAdOns->infoBlockImageTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->infoBlockImageRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $infoBlockImageAllSIdeMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-image-icon-content.responsive-block-editor-addons-ifb-imgicon-wrap'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockImageAllSIdeMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->infoBlockPage, 20);
        $I->click($blockEditorAdOns->infoBlockPage);
        $I->waitForElement($blockEditorAdOns->infoBlockPageClass,20);
        $infoBlockImageAllSIdeMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-image-icon-content.responsive-block-editor-addons-ifb-imgicon-wrap'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockImageAllSIdeMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->infoBlockSelectClass, 20);
        $I->click($blockEditorAdOns->infoBlockSelectClass);
        $infoBlockImageAllSIdeMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-ifb-image-icon-content.responsive-block-editor-addons-ifb-imgicon-wrap'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('14px', $infoBlockImageAllSIdeMarginOnPage);

        $I->click($blockEditorAdOns->infoBlockIconBtn);
        $I->click($blockEditorAdOns->infoBlockSpacingBtn);
        $I->pressKey($blockEditorAdOns->infoBlockImageTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->infoBlockImageRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

}