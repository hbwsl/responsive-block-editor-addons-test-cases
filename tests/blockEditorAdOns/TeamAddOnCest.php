<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class TeamAddOnCest
{/*
    public function NumberOfTeamMemberBoxesShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralNumberOfTeamMemberBoxes,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralNumberOfTeamMemberBoxes,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->wait(4);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(4);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralNumberOfTeamMemberBoxes,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralNumberOfTeamMemberBoxes,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function GutterSmallSelectedShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralGutterSmallSelected);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterSmallClass);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterSmallClass);
        $I->wait(4);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterSmallClass);

    }

    public function GutterMediumSelectedShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralGutterMediumSelected);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterMediumClass);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterMediumClass);
        $I->wait(4);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterMediumClass);

    }

    public function GutterLargeSelectedShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralGutterLargeSelected);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterLargeClass);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterLargeClass);
        $I->wait(4);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterLargeClass);

    }

    public function GutterHugeSelectedShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralGutterHugeSelected);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterHugeClass);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterHugeClass);
        $I->wait(4);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->teamGeneralGutterHugeClass);

    }

    public function ChangesMadeInBorderWidthShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $generalBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-width');
            });
        $I->assertEquals('6px', $generalBorderWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBorderWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-width');
            });
        $I->assertEquals('6px', $generalBorderWidthOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-width');
            });
        $I->assertEquals('6px', $generalBorderWidthOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ChangesMadeInBorderRadiusShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                       Page\LoginPage $loginPage,
                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $generalBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-radius');
            });
        $I->assertEquals('6px', $generalBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBorderRadiusOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-radius');
            });
        $I->assertEquals('6px', $generalBorderRadiusOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-radius');
            });
        $I->assertEquals('6px', $generalBorderRadiusOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ChangesMadeInPaddingShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $generalPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('padding');
            });
        $I->assertEquals('6px', $generalPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('padding');
            });
        $I->assertEquals('6px', $generalPaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('padding');
            });
        $I->assertEquals('6px', $generalPaddingOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);

        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ChangesMadeInColumnBackgroundColorAndBackgroundColorOpacityShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                                                         Page\LoginPage $loginPage,
                                                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralLightGrayBackgroundColorSelect);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $generalOpacityOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(100deg, rgba(220, 215, 202, 0.92) 0%, rgba(220, 215, 202, 0.92) 100%), url("http://localhost/responsiveaddons/wp-admin/undefined")', $generalOpacityOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalOpacityOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(100deg, rgba(220, 215, 202, 0.92) 0%, rgba(220, 215, 202, 0.92) 100%), url("http://localhost/responsiveaddons/undefined")', $generalOpacityOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalOpacityOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(100deg, rgba(220, 215, 202, 0.92) 0%, rgba(220, 215, 202, 0.92) 100%), url("http://localhost/responsiveaddons/wp-admin/undefined")', $generalOpacityOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralLightGrayBackgroundColorResetBtn);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->teamGeneralBackGroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ChangesMadeInColumnBackgroundPositionShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                                   Page\LoginPage $loginPage,
                                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionLeftTop);

        $generalBackGroundPositionLeftTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 0%, 0% 0%', $generalBackGroundPositionLeftTopOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionLeftTopFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 0%, 0% 0%', $generalBackGroundPositionLeftTopFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionLeftTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 0%, 0% 0%', $generalBackGroundPositionLeftTopOnPage);


        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionLeftCenter);

        $generalBackGroundPositionLeftCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 50%, 0% 50%', $generalBackGroundPositionLeftCenterOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionLeftCenterFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 50%, 0% 50%', $generalBackGroundPositionLeftCenterFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionLeftCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 50%, 0% 50%', $generalBackGroundPositionLeftCenterOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionLeftBottom);

        $generalBackGroundPositionLeftBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 100%, 0% 100%', $generalBackGroundPositionLeftBottomOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionLeftBottomFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 100%, 0% 100%', $generalBackGroundPositionLeftBottomFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionLeftBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('0% 100%, 0% 100%', $generalBackGroundPositionLeftBottomOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionRightTop);

        $generalBackGroundPositionRightTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 0%, 100% 0%', $generalBackGroundPositionRightTopOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionRightTopFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 0%, 100% 0%', $generalBackGroundPositionRightTopFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionRightTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 0%, 100% 0%', $generalBackGroundPositionRightTopOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionRightCenter);

        $generalBackGroundPositionRightCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 50%, 100% 50%', $generalBackGroundPositionRightCenterOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionRightCenterFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 50%, 100% 50%', $generalBackGroundPositionRightCenterFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionRightCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 50%, 100% 50%', $generalBackGroundPositionRightCenterOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionRightBottom);

        $generalBackGroundPositionRightBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 100%, 100% 100%', $generalBackGroundPositionRightBottomOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionRightBottomFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 100%, 100% 100%', $generalBackGroundPositionRightBottomFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionRightBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('100% 100%, 100% 100%', $generalBackGroundPositionRightBottomOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionCenterTop);

        $generalBackGroundPositionCenterTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 0%, 50% 0%', $generalBackGroundPositionCenterTopOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionCenterTopFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 0%, 50% 0%', $generalBackGroundPositionCenterTopFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionCenterTopOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 0%, 50% 0%', $generalBackGroundPositionCenterTopOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionCenterCenter);

        $generalBackGroundPositionCenterCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 50%, 50% 50%', $generalBackGroundPositionCenterCenterOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionCenterCenterFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 50%, 50% 50%', $generalBackGroundPositionCenterCenterFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionCenterCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 50%, 50% 50%', $generalBackGroundPositionCenterCenterOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundPositionCenterBottom);

        $generalBackGroundPositionCenterBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 100%, 50% 100%', $generalBackGroundPositionCenterBottomOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundPositionCenterBottomFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 100%, 50% 100%', $generalBackGroundPositionCenterBottomFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundPositionCenterBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-position');
            });
        $I->assertEquals('50% 100%, 50% 100%', $generalBackGroundPositionCenterBottomOnPage);

    }

    public function ChangesMadeInColumnBackgroundRepeatShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                                   Page\LoginPage $loginPage,
                                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
       /* $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundRepeatInitial);

        $generalRepeatInitialOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalRepeatInitialOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalRepeatInitialFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalRepeatInitialFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalRepeatInitialOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalRepeatInitialOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundRepeatRepeat);

        $generalRepeatRepeatOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalRepeatRepeatOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalRepeatRepeatFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalRepeatRepeatFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalRepeatRepeatOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalRepeatRepeatOnPage);

    }

    public function ChangesMadeInColumnBackgroundSizeAttachmentShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeAttachmentFixed);

        $generalBackGroundSizeAttachmentFixedOnPage = $I->executeInSelenium(
             function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                 return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-attachment');
             });
        $I->assertEquals('fixed, fixed', $generalBackGroundSizeAttachmentFixedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeAttachmentFixedFrontEnd = $I->executeInSelenium(
             function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                 return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-attachment');
             });
        $I->assertEquals('fixed, fixed', $generalBackGroundSizeAttachmentFixedFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeAttachmentFixedOnPage = $I->executeInSelenium(
             function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                 return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-attachment');
             });
        $I->assertEquals('fixed, fixed', $generalBackGroundSizeAttachmentFixedOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeAttachmentScroll);

        $generalBackGroundSizeAttachmentScrollOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-attachment');
            });
        $I->assertEquals('scroll, scroll', $generalBackGroundSizeAttachmentScrollOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeAttachmentScrollFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-attachment');
            });
        $I->assertEquals('scroll, scroll', $generalBackGroundSizeAttachmentScrollFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeAttachmentScrollOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-attachment');
            });
        $I->assertEquals('scroll, scroll', $generalBackGroundSizeAttachmentScrollOnPage);

    }

    public function ChangesMadeInColumnBackgroundSizeShouldReflectedInFrontEnd(AcceptanceTester $I,
                                                                               Page\LoginPage $loginPage,
                                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeInitial);

        $generalBackGroundSizeInitiaOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeInitiaOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeInitiaFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeInitiaFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeInitiaOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeInitiaOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeCover);

        $generalBackGroundSizeCoverOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('cover, cover', $generalBackGroundSizeCoverOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeCoverFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('cover, cover', $generalBackGroundSizeCoverFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeCoverOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('cover, cover', $generalBackGroundSizeCoverOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeContain);

        $generalBackGroundSizeContainOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('contain, contain', $generalBackGroundSizeContainOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeContainFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('contain, contain', $generalBackGroundSizeContainFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeContainOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('contain, contain', $generalBackGroundSizeContainOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeAuto);

        $generalBackGroundSizeAutoOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeAutoOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeAutoFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeAutoFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeAutoOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeAutoOnPage);
//
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamGeneralBtn);
        $I->click($blockEditorAdOns->teamGeneralColumnBackgroundBtn);
        $I->click($blockEditorAdOns->teamGeneralBackGroundSizeInherit);

        $generalBackGroundSizeInheritOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeInheritOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $generalBackGroundSizeInheritFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeInheritFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $generalBackGroundSizeInheritOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('background-size');
            });
        $I->assertEquals('auto, auto', $generalBackGroundSizeInheritOnPage);
    }

    public function AfterHidingSocialIconsShouldAlsoReflectedInFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamSocialBtn);

        $I->click($blockEditorAdOns->teamSocialTwitterToggleBtn);
        $I->click($blockEditorAdOns->teamSocialLinkedinToggleBtn);
        $I->click($blockEditorAdOns->teamSocialEmailToggleBtn);
        $I->click($blockEditorAdOns->teamSocialPintrestToggleBtn);

        $I->dontSeeElement($blockEditorAdOns->teamSocialTwitterClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialLinkedinClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialEmailClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialPintrestClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->wait(3);
        $I->dontSeeElement($blockEditorAdOns->teamSocialTwitterClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialLinkedinClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialEmailClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialPintrestClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->dontSeeElement($blockEditorAdOns->teamSocialTwitterClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialLinkedinClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialEmailClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialPintrestClass);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamSocialBtn);

        $I->click($blockEditorAdOns->teamSocialTwitterToggleBtn);
        $I->click($blockEditorAdOns->teamSocialLinkedinToggleBtn);
        $I->click($blockEditorAdOns->teamSocialEmailToggleBtn);
        $I->click($blockEditorAdOns->teamSocialPintrestToggleBtn);
        $I->click($blockEditorAdOns->teamSocialFacebookToggleBtn);
        $I->click($blockEditorAdOns->teamSocialInstagramToggleBtn);
        $I->click($blockEditorAdOns->teamSocialYoutubeToggleBtn);

        $I->dontSeeElement($blockEditorAdOns->teamSocialFacebookClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialInstagramClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialYoutubeClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->wait(3);
        $I->dontSeeElement($blockEditorAdOns->teamSocialFacebookClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialInstagramClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialYoutubeClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->dontSeeElement($blockEditorAdOns->teamSocialFacebookClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialInstagramClass);
        $I->dontSeeElement($blockEditorAdOns->teamSocialYoutubeClass);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamSocialBtn);

        $I->click($blockEditorAdOns->teamSocialFacebookToggleBtn);
        $I->click($blockEditorAdOns->teamSocialInstagramToggleBtn);
        $I->click($blockEditorAdOns->teamSocialYoutubeToggleBtn);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function IfRedColorIsSeectedForSocialIconShouldAlsoReflectedInFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->waitForElement($blockEditorAdOns->teamPageClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamSocialBtn);
        $I->click($blockEditorAdOns->teamSocialIconRedColorSelect);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $teamSocialIconRedColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.dashicons.dashicons-twitter'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $teamSocialIconRedColorSelectOnPage);

        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->teamPage, 20);
        $I->click($blockEditorAdOns->teamPage);
        $I->wait(3);

        $teamSocialIconRedColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.dashicons.dashicons-twitter'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $teamSocialIconRedColorSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

        $teamSocialIconRedColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.dashicons.dashicons-twitter'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $teamSocialIconRedColorSelectOnPage);

        $I->click($blockEditorAdOns->teamSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->teamSocialBtn);
        $I->click($blockEditorAdOns->teamSocialIconColorClear);
        $I->click($blockEditorAdOns->updateBtn);

    }


        public function ChangesInColorSettingShouldAlsoReflectedInFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->teamPage, 20);
            $I->click($blockEditorAdOns->teamPage);
            $I->waitForElement($blockEditorAdOns->teamPageClass);
            $I->wait(3);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

            $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
            $I->click($blockEditorAdOns->teamSelectClass);
            $I->wait(2);
            $I->click($blockEditorAdOns->teamColorSettings);
            $I->click($blockEditorAdOns->teamColorSettingsBorderColorRedSelected);
            $I->click($blockEditorAdOns->teamColorSettingsTitleColorBrownSelected);
            $I->click($blockEditorAdOns->teamColorSettingsDesignationColorLightBrownSelected);
            $I->click($blockEditorAdOns->teamColorSettingsDescriptionColorBlackSelected);

            $borderColorRedOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderColorRedOnPage);

            $titleColorBrownOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-name.keep-placeholder-on-focus'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(109, 109, 109, 1)', $titleColorBrownOnPage);

            $designationColorLightBrownOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-designation.keep-placeholder-on-focus'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(220, 215, 202, 1)', $designationColorLightBrownOnPage);

            $descriptionColorBlackOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-description.keep-placeholder-on-focus'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(0, 0, 0, 1)', $descriptionColorBlackOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->teamPage, 20);
            $I->click($blockEditorAdOns->teamPage);
            $I->wait(3);

            $borderColorRedOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderColorRedOnFrontEnd);

            $titleColorBrownOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-name'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(109, 109, 109, 1)', $titleColorBrownOnFrontEnd);

            $designationColorLightBrownOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-designation'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(220, 215, 202, 1)', $designationColorLightBrownOnFrontEnd);

            $descriptionColorBlackOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-description'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(0, 0, 0, 1)', $descriptionColorBlackOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

            $borderColorRedOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-team.responsive-block-editor-addons-font-size-full.responsive-block-editor-addons-block-team.image-shape-undefined'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderColorRedOnPage);

            $titleColorBrownOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-name.keep-placeholder-on-focus'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(109, 109, 109, 1)', $titleColorBrownOnPage);

            $designationColorLightBrownOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-designation.keep-placeholder-on-focus'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(220, 215, 202, 1)', $designationColorLightBrownOnPage);

            $descriptionColorBlackOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-description.keep-placeholder-on-focus'))->getCSSValue('color');
                });
            $I->assertEquals('rgba(0, 0, 0, 1)', $descriptionColorBlackOnPage);

            $I->click($blockEditorAdOns->teamSelectClass);
            $I->wait(2);
            $I->click($blockEditorAdOns->teamColorSettings);
            $I->click($blockEditorAdOns->teamColorSettingsBorderColorClearBtn);
            $I->click($blockEditorAdOns->teamColorSettingsTitleColorClearBtn);
            $I->click($blockEditorAdOns->teamColorSettingsDesignationColorClearBtn);
            $I->click($blockEditorAdOns->teamColorSettingsDescriptionColorClearBtn);
            $I->click($blockEditorAdOns->updateBtn);

        }

        public function ChangesMadeInSpacingShouldAlsoReflectedInFrontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->teamPage, 20);
            $I->click($blockEditorAdOns->teamPage);
            $I->waitForElement($blockEditorAdOns->teamPageClass);
            $I->wait(3);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
            $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

            $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
            $I->click($blockEditorAdOns->teamSelectClass);
            $I->wait(2);
            $I->click($blockEditorAdOns->teamSpacingBtn);
            $I->pressKey($blockEditorAdOns->teamTitleBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamTitleBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamDesignationBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamDesignationBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamDescriptionBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamDescriptionBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamInterSocialIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamInterSocialIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamImageMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamImageMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamImageMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->teamImageMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $teamTitleBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-name.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamTitleBottomSpacing);

            $teamDesignationBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-designation.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamDesignationBottomSpacing);

            $teamDescriptionBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-description.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamDescriptionBottomSpacing);

            $teamInterSocialIconSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-social-icons.edit-block > li'))->getCSSValue('margin-left');
                });
            $I->assertEquals('2px', $teamInterSocialIconSpacing);

            $teamImageMarginTop = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-avatar'))->getCSSValue('margin-top');
                });
            $I->assertEquals('2px', $teamImageMarginTop);

            $teamInterSocialIconSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-avatar'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamInterSocialIconSpacing);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->teamPage, 20);
            $I->click($blockEditorAdOns->teamPage);
            $I->wait(3);

            $teamTitleBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-name'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamTitleBottomSpacing);

            $teamDesignationBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-designation'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamDesignationBottomSpacing);

            $teamDescriptionBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-description'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamDescriptionBottomSpacing);

            $teamInterSocialIconSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-social-icons > li'))->getCSSValue('margin-left');
                });
            $I->assertEquals('2px', $teamInterSocialIconSpacing);

    //        $teamImageMarginTop = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-avatar'))->getCSSValue('margin-top');
    //            });
    //        $I->assertEquals('20px', $teamImageMarginTop);
    //
    //        $teamInterSocialIconSpacing = $I->executeInSelenium(
    //            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
    //                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-avatar'))->getCSSValue('margin-bottom');
    //            });
    //        $I->assertEquals('20px', $teamInterSocialIconSpacing);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->teamSelectClass, 20);

            $teamTitleBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-name.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamTitleBottomSpacing);

            $teamDesignationBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-designation.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamDesignationBottomSpacing);

            $teamDescriptionBottomSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-team-description.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamDescriptionBottomSpacing);

            $teamInterSocialIconSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-social-icons.edit-block > li'))->getCSSValue('margin-left');
                });
            $I->assertEquals('2px', $teamInterSocialIconSpacing);

            $teamImageMarginTop = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-avatar'))->getCSSValue('margin-top');
                });
            $I->assertEquals('2px', $teamImageMarginTop);

            $teamInterSocialIconSpacing = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-team-avatar'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $teamInterSocialIconSpacing);

            $I->click($blockEditorAdOns->teamSelectClass);
            $I->wait(2);
            $I->click($blockEditorAdOns->teamSpacingBtn);
            $I->pressKey($blockEditorAdOns->teamTitleBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamTitleBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamDesignationBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamDesignationBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamDescriptionBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamDescriptionBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamInterSocialIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamInterSocialIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamImageMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamImageMarginTop,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamImageMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->teamImageMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->click($blockEditorAdOns->updateBtn);
        }
    */


}