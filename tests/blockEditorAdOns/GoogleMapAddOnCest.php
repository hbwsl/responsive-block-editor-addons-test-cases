<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class GoogleMapAddOnCest
{

    public function ChangingWidthShouldAlsoApperInTheFrontEnd(AcceptanceTester $I,
                                                              Page\LoginPage $loginPage,
                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->googleMapPage, 20);
        $I->click($blockEditorAdOns->googleMapPage);
        $I->wait(5);
        //$I->waitForElement($blockEditorAdOns->googleMapClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->googleMapSelect, 20);
        $I->click($blockEditorAdOns->googleMapSelect);
        $I->wait(2);

//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(2);

        // Checking value on current page
        $heightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-googlemap.responsive-block-editor-addons-block-googlemap > div'))->getCSSValue('height');
            });
        $I->assertEquals('300px', $heightOnPage);
        /*$I->click($blockEditorAdOns->updateBtn);

        $I->wait(5);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->googleMapPage, 20);
        $I->click($blockEditorAdOns->googleMapPage);
        $I->wait(5);

        // Checking the value on Front-End
        $heightOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-googlemap.responsive-block-editor-addons-block-googlemap > iframe'))->getCSSValue('min-height');
            });
        $I->assertEquals('300px', $heightOnFrontEnd);
        $I->wait(2);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->googleMapSelect, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->googleMapSelect, 20);

        $heightOnPageEdit = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-googlemap.responsive-block-editor-addons-block-googlemap'))->getCSSValue('height');
            });
        $I->assertEquals('300px', $heightOnPageEdit);

        $I->click($blockEditorAdOns->googleMapSelect);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->googleMapWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->wait(2);
        $I->click($blockEditorAdOns->updateBtn);
    */}
}