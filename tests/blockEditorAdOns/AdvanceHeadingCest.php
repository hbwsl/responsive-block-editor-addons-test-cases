<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class AdvanceHeadingCest
{/*
    public function HeadingToggleBtnTest(AcceptanceTester $I,
                                         Page\LoginPage $loginPage,
                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingToggleButton);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingClass);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingToggleButton);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingClass);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function SubHeadingToggleBtnTest(AcceptanceTester $I,
                                         Page\LoginPage $loginPage,
                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralSubHeadingToggleButton);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralSubHeadingClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralSubHeadingClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralSubHeadingClass);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralSubHeadingToggleButton);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralSubHeadingClass);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function SepratorToggleBtnTest(AcceptanceTester $I,
                                          Page\LoginPage $loginPage,
                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralSeparatorButton);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralSeparatorClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralSeparatorClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->dontSeeElement($blockEditorAdOns->advanceHeadingGeneralSeparatorClass);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralSeparatorButton);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralSeparatorClass);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function IfH1HeadingTagSelectedThenItShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH1);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH1Class);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH1Class);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH1Class);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfH2HeadingTagSelectedThenItShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);
    }

    public function IfH3HeadingTagSelectedThenItShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH3Class);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH3Class);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH3Class);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfH4HeadingTagSelectedThenItShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH4);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH4Class);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH4Class);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH4Class);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfH5HeadingTagSelectedThenItShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH5);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH5Class);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH5Class);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH5Class);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfH6HeadingTagSelectedThenItShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH6);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH6Class);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH6Class);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH6Class);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingGeneralButton);
        $I->click($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2);
        $I->SeeElement($blockEditorAdOns->advanceHeadingGeneralHeadingTagH2Class);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesinHeadingBottomSpacingShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSpacingButton);
        $I->pressKey($blockEditorAdOns->advanceHeadingBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('18px', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-title-text'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('18px', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('18px', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSpacingButton);
        $I->click($blockEditorAdOns->advanceHeadingBottomSpacingClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesinHeadingBottomSpacingShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSpacingButton);
        $I->pressKey($blockEditorAdOns->advanceHeadingSepratorBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSepratorBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSepratorBottomSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('18px', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('18px', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('18px', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSpacingButton);
        $I->click($blockEditorAdOns->advanceHeadingSepratorBottomSpacingClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToSolidShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('solid', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('solid', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('solid', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToDashedShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                              Page\LoginPage $loginPage,
                                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleDashed);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('dashed', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('dashed', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('dashed', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToDottedShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleDotted);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('dotted', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('dotted', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('dotted', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToDoubleShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleDouble);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('double', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('double', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('double', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToGrooveShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleGroove);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('groove', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('groove', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('groove', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToInsetShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleInset);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('inset', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('inset', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('inset', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }


    public function ChangesMadeInSepratorStyleToOutsetShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleOutset);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('outset', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('outset', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('outset', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorStyleToRidgeShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleRidge);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('ridge', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('ridge', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-style');
            });
        $I->assertEquals('ridge', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorStyleSolid);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorThicknessShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->pressKey($blockEditorAdOns->advanceHeadingSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('5px', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingBottomSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('5px', $advanceHeadingBottomSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingBottomSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('5px', $advanceHeadingBottomSpacingOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorThicknessReset);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorWidthShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->pressKey($blockEditorAdOns->advanceHeadingSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $advanceHeadingSeparatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('width');
            });
        $I->assertEquals('22px', $advanceHeadingSeparatorWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingSeparatorWidthFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('width');
            });
        $I->assertEquals('22px', $advanceHeadingSeparatorWidthFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingSeparatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('width');
            });
        $I->assertEquals('22px', $advanceHeadingSeparatorWidthOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorWidthReset);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInSepratorColorShouldChangeForFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorColorRed);

        $advanceHeadingSeparatorColorRedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-bottom-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $advanceHeadingSeparatorColorRedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingSeparatorColorRedFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-bottom-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $advanceHeadingSeparatorColorRedFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingSeparatorColorRedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-seperator'))->getCSSValue('border-bottom-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $advanceHeadingSeparatorColorRedOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorButton);
        $I->click($blockEditorAdOns->advanceHeadingSeparatorColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }


    public function ChangesMadeInHeadingTypoGraphyShouldAlsoRefectInFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingTypographyBtn);
        $I->click($blockEditorAdOns->advanceHeadingHeadingTypographyBtn);

        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(1);
        $I->click($blockEditorAdOns->advanceHeadingHeadingTypographyFontWeight500);

        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->advanceHeadingHeadingTypographyRedColorSelect);

        $advanceHeadingHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('30px', $advanceHeadingHeadingTypographyFontSizeOnPage);

        $advanceHeadingHeadingTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $advanceHeadingHeadingTypographyFontWeight500OnPage);
        $advanceHeadingHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('120px', $advanceHeadingHeadingTypographyLineHeightOnPage);
        $advanceHeadingHeadingTypographyLeterSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('letter-spacing');
            });
        $I->assertEquals('1.3px', $advanceHeadingHeadingTypographyLeterSpacingOnPage);
        $advanceHeadingHeadingTypographyRedColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $advanceHeadingHeadingTypographyRedColorSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingHeadingTypographyFontSizeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-title-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('30px', $advanceHeadingHeadingTypographyFontSizeOnFrontEnd);

        $advanceHeadingHeadingTypographyFontWeight500OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-title-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $advanceHeadingHeadingTypographyFontWeight500OnFrontEnd);
        $advanceHeadingHeadingTypographyLineHeightOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-title-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('120px', $advanceHeadingHeadingTypographyLineHeightOnFrontEnd);
        $advanceHeadingHeadingTypographyLeterSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-title-text'))->getCSSValue('letter-spacing');
            });
        $I->assertEquals('1.3px', $advanceHeadingHeadingTypographyLeterSpacingOnFrontEnd);
        $advanceHeadingHeadingTypographyRedColorSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-title-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $advanceHeadingHeadingTypographyRedColorSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('30px', $advanceHeadingHeadingTypographyFontSizeOnPage);

        $advanceHeadingHeadingTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $advanceHeadingHeadingTypographyFontWeight500OnPage);
        $advanceHeadingHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('120px', $advanceHeadingHeadingTypographyLineHeightOnPage);
        $advanceHeadingHeadingTypographyLeterSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('letter-spacing');
            });
        $I->assertEquals('1.3px', $advanceHeadingHeadingTypographyLeterSpacingOnPage);
        $advanceHeadingHeadingTypographyRedColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-title-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $advanceHeadingHeadingTypographyRedColorSelectOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingTypographyBtn);
        $I->click($blockEditorAdOns->advanceHeadingHeadingTypographyBtn);

        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->wait(1);
        $I->click($blockEditorAdOns->advanceHeadingHeadingTypographyFontWeight600);

        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->advanceHeadingHeadingTypographyColorClearBtn);

        $I->click($blockEditorAdOns->updateBtn);

    }
*/
    public function ChangesMadeInSubHeadingTypoGraphyShouldAlsoRefectInFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->waitForElement($blockEditorAdOns->advanceHeadingClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingTypographyBtn);
        $I->click($blockEditorAdOns->advanceHeadingSubHeadingTypographyBtn);

        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(1);
        $I->click($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontWeight600);

        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->advanceHeadingSubHeadingTypographyBrownColorSelect);

        $advanceHeadingSubHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $advanceHeadingSubHeadingTypographyFontSizeOnPage);

        $advanceHeadingSubHeadingTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $advanceHeadingSubHeadingTypographyFontWeight600OnPage);
        $advanceHeadingSubHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('80px', $advanceHeadingSubHeadingTypographyLineHeightOnPage);
        $advanceHeadingSubHeadingTypographyLeterSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('letter-spacing');
            });
        $I->assertEquals('1.3px', $advanceHeadingSubHeadingTypographyLeterSpacingOnPage);
        $advanceHeadingSubHeadingTypographyBrownColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $advanceHeadingSubHeadingTypographyBrownColorSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceHeadingPage, 20);
        $I->click($blockEditorAdOns->advanceHeadingPage);
        $I->wait(3);

        $advanceHeadingSubHeadingTypographyFontSizeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-desc-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $advanceHeadingSubHeadingTypographyFontSizeOnFrontEnd);

        $advanceHeadingSubHeadingTypographyFontWeight600OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-desc-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $advanceHeadingSubHeadingTypographyFontWeight600OnFrontEnd);
        $advanceHeadingSubHeadingTypographyLineHeightOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-desc-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('80px', $advanceHeadingSubHeadingTypographyLineHeightOnFrontEnd);
        $advanceHeadingSubHeadingTypographyLeterSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-desc-text'))->getCSSValue('letter-spacing');
            });
        $I->assertEquals('1.3px', $advanceHeadingSubHeadingTypographyLeterSpacingOnFrontEnd);
        $advanceHeadingSubHeadingTypographyBrownColorSelectFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-heading-desc-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $advanceHeadingSubHeadingTypographyBrownColorSelectFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->advanceHeadingSelectClass, 20);

        $advanceHeadingSubHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $advanceHeadingSubHeadingTypographyFontSizeOnPage);

        $advanceHeadingSubHeadingTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $advanceHeadingSubHeadingTypographyFontWeight600OnPage);
        $advanceHeadingSubHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('80px', $advanceHeadingSubHeadingTypographyLineHeightOnPage);
        $advanceHeadingSubHeadingTypographyLeterSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('letter-spacing');
            });
        $I->assertEquals('1.3px', $advanceHeadingSubHeadingTypographyLeterSpacingOnPage);
        $advanceHeadingSubHeadingTypographyBrownColorSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-heading-desc-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $advanceHeadingSubHeadingTypographyBrownColorSelectOnPage);

        $I->click($blockEditorAdOns->advanceHeadingSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->advanceHeadingTypographyBtn);
        $I->click($blockEditorAdOns->advanceHeadingSubHeadingTypographyBtn);

        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->wait(1);
        $I->click($blockEditorAdOns->advanceHeadingSubHeadingTypographyFontWeight400);

        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->advanceHeadingSubHeadingTypographyLeterSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->advanceHeadingSubHeadingTypographyColorClearBtn);

        $I->click($blockEditorAdOns->updateBtn);

    }
}