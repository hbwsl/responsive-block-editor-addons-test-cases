<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class PricingTableAddOnCest
{
    /*   public function NumberOfTablesAndGutterShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
       {
           $loginPage->userLogin($I);

           $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
           $I->click($blockEditorAdOns->pricingTablePage);
           $I->wait(5);

           $I->click($blockEditorAdOns->editPageLink);
   //        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
   //        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

           $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
           $I->click($blockEditorAdOns->pricingTableSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingTableGeneralBtn);
           $I->wait(2);

           $I->pressKey($blockEditorAdOns->pricingTableNumberOfTables, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
           $I->pressKey($blockEditorAdOns->pricingTableNumberOfTables, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

           $I->click($blockEditorAdOns->pricingTableGutterMedium);

           $I->click($blockEditorAdOns->updateBtn);
           $I->wait(4);

           $I->amOnPage('/');
           $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
           $I->click($blockEditorAdOns->pricingTablePage);
           $I->scrollTo($blockEditorAdOns->pricingTableButton1);
           $I->wait(5);

           $I->click($blockEditorAdOns->editPageLink);
           $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
           $I->reloadPage();

           $I->click($blockEditorAdOns->pricingTableSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingTableGeneralBtn);
           $I->wait(2);

           $I->pressKey($blockEditorAdOns->pricingTableNumberOfTables, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
           $I->click($blockEditorAdOns->pricingTableGutterHuge);

           $I->click($blockEditorAdOns->updateBtn);
           $I->wait(4);

           $I->amOnPage('/');
           $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
           $I->click($blockEditorAdOns->pricingTablePage);
           $I->scrollTo($blockEditorAdOns->pricingTableButton1);
           $I->wait(5);

           $I->click($blockEditorAdOns->editPageLink);
           $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
           $I->reloadPage();

           $I->click($blockEditorAdOns->pricingTableSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingTableGeneralBtn);
           $I->wait(2);

           $I->pressKey($blockEditorAdOns->pricingTableNumberOfTables, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
           $I->click($blockEditorAdOns->pricingTableGutterSmall);

           $I->click($blockEditorAdOns->updateBtn);

       }

       public function AlignmentShouldChangeAsPerSelectionOnFronEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
       {
           $loginPage->userLogin($I);

           $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
           $I->click($blockEditorAdOns->pricingTablePage);
           $I->wait(5);

           $I->click($blockEditorAdOns->editPageLink);

           $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
           $I->click($blockEditorAdOns->pricingTableSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingTableGeneralBtn);
           $I->wait(2);

           $I->click($blockEditorAdOns->pricingTableAlignmentLeft);

           $alignmentLeftOnPage = $I->executeInSelenium(
               function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                   return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined'))->getCSSValue('text-align');
               });
           $I->assertEquals('left', $alignmentLeftOnPage);


           $I->click($blockEditorAdOns->updateBtn);
           $I->wait(4);

           $I->amOnPage('/');
           $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
           $I->click($blockEditorAdOns->pricingTablePage);
           $I->scrollTo($blockEditorAdOns->pricingTableButton1);
           $I->wait(5);

           $alignmentLeftOnFrontEnd = $I->executeInSelenium(
               function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                   return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined'))->getCSSValue('text-align');
               });
           $I->assertEquals('left', $alignmentLeftOnFrontEnd);


           $I->click($blockEditorAdOns->editPageLink);
           $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
           $I->reloadPage();

           $alignmentLeftOnPage = $I->executeInSelenium(
               function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                   return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined'))->getCSSValue('text-align');
               });
           $I->assertEquals('left', $alignmentLeftOnPage);

           //Checking For Right Alighnment

           $I->click($blockEditorAdOns->pricingTableSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingTableGeneralBtn);
           $I->wait(2);

           $I->click($blockEditorAdOns->pricingTableAlignmentRight);

           $alignmentLeftOnPage = $I->executeInSelenium(
               function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                   return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined'))->getCSSValue('text-align');
               });
           $I->assertEquals('right', $alignmentLeftOnPage);

           $I->click($blockEditorAdOns->updateBtn);
           $I->wait(4);

           $I->amOnPage('/');
           $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
           $I->click($blockEditorAdOns->pricingTablePage);
           $I->scrollTo($blockEditorAdOns->pricingTableButton1);
           $I->wait(5);

           $alignmentLeftOnFrontEnd = $I->executeInSelenium(
               function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                   return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined'))->getCSSValue('text-align');
               });
           $I->assertEquals('right', $alignmentLeftOnFrontEnd);


           $I->click($blockEditorAdOns->editPageLink);
           $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
           $I->reloadPage();

           $alignmentLeftOnPage = $I->executeInSelenium(
               function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                   return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table.has-text-align-center.image-shape-undefined'))->getCSSValue('text-align');
               });
           $I->assertEquals('right', $alignmentLeftOnPage);

           $I->click($blockEditorAdOns->pricingTableSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingTableGeneralBtn);
           $I->wait(2);

           $I->click($blockEditorAdOns->pricingTableAlignmentCenter);
           $I->click($blockEditorAdOns->updateBtn);

       }


    public function ToggleButtonsWorkingTest(AcceptanceTester $I,
                                             Page\LoginPage $loginPage,
                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
        $I->click($blockEditorAdOns->pricingTablePage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);

        $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
        $I->click($blockEditorAdOns->pricingTableSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingTableGeneralBtn);
        $I->wait(2);
        $I->scrollTo($blockEditorAdOns->pricingTableAlignmentCenter);

        $I->click($blockEditorAdOns->pricingTableTitleToggle);
        $I->click($blockEditorAdOns->pricingTablePriceToggle);
        $I->click($blockEditorAdOns->pricingTableSubPriceToggle);
        $I->click($blockEditorAdOns->pricingTableButtonToggle);

        $I->dontSeeElement($blockEditorAdOns->pricingTableTitleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTablePriceToggleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableSubPriceSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableButtonSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
        $I->click($blockEditorAdOns->pricingTablePage);
        $I->wait(5);

        $I->dontSeeElement($blockEditorAdOns->pricingTableTitleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTablePriceToggleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableSubPriceSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableButtonSelector);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
        $I->reloadPage();

        $I->dontSeeElement($blockEditorAdOns->pricingTableTitleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTablePriceToggleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableSubPriceSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableButtonSelector);

        $I->click($blockEditorAdOns->pricingTableSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingTableGeneralBtn);
        $I->wait(2);
        $I->scrollTo($blockEditorAdOns->pricingTableAlignmentCenter);

        $I->click($blockEditorAdOns->pricingTableTitleToggle);
        $I->click($blockEditorAdOns->pricingTablePriceToggle);
        $I->click($blockEditorAdOns->pricingTableSubPriceToggle);
        $I->click($blockEditorAdOns->pricingTableButtonToggle);

        $I->click($blockEditorAdOns->pricingTablePricePrefixToggle);
        $I->click($blockEditorAdOns->pricingTablePriceSuffixToggle);
        $I->click($blockEditorAdOns->pricingTableFeaturesToggle);

        $I->dontSeeElement($blockEditorAdOns->pricingTablePrefixToggleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTablePriceSuffixSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableFeaturesSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
        $I->click($blockEditorAdOns->pricingTablePage);
        $I->wait(5);

        $I->dontSeeElement($blockEditorAdOns->pricingTablePrefixToggleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTablePriceSuffixSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableFeaturesSelector);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
        $I->reloadPage();

        $I->dontSeeElement($blockEditorAdOns->pricingTablePrefixToggleSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTablePriceSuffixSelector);
        $I->dontSeeElement($blockEditorAdOns->pricingTableFeaturesSelector);

        $I->click($blockEditorAdOns->pricingTableSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingTableGeneralBtn);
        $I->wait(2);
        $I->scrollTo($blockEditorAdOns->pricingTableAlignmentCenter);

        $I->click($blockEditorAdOns->pricingTablePricePrefixToggle);
        $I->click($blockEditorAdOns->pricingTablePriceSuffixToggle);
        $I->click($blockEditorAdOns->pricingTableFeaturesToggle);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ColorsInColorsSettingShouldChangeAsPerUserSelectionAndReflectedOnFronEnd(AcceptanceTester $I,
                                                                                             Page\LoginPage $loginPage,
                                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
        $I->click($blockEditorAdOns->pricingTablePage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);

        $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
        $I->click($blockEditorAdOns->pricingTableSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingTableColorSettingsBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingTableTextColorRedSelect);
        $I->click($blockEditorAdOns->pricingTableTitleColorBlackSelect);
        $I->click($blockEditorAdOns->pricingTablePricePrefixColorBrownSelect);
        $I->click($blockEditorAdOns->pricingTablePriceColorRedSelect);
        $I->click($blockEditorAdOns->pricingTablePriceSuffixColorBlackSelect);
        $I->click($blockEditorAdOns->pricingTableSubPriceColorBrownSelect);
        $I->click($blockEditorAdOns->pricingTableFeaturesColorRedSelect);

//        $textColorOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('color');
//            });
//        $I->assertEquals('rgba(205, 38, 83, 1)', $textColorOnPage);

        $titleColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $titleColorOnPage);

        $pricePrifixColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__currency.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $pricePrifixColorOnPage);

        $priceColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__amount.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $priceColorOnPage);

        $priceSuffixColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__price_suffix.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $priceSuffixColorOnPage);

        $subPriceColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $subPriceColorOnPage);

        $featuresColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__features.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $featuresColorOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
        $I->click($blockEditorAdOns->pricingTablePage);
        $I->wait(4);

        $titleColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__title'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $titleColorOnFrontEnd);

        $pricePrifixColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__currency'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $pricePrifixColorOnFrontEnd);

        $priceColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__amount'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $priceColorOnFrontEnd);

        $priceSuffixColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__price_suffix'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $priceSuffixColorOnFrontEnd);

        $subPriceColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $subPriceColorOnFrontEnd);

        $featuresColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__features'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $featuresColorOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);

        $titleColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $titleColorOnPage);

        $pricePrifixColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__currency.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $pricePrifixColorOnPage);

        $priceColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__amount.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $priceColorOnPage);

        $priceSuffixColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__price_suffix.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $priceSuffixColorOnPage);

        $subPriceColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $subPriceColorOnPage);

        $featuresColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__features.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $featuresColorOnPage);

        $I->click($blockEditorAdOns->pricingTableSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingTableColorSettingsBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingTableTextColorclear);
        $I->click($blockEditorAdOns->pricingTableTitleColorclear);
        $I->click($blockEditorAdOns->pricingTablePricePrefixColorclear);
        $I->click($blockEditorAdOns->pricingTablePriceColorclear);
        $I->click($blockEditorAdOns->pricingTablePriceSuffixColorclear);
        $I->click($blockEditorAdOns->pricingTableSubPriceColorclear);
        $I->click($blockEditorAdOns->pricingTableFeaturesColorclear);

        $I->click($blockEditorAdOns->updateBtn);


    }


        public function ChangeInSpacingByUserShouldReflectedOnFronEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableSpacingBtn);
            $I->wait(2);

            $I->pressKey($blockEditorAdOns->pricingTableTitleSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->pricingTableTitleSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->pricingTablePriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->pricingTablePriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->pricingTableSubPriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->pricingTableSubPriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->pricingTableButtonSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->pricingTableButtonSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->pricingTableFeaturesSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->pricingTableFeaturesSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $titleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $titleOnPage);

            $tablePriceOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__price-wrapper'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $tablePriceOnPage);

            $subPriceOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $subPriceOnPage);

            $buttonOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__button.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $buttonOnPage);

            $featuresOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $featuresOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(4);

            $titleOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__title'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $titleOnFrontEnd);

            $tablePriceOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__price-wrapper'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $tablePriceOnFrontEnd);

            $subPriceOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $subPriceOnFrontEnd);

            $buttonOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__features'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $buttonOnFrontEnd);

            $featuresOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__button'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $featuresOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);

            $titleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $titleOnPage);

            $tablePriceOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item__price-wrapper'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $tablePriceOnPage);

            $subPriceOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__sub_price.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $subPriceOnPage);

            $buttonOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__button.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $buttonOnPage);

            $featuresOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.wp-block-responsive-block-editor-addons-pricing-table-item__title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
                });
            $I->assertEquals('2px', $featuresOnPage);

            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableSpacingBtn);
            $I->wait(2);

            $I->pressKey($blockEditorAdOns->pricingTableTitleSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->pricingTableTitleSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->pressKey($blockEditorAdOns->pricingTablePriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->pricingTablePriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->pressKey($blockEditorAdOns->pricingTableSubPriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->pricingTableSubPriceSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->pressKey($blockEditorAdOns->pricingTableButtonSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->pricingTableButtonSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->pressKey($blockEditorAdOns->pricingTableFeaturesSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->pricingTableFeaturesSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->click($blockEditorAdOns->updateBtn);

        }


        public function SolidBorderShouldBeSelectedAndReflectedTheSameOnFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableBorderBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableSolidBorder);
            $I->pressKey($blockEditorAdOns->priceTableBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->priceTableBorderColorRed);

            $solidBorderStyleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('solid', $solidBorderStyleOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnPage);

            $borderRadiusOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(4);

            $solidBorderStyleOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('solid', $solidBorderStyleOnFrontEnd);

            $borderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnFrontEnd);

            $borderRadiusOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnFrontEnd);

            $borderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);

            $solidBorderStyleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('solid', $solidBorderStyleOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnPage);

            $borderRadiusOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnPage);

            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableBorderBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableBorderWidthResetBtn);
            $I->click($blockEditorAdOns->priceTableBorderRadiusResetBtn);
            $I->click($blockEditorAdOns->priceTableBorderColorClearBtn);
            $I->click($blockEditorAdOns->updateBtn);

        }

        public function DottedBorderShouldBeSelectedAndReflectedTheSameOnFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableBorderBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableDottedBorder);
            $I->pressKey($blockEditorAdOns->priceTableBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->priceTableBorderColorRed);

            $dottedBorderStyleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $dottedBorderStyleOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnPage);

            $borderRadiusOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(4);

            $dottedBorderStyleOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $dottedBorderStyleOnFrontEnd);

            $borderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnFrontEnd);

            $borderRadiusOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnFrontEnd);

            $borderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);

            $dottedBorderStyleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $dottedBorderStyleOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnPage);

            $borderRadiusOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnPage);

            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableBorderBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableBorderWidthResetBtn);
            $I->click($blockEditorAdOns->priceTableBorderRadiusResetBtn);
            $I->click($blockEditorAdOns->priceTableBorderColorClearBtn);
            $I->click($blockEditorAdOns->updateBtn);

        }

        public function DashedBorderShouldBeSelectedAndReflectedTheSameOnFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableBorderBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableDashedBorder);
            $I->pressKey($blockEditorAdOns->priceTableBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->priceTableBorderColorRed);

            $dashedBorderStyleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $dashedBorderStyleOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnPage);

            $borderRadiusOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(4);

            $dashedBorderStyleOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $dashedBorderStyleOnFrontEnd);

            $borderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnFrontEnd);

            $borderRadiusOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnFrontEnd);

            $borderWidthOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);

            $dashedBorderStyleOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-style');
                });
            $I->assertEquals('dotted', $dashedBorderStyleOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-width');
                });
            $I->assertEquals('2px', $borderWidthOnPage);

            $borderRadiusOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-radius');
                });
            $I->assertEquals('1px', $borderRadiusOnPage);

            $borderWidthOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('border-color');
                });
            $I->assertEquals('rgb(205, 38, 83)', $borderWidthOnPage);

            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableBorderBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableBorderWidthResetBtn);
            $I->click($blockEditorAdOns->priceTableBorderRadiusResetBtn);
            $I->click($blockEditorAdOns->priceTableBorderColorClearBtn);
            $I->click($blockEditorAdOns->updateBtn);

        }

            public function ColorBackgroundForPricingTableColumnsSetByUserShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                                      Page\LoginPage $loginPage,
                                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableColumnBackgroundBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableColorBackGroundType);
            $I->click($blockEditorAdOns->priceTableBackgroundColorGrey);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);


            $backgroundColorOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('background-color');
                });
            $I->assertEquals('rgba(220, 215, 202, 0.9)', $backgroundColorOnPage);

            $I->click($blockEditorAdOns->updateBtn);
            $I->wait(4);

            $I->amOnPage('/');
            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(4);

            $backgroundColorOnFrontEnd = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('background-color');
                });
            $I->assertEquals('rgba(220, 215, 202, 0.9)', $backgroundColorOnFrontEnd);

            $I->click($blockEditorAdOns->editPageLink);
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->reloadPage();
            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);

            $backgroundColorOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('background-color');
                });
            $I->assertEquals('rgba(220, 215, 202, 0.9)', $backgroundColorOnPage);

            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableColumnBackgroundBtn);
            $I->wait(2);
            $I->click($blockEditorAdOns->priceTableColorBackGroundType);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->click($blockEditorAdOns->priceTableBackgroundColorClear);
            $I->click($blockEditorAdOns->updateBtn);

        }


        public function GradientBackgroundForPricingTableColumnsSetByUserShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                                     Page\LoginPage $loginPage,
                                                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableColumnBackgroundBtn);
            $I->wait(2);

            $I->click($blockEditorAdOns->priceTableGradientBackGroundType);
            $I->click($blockEditorAdOns->priceTableGradientColor_1GreySelect);
            $I->click($blockEditorAdOns->priceTableGradientColor_2RedSelect);

            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_1Location,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientColor_2Location,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
            $I->pressKey($blockEditorAdOns->priceTableGradientOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);


            $gradientBackgroundOnPage = $I->executeInSelenium(
                function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                    return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-pricing-table-item'))->getCSSValue('background-image');
                });
            $I->assertEquals('linear-gradient(80deg, rgba(220, 215, 202, 0.9) 10%, rgba(205, 38, 83, 0.9) 90%)', $gradientBackgroundOnPage);


        }

     */
        public function GradientBackgroundForPricingTableColumnsSetByUserShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                                     Page\LoginPage $loginPage,
                                                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
        {
            $loginPage->userLogin($I);

            $I->waitForElement($blockEditorAdOns->pricingTablePage, 20);
            $I->click($blockEditorAdOns->pricingTablePage);
            $I->wait(5);

            $I->click($blockEditorAdOns->editPageLink);

            $I->waitForElement($blockEditorAdOns->pricingTableSelect, 20);
            $I->click($blockEditorAdOns->pricingTableSelect);
            $I->wait(2);
            $I->click($blockEditorAdOns->pricingTableButtonSettingBtn);
            $I->wait(2);


        }


}