<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class CountUpBlockCest
{/*
    //Test Case for Left Alignment
    public function LeftAlignmentChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->click($blockEditorAdOns->countUpGeneralAlignmentLeft);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentLeftClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentLeftClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentLeftClassOnFrontEnd);

        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->click($blockEditorAdOns->countUpGeneralAlignmentMiddle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Right Alignment
    public function RightAlignmentChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->click($blockEditorAdOns->countUpGeneralAlignmentRight);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentRightClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentRightClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentRightClassOnFrontEnd);

        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->click($blockEditorAdOns->countUpGeneralAlignmentMiddle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Middle Alignment
    public function MiddleAlignmentChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentMiddleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentMiddleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpGeneralAlignmentMiddleClassOnFrontEnd);

        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number of Columns
    public function NumberOfColumnsChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->pressKey($blockEditorAdOns->countUpColumns,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColumns,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(3);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->wait(3);
        $I->click($blockEditorAdOns->countUpGeneralButton);
        $I->pressKey($blockEditorAdOns->countUpColumns,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColumns,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Enabled Title Toggle Button
    public function EnableTitleChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->seeElement($blockEditorAdOns->countUpContentEnableTitleToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpContentEnableTitleToggleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpContentEnableTitleToggleClassOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Disabled Title Toggle Button
    public function DisableTitleChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableTitleToggle);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableTitleToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableTitleToggleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableTitleToggleClassOnPage);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableTitleToggle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Enabled Number Toggle Button
    public function EnableNumberChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                              Page\LoginPage $loginPage,
                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->seeElement($blockEditorAdOns->countUpContentEnableNumberToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpContentEnableNumberToggleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpContentEnableNumberToggleClassOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Disabled Number Toggle Button
    public function DisableNumberChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableNumberToggle);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableNumberToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableNumberToggleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableNumberToggleClassOnPage);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableNumberToggle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Enabled Description Toggle Button
    public function EnableDescriptionChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->seeElement($blockEditorAdOns->countUpContentEnableDiscriptionToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpContentEnableDiscriptionToggleClassFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpContentEnableDiscriptionToggleClassOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Disabled Discription Toggle Button
    public function DisableDiscriptionChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableDiscriptionToggle);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableDiscriptionToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableDiscriptionToggleClassFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableDiscriptionToggleClassOnPage);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableDiscriptionToggle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Enabled Icon Toggle Button
    public function EnableIconChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableIconToggle);
        $I->seeElement($blockEditorAdOns->countUpContentEnableIconToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->seeElement($blockEditorAdOns->countUpContentEnableIconToggleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->seeElement($blockEditorAdOns->countUpContentEnableIconToggleClassOnPage);
        $I->click($blockEditorAdOns->countUpContentBtn);
        $I->click($blockEditorAdOns->countUpContentEnableIconToggle);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Disabled Icon Toggle Button
    public function DisableIconChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableIconToggleClassOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableIconToggleClassOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->dontSeeElement($blockEditorAdOns->countUpContentEnableIconToggleClassOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Solid And Width
    public function ChangeBorderStyleSolidShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockBorderStyleSolidOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockBorderStyleSolidOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $infoBlockBorderStyleSolidOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Dotted And Width
    public function ChangeBorderStyleDottedShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleDotted);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockBorderStyleDottedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockBorderStyleDottedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $infoBlockBorderStyleDottedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleDotted);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Dashed And Width
    public function ChangeBorderStyleDashedShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleDashed);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockBorderStyleDashedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockBorderStyleDashedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $infoBlockBorderStyleDashedOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleDashed);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Double And Width
    public function ChangeBorderStyleDoubleShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleDouble);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockBorderStyleDoubleOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockBorderStyleDoubleOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $infoBlockBorderStyleDoubleOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleDouble);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Groove And Width
    public function ChangeBorderStyleGrooveShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleGroove);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $infoBlockBorderStyleGrooveOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $infoBlockBorderStyleGrooveOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $infoBlockBorderStyleGrooveOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleGroove);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Inset And Width
    public function ChangeBorderStyleInsetShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleInset);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $infoBlockBorderStyleInsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $infoBlockBorderStyleInsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $infoBlockBorderStyleInsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleInset);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Outset And Width
    public function ChangeBorderStyleOutsetShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleOutset);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $infoBlockBorderStyleOutsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $infoBlockBorderStyleOutsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $infoBlockBorderStyleOutsetOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleOutset);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Border Style-Ridge And Width
    public function ChangeBorderStyleRidgeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleRidge);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->countUpBorderColorRedSelect);

        $infoBlockBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $infoBlockBorderStyleRidgeOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $infoBlockBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $infoBlockBorderStyleRidgeOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $infoBlockBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $infoBlockBorderStyleRidgeOnPage);

        $infoBlockBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $infoBlockBorderWidthOnPage);

        $infoBlockBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('border-radius');
            });
        $I->assertEquals('2px', $infoBlockBorderRadiusOnPage);

        $I->click($blockEditorAdOns->countUpBorderBtn);
        $I->click($blockEditorAdOns->countUpBorderStyleRidge);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->countUpBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Title Color
    public function TitleColorChangeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorTitleColorRedSelect);

        $countUpColorTitleColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorTitleColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorTitleColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorTitleColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpColorTitleColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorTitleColorRedSelectOnPage);

        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorTitleColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Number Color
    public function NumberColorChangeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorNumberColorRedSelect);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorNumberColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Text Color
    public function TextColorShouldChangeAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                              Page\LoginPage $loginPage,
                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorTextColorRedSelect);

        $countUpColorTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpColorTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $countUpColorTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorTextColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Background Color
    public function BackgroundColorChangeShouldAlsoAppearOnFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorBackgroundColorRedSelect);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);


        $countUpColorBackgroundColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.2)', $countUpColorBackgroundColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorBackgroundColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.2)', $countUpColorBackgroundColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpColorBackgroundColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.2)', $countUpColorBackgroundColorRedSelectOnPage);

        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpColorSettingsBtn);
        $I->click($blockEditorAdOns->countUpColorBackgroundColorClear);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpColorOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        //$I->click($blockEditorAdOns->countUpColorOpacityReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Size changes
    public function NumberTypographyFontSizeChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('44px', $countUpHeadingTypographyFontSizeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-size');
            });
        $I->assertEquals('44px', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('44px', $countUpHeadingTypographyFontSizeOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight100 changes
    public function NumberTypographyFontWeight100ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight100);

        $countUpHeadingTypographyFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpHeadingTypographyFontWeight100OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpHeadingTypographyFontWeight100OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight200 changes
    public function NumberTypographyFontWeight200ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight200);

        $countUpHeadingTypographyFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpHeadingTypographyFontWeight200OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpHeadingTypographyFontWeight200OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight300 changes
    public function NumberTypographyFontWeight300ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight300);

        $countUpHeadingTypographyFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpHeadingTypographyFontWeight300OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpHeadingTypographyFontWeight300OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight400 changes
    public function NumberTypographyFontWeight400ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);

        $countUpHeadingTypographyFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpHeadingTypographyFontWeight400OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpHeadingTypographyFontWeight400OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight500 changes
    public function NumberTypographyFontWeight500ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight500);

        $countUpHeadingTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpHeadingTypographyFontWeight500OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpHeadingTypographyFontWeight500OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight600 changes
    public function NumberTypographyFontWeight600ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight600);

        $countUpHeadingTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpHeadingTypographyFontWeight600OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpHeadingTypographyFontWeight600OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight700 changes
    public function NumberTypographyFontWeight700ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight700);

        $countUpHeadingTypographyFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpHeadingTypographyFontWeight700OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpHeadingTypographyFontWeight700OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight800 changes
    public function NumberTypographyFontWeight800ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight800);

        $countUpHeadingTypographyFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpHeadingTypographyFontWeight800OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpHeadingTypographyFontWeight800OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Font-Weight900 changes
    public function NumberTypographyFontWeight900ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight900);

        $countUpHeadingTypographyFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpHeadingTypographyFontWeight900OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpHeadingTypographyFontWeight900OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Line-Height changes
    public function NumberTypographyLineHeightChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('160px', $countUpHeadingTypographyLineHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('line-height');
            });
        $I->assertEquals('160px', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('160px', $countUpHeadingTypographyLineHeightOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Number Typography Bottom-Margin changes
    public function NumberTypographyBottomMarginChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                               Page\LoginPage $loginPage,
                                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyNumberBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyNumberBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpNumberTypographyNumberBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpNumberTypographyNumberBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('23px', $countUpNumberTypographyNumberBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorNumberColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__amount'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('23px', $countUpColorNumberColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpNumberTypographyNumberBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__amount.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('23px', $countUpNumberTypographyNumberBottomMarginOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyBtn);
        $I->click($blockEditorAdOns->countUpNumberTypographyNumberBottomMarginReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Size changes
    public function HeadingTypographyFontSizeChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $countUpHeadingTypographyFontSizeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $countUpHeadingTypographyFontSizeOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight100 changes
    public function HeadingTypographyFontWeight100ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight100);

        $countUpHeadingTypographyFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpHeadingTypographyFontWeight100OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpHeadingTypographyFontWeight100OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight200 changes
    public function HeadingTypographyFontWeight200ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight200);

        $countUpHeadingTypographyFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpHeadingTypographyFontWeight200OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpHeadingTypographyFontWeight200OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight300 changes
    public function HeadingTypographyFontWeight300ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight300);

        $countUpHeadingTypographyFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpHeadingTypographyFontWeight300OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpHeadingTypographyFontWeight300OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight400 changes
    public function HeadingTypographyFontWeight400ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);

        $countUpHeadingTypographyFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpHeadingTypographyFontWeight400OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpHeadingTypographyFontWeight400OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight500 changes
    public function HeadingTypographyFontWeight500ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight500);

        $countUpHeadingTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpHeadingTypographyFontWeight500OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpHeadingTypographyFontWeight500OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight600 changes
    public function HeadingTypographyFontWeight600ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight600);

        $countUpHeadingTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpHeadingTypographyFontWeight600OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpHeadingTypographyFontWeight600OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight700 changes
    public function HeadingTypographyFontWeight700ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight700);

        $countUpHeadingTypographyFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpHeadingTypographyFontWeight700OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpHeadingTypographyFontWeight700OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight800 changes
    public function HeadingTypographyFontWeight800ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight800);

        $countUpHeadingTypographyFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpHeadingTypographyFontWeight800OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpHeadingTypographyFontWeight800OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Heading Typography Font-Weight900 changes
    public function HeadingTypographyFontWeight900ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight900);

        $countUpHeadingTypographyFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpHeadingTypographyFontWeight900OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpHeadingTypographyFontWeight900OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
    //Test Case for Heading Typography Line-Height changes
    public function HeadingTypographyLineHeightChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->wait(4);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->wait(4);
        //        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('32px', $countUpHeadingTypographyLineHeightOnPage);

//        $I->click($blockEditorAdOns->updateBtn);
//        $I->wait(4);
//        $I->amOnPage('/');
//        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
//        $I->click($blockEditorAdOns->countUpPage);
//        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);
//
//        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('line-height');
//            });
//        $I->assertEquals('80px', $countUpColorHeadingColorRedSelectOnPage);
//
//        $I->click($blockEditorAdOns->editPageLink);
//        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
//        $I->reloadPage();
//        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
//        $I->click($blockEditorAdOns->countUpSelectClass);
//
//        $countUpHeadingTypographyLineHeightOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('line-height');
//            });
//        $I->assertEquals('80px', $countUpHeadingTypographyLineHeightOnPage);
//
//        $I->click($blockEditorAdOns->countUpTypographyBtn);
//        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
//        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->click($blockEditorAdOns->updateBtn);
//        $loginPage->userLogout($I);
    }
/*
    //Test Case for Heading Typography Bottom-Margin changes
    public function HeadingTypographyBottomMarginChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                               Page\LoginPage $loginPage,
                                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpHeadingTypographyBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpHeadingTypographyHeadingBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('23px', $countUpHeadingTypographyHeadingBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorHeadingColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__title'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('23px', $countUpColorHeadingColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpHeadingTypographyHeadingBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('23px', $countUpHeadingTypographyHeadingBottomMarginOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBtn);
        $I->click($blockEditorAdOns->countUpHeadingTypographyBottomMarginReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Size changes
    public function DescriptionTypographyFontSizeChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpDescriptionTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $countUpDescriptionTypographyFontSizeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('20px', $countUpDescriptionTypographyFontSizeOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyFontSize, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight100 changes
    public function DescriptionTypographyFontWeight100ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight100);

        $countUpDescriptionTypographyFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpDescriptionTypographyFontWeight100OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $countUpDescriptionTypographyFontWeight100OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight200 changes
    public function DescriptionTypographyFontWeight200ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight200);

        $countUpDescriptionTypographyFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpDescriptionTypographyFontWeight200OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $countUpDescriptionTypographyFontWeight200OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight300 changes
    public function DescriptionTypographyFontWeight300ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight300);

        $countUpDescriptionTypographyFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpDescriptionTypographyFontWeight300OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $countUpDescriptionTypographyFontWeight300OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight400 changes
    public function DescriptionTypographyFontWeight400ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);

        $countUpDescriptionTypographyFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpDescriptionTypographyFontWeight400OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $countUpDescriptionTypographyFontWeight400OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight500 changes
    public function DescriptionTypographyFontWeight500ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight500);

        $countUpDescriptionTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpDescriptionTypographyFontWeight500OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $countUpDescriptionTypographyFontWeight500OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight600 changes
    public function DescriptionTypographyFontWeight600ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight600);

        $countUpDescriptionTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpDescriptionTypographyFontWeight600OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $countUpDescriptionTypographyFontWeight600OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight700 changes
    public function DescriptionTypographyFontWeight700ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight700);

        $countUpDescriptionTypographyFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpDescriptionTypographyFontWeight700OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $countUpDescriptionTypographyFontWeight700OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight800 changes
    public function DescriptionTypographyFontWeight800ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight800);

        $countUpDescriptionTypographyFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpDescriptionTypographyFontWeight800OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $countUpDescriptionTypographyFontWeight800OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Font-Weight900 changes
    public function DescriptionTypographyFontWeight900ChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight900);

        $countUpDescriptionTypographyFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpDescriptionTypographyFontWeight900OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $countUpDescriptionTypographyFontWeight900OnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Line-Height changes
    public function DescriptionTypographyLineHeightChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                              Page\LoginPage $loginPage,
                                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpDescriptionTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('80px', $countUpDescriptionTypographyLineHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('line-height');
            });
        $I->assertEquals('80px', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('80px', $countUpDescriptionTypographyLineHeightOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyLineHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Description Typography Bottom-Margin changes
    public function DescriptionTypographyBottomMarginChangesShouldChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass, 20);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->countUpDescriptionTypographyBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $countUpDescriptionTypographyDescriptionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('33px', $countUpDescriptionTypographyDescriptionBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->countUpPage, 20);
        $I->click($blockEditorAdOns->countUpPage);
        $I->waitForElement($blockEditorAdOns->countUpPageClass,20);

        $countUpColorDescriptionColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-count-item__features'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('33px', $countUpColorDescriptionColorRedSelectOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->countUpSelectClass);
        $I->click($blockEditorAdOns->countUpSelectClass);

        $countUpDescriptionTypographyDescriptionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-count-item__features.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('33px', $countUpDescriptionTypographyDescriptionBottomMarginOnPage);

        $I->click($blockEditorAdOns->countUpTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->countUpDescriptionTypographyBottomMarginReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
}