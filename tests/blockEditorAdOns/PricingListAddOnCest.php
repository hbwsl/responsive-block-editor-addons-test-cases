<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
use Codeception\Util\Locator;
class PricingListAddOnCest
{
    /*   public function NumberOfItemsShouldChangeAsPerSelection(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
       {
           $loginPage->userLogin($I);

           $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
           $I->click($blockEditorAdOns->pricingListPage);
           $I->wait(5);

           $I->click($blockEditorAdOns->editPageLink);
           $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
           $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

           $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
           $I->click($blockEditorAdOns->pricingListSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingListGeneral);
           $I->wait(2);

           $I->pressKey($blockEditorAdOns->pricingListNumberOfItems,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
           $I->pressKey($blockEditorAdOns->pricingListNumberOfItems,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

           $I->wait(5);

       }

       public function NumberOfColumnsShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
       {
           $loginPage->userLogin($I);

           $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
           $I->click($blockEditorAdOns->pricingListPage);
           $I->wait(5);

           $I->click($blockEditorAdOns->editPageLink);
           $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
           $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

           $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
           $I->click($blockEditorAdOns->pricingListSelect);
           $I->wait(2);
           $I->click($blockEditorAdOns->pricingListGeneral);
           $I->wait(2);

           $I->pressKey($blockEditorAdOns->pricingListColumns,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
           $I->pressKey($blockEditorAdOns->pricingListColumns,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

       }


    public function SeparatorStyleShouldChangedToSolid(AcceptanceTester $I,
                                                       Page\LoginPage $loginPage,
                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSeparatorBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListSeparatorStyleSolid);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $separatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('564px', $separatorWidthOnPage);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingListPage,20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(4);

        // Checking the value on Front-End
        $separatorWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('535.797px', $separatorWidthOnFrontEnd);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->reloadPage();

        $separatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('564px', $separatorWidthOnPage);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);


        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSeparatorBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListSeparatorStyleSolid);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->click($blockEditorAdOns->updateBtn);
    }


    public function SeparatorStyleShouldChangedToDotted(AcceptanceTester $I,
                                                       Page\LoginPage $loginPage,
                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSeparatorBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListSeparatorStyleDotted);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $separatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('564px', $separatorWidthOnPage);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingListPage,20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(4);

        // Checking the value on Front-End
        $separatorWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('535.797px', $separatorWidthOnFrontEnd);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->reloadPage();

        $separatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('564px', $separatorWidthOnPage);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);


        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSeparatorBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListSeparatorStyleDotted);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->click($blockEditorAdOns->updateBtn);
    }


    public function SeparatorStyleShouldChangedToDashed(AcceptanceTester $I,
                                                       Page\LoginPage $loginPage,
                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSeparatorBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListSeparatorStyleDashed);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $separatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('564px', $separatorWidthOnPage);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingListPage,20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(4);

        // Checking the value on Front-End
        $separatorWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('535.797px', $separatorWidthOnFrontEnd);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->reloadPage();

        $separatorWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('width');
            });
        $I->assertEquals('564px', $separatorWidthOnPage);

        $borderTopWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-separator'))->getCSSValue('border-top-width');
            });
        $I->assertEquals('4px', $borderTopWidthOnPage);


        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSeparatorBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListSeparatorStyleDashed);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListSeparatorThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->click($blockEditorAdOns->updateBtn);
    }



    public function ChangesMadeInSpacingShouldAppearAtFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSpacingBtn);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->pricingListRowGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListRowGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $pricingListRowGap = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-wrap.resp-desktop-column-2.image-position-top'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('8px', $pricingListRowGap);

        $columnGapOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-wrap.resp-desktop-column-2.image-position-top'))->getCSSValue('padding-left');
            });
        $I->assertEquals('7.5px', $columnGapOnPage);


        $I->click('div.responsive-block-editior-addons-pricing-list-item-title-wrap > h4.rich-text.block-editor-rich-text__editable.responsive-block-editior-addons-pricing-list-item-title.keep-placeholder-on-focus');

        $titleBottomMargin = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('h4:first-of-type'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('13px', $titleBottomMargin);

        $allSidePaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-content'))->getCSSValue('padding');
            });
        $I->assertEquals('7px', $allSidePaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(4);

        $pricingListRowGapOnFronEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-wrap.resp-desktop-column-2.image-position-top'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('8px', $pricingListRowGapOnFronEnd);

        $columnGapOnPageOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-wrap.resp-desktop-column-2.image-position-top'))->getCSSValue('padding-left');
            });
        $I->assertEquals('7.5px', $columnGapOnPageOnFrontEnd);

        $titleBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('h4:first-of-type'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('13px', $titleBottomMarginOnFrontEnd);

        $allSidePaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-content'))->getCSSValue('padding');
            });
        $I->assertEquals('7px', $allSidePaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->reloadPage();

        $pricingListRowGap = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-wrap.resp-desktop-column-2.image-position-top'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('8px', $pricingListRowGap);

        $columnGapOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-wrap.resp-desktop-column-2.image-position-top'))->getCSSValue('padding-left');
            });
        $I->assertEquals('7.5px', $columnGapOnPage);


        $I->click('div.responsive-block-editior-addons-pricing-list-item-title-wrap > h4.rich-text.block-editor-rich-text__editable.responsive-block-editior-addons-pricing-list-item-title.keep-placeholder-on-focus');

        $titleBottomMargin = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('h4:first-of-type'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('13px', $titleBottomMargin);

        $allSidePaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-content'))->getCSSValue('padding');
            });
        $I->assertEquals('7px', $allSidePaddingOnPage);

        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListSpacingBtn);
        $I->wait(2);

        $I->pressKey($blockEditorAdOns->pricingListRowGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListRowGap, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListColumnGap, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListTitleBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->pricingListLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $I->click($blockEditorAdOns->updateBtn);
    }
*/
    public function ColorSettingsChangesShouldAppearAtFrontEnd(AcceptanceTester $I,
                                                               Page\LoginPage $loginPage,
                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(5);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListColorSettingBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListTitleColorSelect);
        $I->click($blockEditorAdOns->pricingListContentColorSelect);
        $I->click($blockEditorAdOns->pricingListPriceColorSelect);

        $titleColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('h4:first-of-type'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $titleColorOnPage);


        $contentColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editior-addons-pricing-list-item-description.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $contentColorOnPage);

        $priceColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editior-addons-pricing-list-item-price.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(220, 215, 202, 1)', $priceColorOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->pricingListPage, 20);
        $I->click($blockEditorAdOns->pricingListPage);
        $I->wait(4);

        $titleColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('h4:first-of-type'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $titleColorOnFrontEnd);


        $contentColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-description'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $contentColorOnFrontEnd);

        $priceColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editior-addons-pricing-list-item-price'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(220, 215, 202, 1)', $priceColorOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->pricingListSelect, 20);

        $titleColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('h4:first-of-type'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $titleColorOnPage);


        $contentColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editior-addons-pricing-list-item-description.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $contentColorOnPage);

        $priceColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editior-addons-pricing-list-item-price.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(220, 215, 202, 1)', $priceColorOnPage);

        $I->click($blockEditorAdOns->pricingListSelect);
        $I->wait(2);
        $I->click($blockEditorAdOns->pricingListColorSettingBtn);
        $I->wait(2);

        $I->click($blockEditorAdOns->pricingListTitleColorBlackSelect);
        $I->click($blockEditorAdOns->pricingListContentColorBlackSelect);
        $I->click($blockEditorAdOns->pricingListPriceColorBlackSelect);

        $I->click($blockEditorAdOns->updateBtn);

    }

}