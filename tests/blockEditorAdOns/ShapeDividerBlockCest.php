<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class ShapeDividerBlockCest
{/*
    //Test for Shape Color - Solid
    public function WhenShapeColorIsChangedThenItShouldAlsoRefelectInFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerShapeColorBtn);
        $I->click($blockEditorAdOns->shapeDividerShapeColorRedSelect);

        $shapeDividerShapeColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $shapeDividerShapeColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $shapeDividerShapeColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.alignfull.has-accent-color'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $shapeDividerShapeColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);

        $shapeDividerShapeColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $shapeDividerShapeColorRedSelectOnPage);

        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerShapeColorBtn);
        $I->click($blockEditorAdOns->shapeDividerShapeColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);


    }

    //Test for Background Color - Solid
    public function WhenUserSelectsSolidBackgroundColorThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                         Page\LoginPage $loginPage,
                                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerBackgroundBtn);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeColor);
        $I->click($blockEditorAdOns->shapeDividerBackgroundColorDropDownBtn);
        $I->click($blockEditorAdOns->shapeDividerBackgroundColorBrownSelect);

        $shapeDividerBackgroundColorBrownSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $shapeDividerBackgroundColorBrownSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $shapeDividerBackgroundColorBrownSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.alignfull.has-secondary-background-color'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $shapeDividerBackgroundColorBrownSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);

        $shapeDividerShapeColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $shapeDividerShapeColorRedSelectOnPage);

        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerShapeColorBtn);
        $I->click($blockEditorAdOns->shapeDividerShapeColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);

    }

    //Test for Background Type - Gradient
    public function WhenUserSelectsGradientBackgroundThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerBackgroundBtn);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeGradient);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeGradientColor1Red);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeGradientColor2LightBrown);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $shapeDividerBackgroundTypeGradientOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $shapeDividerBackgroundTypeGradientOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $shapeDividerBackgroundTypeGradientFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.alignfull'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $shapeDividerBackgroundTypeGradientFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);

        $shapeDividerBackgroundTypeGradientOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $shapeDividerBackgroundTypeGradientOnPage);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerBackgroundBtn);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeGradient);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeGradientColor1Clear);
        $I->click($blockEditorAdOns->shapeDividerBackgroundTypeGradientColor2Clear);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundTypeGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Height
    public function WhenUserChangesShapeHeightThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $shapeDividerShapeHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.is-style-hills > div:first-child'))->getCSSValue('height');
            });
        $I->assertEquals('95px', $shapeDividerShapeHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $shapeDividerShapeHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider__svg-wrapper'))->getCSSValue('min-height');
            });
        $I->assertEquals('95px', $shapeDividerShapeHeightOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);

        $shapeDividerShapeHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.is-style-hills > div:first-child'))->getCSSValue('height');
            });
        $I->assertEquals('95px', $shapeDividerShapeHeightOnPage);

        //$I->click($blockEditorAdOns->shapeDividerDividerSettingsBtn);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerShapeHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Background Height
    public function WhenUserChangesBackgroundHeightThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                     Page\LoginPage $loginPage,
                                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $shapeDividerBackgroundHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.is-style-hills > div:nth-child(2)'))->getCSSValue('height');
            });
        $I->assertEquals('45px', $shapeDividerBackgroundHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $shapeDividerBackgroundHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider__alt-wrapper'))->getCSSValue('min-height');
            });
        $I->assertEquals('45px', $shapeDividerBackgroundHeightOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);

        $shapeDividerBackgroundHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-shape-divider.is-style-hills > div:nth-child(2)'))->getCSSValue('height');
            });
        $I->assertEquals('45px', $shapeDividerBackgroundHeightOnPage);

        //$I->click($blockEditorAdOns->shapeDividerDividerSettingsBtn);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->shapeDividerBackgroundHeight, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Style Wavy
    public function WhenUserChangesShapeStyleToWavyThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                     Page\LoginPage $loginPage,
                                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleWavy);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleWavyClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleWavyClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleWavyClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);

    }

    //Test for Shape Style Hills
    public function WhenUserChangesShapeStyleToHillsThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                     Page\LoginPage $loginPage,
                                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleHillsClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleHillsClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleHillsClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Style Waves
    public function WhenUserChangesShapeStyleToWavesThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                      Page\LoginPage $loginPage,
                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleWaves);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleWavesClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleWavesClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleWavesClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Style Angles
    public function WhenUserChangesShapeStyleToAnglesThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                      Page\LoginPage $loginPage,
                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleAngled);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleAngledClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleAngledClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleAngledClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Style Sloped
    public function WhenUserChangesShapeStyleToSlopedThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleSloped);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleSlopedClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleSlopedClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleSlopedClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Style Rounded
    public function WhenUserChangesShapeStyleToRoundedThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleRounded);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleRoundedClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleRoundedClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleRoundedClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Shape Style Triangle
    public function WhenUserChangesShapeStyleToTriangleThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStyleTriangle);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleTriangleClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStyleTriangleClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStyleTriangleClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
    //Test for Shape Style Pointed
    public function WhenUserChangesShapeStyleToPointedThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                        Page\LoginPage $loginPage,
                                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->click($blockEditorAdOns->shapeDividerStylePointed);
        $I->seeElement($blockEditorAdOns->shapeDividerStylePointedClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->shapeDividerPage, 20);
        $I->click($blockEditorAdOns->shapeDividerPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->shapeDividerStylePointedClassSelectorFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->shapeDividerSelectClass, 20);
        $I->click($blockEditorAdOns->shapeDividerSelectClass);
        $I->seeElement($blockEditorAdOns->shapeDividerStylePointedClassSelector);
        $I->click($blockEditorAdOns->shapeDividerStyleHills);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

}