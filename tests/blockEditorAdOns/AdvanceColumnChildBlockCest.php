<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
use \Codeception\Util\Locator;

class AdvanceColumnChildBlockCest
{/*
    //Test Cases for Inner Column

    //Test Cases for Content Width
    public function IfUserChangedTheLayoutWidthThenItShouldAlsoChnagedForFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        //$I->wait(4);
        $I->click($blockEditorAdOns->AdvanceInnerColumnLayoutBtn);
        //$I->wait(4);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnContentWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnContentWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnContentWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnContentWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnContentWidth, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnContentWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-column-wrap.responsive-block-editor-addons-block-column > div:first-child'))->getCSSValue('width');
            });
        $I->assertEquals('271.422px', $AdvanceInnerColumnContentWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        //$I->seeInPopup('Page updated');
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->wait(3);

        $AdvanceInnerColumnContentWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-columns-inner-wrap.responsive-columns-columns-2 > div:nth-child(2)'))->getCSSValue('width');
            });
        $I->assertEquals('282.859px', $AdvanceInnerColumnContentWidthOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);

        $AdvanceInnerColumnContentWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-column-wrap.responsive-block-editor-addons-block-column > div:first-child'))->getCSSValue('width');
            });
        $I->assertEquals('271.422px', $AdvanceInnerColumnContentWidthOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnLayoutBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnContentWidthReset);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test Cases for Background Color And Opacity
    public function BackgroundTypeIsChangedToColor(AcceptanceTester $I,
                                                   Page\LoginPage $loginPage,
                                                   Page\BlockEditorAdOns $blockEditorAdOns)

    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundTypeColor);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundColorRedSelect);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacity, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnBackgroundTypeColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.3)', $AdvanceInnerColumnBackgroundTypeColorOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->wait(3);

        $AdvanceInnerColumnBackgroundTypeColorOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.3)', $AdvanceInnerColumnBackgroundTypeColorOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);

        $AdvanceInnerColumnBackgroundTypeColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.3)', $AdvanceInnerColumnBackgroundTypeColorOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundTypeColor);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundColorClearBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundOpacityResetBtn);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test for Background Type - Gradient
    public function WhenUserSelectsGradientBackgroundThenItShouldAlsoReflectInFrontEnd(AcceptanceTester $I,
                                                                                       Page\LoginPage $loginPage,
                                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundTypeGradient);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColor1RedSelect);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColor2LightBrownSelect);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $AdvanceInnerColumnBackgroundTypeGradientOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $AdvanceInnerColumnBackgroundTypeGradientOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBackgroundTypeGradientOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $AdvanceInnerColumnBackgroundTypeGradientOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBackgroundTypeGradientOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgba(205, 38, 83, 0.2) 10%, rgba(220, 215, 202, 0.2) 90%)', $AdvanceInnerColumnBackgroundTypeGradientOnPage);

        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundTypeGradient);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColor1Clear);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColor2Clear);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation1, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorLocation2, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBackgroundGradientColorGradientDirection, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->updateBtn);

    }

    //Inner Column Test For Top-Padding
    public function TopPaddingChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingTopPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-top');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingTopPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingTopPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('margin-top');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingTopPaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingTopPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-top');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingTopPaddingOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Bottom-Padding
    public function BottomPaddingChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingBottomPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingBottomPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingBottomPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingBottomPaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingBottomPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingBottomPaddingOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Left-Padding
    public function LeftPaddingChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingLeftPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-left');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingLeftPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingLeftPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('padding-left');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingLeftPaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingLeftPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-left');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingLeftPaddingOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Right-Padding
    public function RightPaddingChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingRightPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-right');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingRightPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingRightPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('padding-right');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingRightPaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingRightPaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('padding-right');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingRightPaddingOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightPadding, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Top-Margin
    public function TopMarginChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                Page\LoginPage $loginPage,
                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingTopMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-top');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingTopMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingTopMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('margin-top');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingTopMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingTopMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-top');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingTopMarginOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingTopMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Bottom-Margin
    public function BottomMarginChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingBottomMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingBottomMarginOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingBottomMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Left-Margin
    public function LeftMarginChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingLeftMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-left');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingLeftMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnSpacingLeftMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('margin-left');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingLeftMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingLeftMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-left');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingLeftMarginOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingLeftMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Right-Margin
    public function RightMarginChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                 Page\LoginPage $loginPage,
                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $AdvanceInnerColumnSpacingRightMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-right');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingRightMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $AdvanceInnerColumnSpacingRightMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('margin-right');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingRightMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnSpacingRightMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('margin-right');
            });
        $I->assertEquals('20px', $AdvanceInnerColumnSpacingRightMarginOnPage);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSpacingBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnSpacingRightMargin, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Solid
    public function BorderStyleChangesToSolidShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeSolid);

        $AdvanceInnerColumnBorderTypeSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $AdvanceInnerColumnBorderTypeSolidOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeSolidOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $AdvanceInnerColumnBorderTypeSolidOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $AdvanceInnerColumnBorderTypeSolidOnPage);
    }

    //Inner Column Test For Border Style Dotted
    public function BorderStyleChangesToDottedShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeDotted);

        $AdvanceInnerColumnBorderTypeDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $AdvanceInnerColumnBorderTypeDottedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeDottedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $AdvanceInnerColumnBorderTypeDottedOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $AdvanceInnerColumnBorderTypeDottedOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Dashed
    public function BorderStyleChangesToDashedShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeDashed);

        $AdvanceInnerColumnBorderTypeDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $AdvanceInnerColumnBorderTypeDashedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeDashedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $AdvanceInnerColumnBorderTypeDashedOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $AdvanceInnerColumnBorderTypeDashedOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Double
    public function BorderStyleChangesToDoubleShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeDouble);

        $AdvanceInnerColumnBorderTypeDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $AdvanceInnerColumnBorderTypeDoubleOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeDoubleOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $AdvanceInnerColumnBorderTypeDoubleOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $AdvanceInnerColumnBorderTypeDoubleOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Groove
    public function BorderStyleChangesToGrooveShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeGroove);

        $AdvanceInnerColumnBorderTypeGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $AdvanceInnerColumnBorderTypeGrooveOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeGrooveOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $AdvanceInnerColumnBorderTypeGrooveOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $AdvanceInnerColumnBorderTypeGrooveOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Inset
    public function BorderStyleChangesToInsetShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeInset);

        $AdvanceInnerColumnBorderTypeInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $AdvanceInnerColumnBorderTypeInsetOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeInsetOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $AdvanceInnerColumnBorderTypeInsetOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $AdvanceInnerColumnBorderTypeInsetOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Outset
    public function BorderStyleChangesToOutsetShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeOutset);

        $AdvanceInnerColumnBorderTypeOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $AdvanceInnerColumnBorderTypeOutsetOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeOutsetOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $AdvanceInnerColumnBorderTypeOutsetOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $AdvanceInnerColumnBorderTypeOutsetOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Style Ridge
    public function BorderStyleChangesToRidgeShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeRidge);

        $AdvanceInnerColumnBorderTypeRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $AdvanceInnerColumnBorderTypeRidgeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderTypeRidgeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $AdvanceInnerColumnBorderTypeRidgeOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderTypeRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $AdvanceInnerColumnBorderTypeRidgeOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderTypeNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Border Radius
    public function BorderRadiusChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $AdvanceInnerColumnBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $AdvanceInnerColumnBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass,20);

        $AdvanceInnerColumnBorderRadiusOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $AdvanceInnerColumnBorderRadiusOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $AdvanceInnerColumnBorderRadiusOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderRadiusReset);
        $I->click($blockEditorAdOns->updateBtn);
    }

    //Inner Column Test For Box Shadow Tests(Color,Horizontal,Vertical,Blur,Spread,Position)
    public function BoxShadowColorAndItsAttributesChangesShouldReflectToFrontEndToo(AcceptanceTester $I,
                                                                                    Page\LoginPage $loginPage,
                                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBoxShadowBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBoxShadowColorRedSelect);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowHorizontal, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowHorizontal, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowVertical, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowVertical, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowBlur, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowBlur, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowSpread, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowSpread, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBoxShadowPositionInset);

        $AdvanceInnerColumnBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 22px 22px 52px 22px inset', $AdvanceInnerColumnBoxShadowOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(3);
        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->advanceColumnPage, 20);
        $I->click($blockEditorAdOns->advanceColumnPage);
        $I->waitForElement($blockEditorAdOns->advanceColumnPageClass, 20);

        $AdvanceInnerColumnBoxShadowOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.post-inner.thin > div > div > div > div > div > div:nth-child(2) > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 22px 22px 52px 22px inset', $AdvanceInnerColumnBoxShadowOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->AdvanceColumnSelectClass, 20);
        $I->click($blockEditorAdOns->AdvanceColumnSelectClass);

        $AdvanceInnerColumnBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 22px 22px 52px 22px inset', $AdvanceInnerColumnBoxShadowOnPage);
        $I->click($blockEditorAdOns->AdvanceInnerColumnSelectClass);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBorderBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBoxShadowBtn);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBoxShadowColorClear);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowHorizontal, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowHorizontal, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowVertical, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowVertical, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowBlur, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowBlur, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowSpread, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->AdvanceInnerColumnBoxShadowSpread, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->AdvanceInnerColumnBoxShadowPositionInset);
        $I->click($blockEditorAdOns->updateBtn);
    }
*/

}