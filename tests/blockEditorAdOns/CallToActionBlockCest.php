<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;

class CallToActionBlockCest
{/*
    // Test for Border-Radius
    public function ChangesMadeInBorderRadiusShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBorderRadius,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionGeneralBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
            });
        $I->assertEquals('16px', $callToActionGeneralBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionGeneralBorderRadiusOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('16px', $callToActionGeneralBorderRadiusOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionGeneralBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('border-radius');
            });
        $I->assertEquals('16px', $callToActionGeneralBorderRadiusOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralBorderRadiusResetButton);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Title Font size
    public function ChangesMadeInTitleFontSizeShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->seeElement($blockEditorAdOns->callToActionFontSizeOnPageSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionFontSizeFrontEndSelector);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->seeElement($blockEditorAdOns->callToActionFontSizeOnPageSelector);

        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleTypographyFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-100
    public function TitleFontWeightChangedTo100ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight100);

        $callToActionTitleFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $callToActionTitleFontWeight100OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight100FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $callToActionTitleFontWeight100FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $callToActionTitleFontWeight100OnPage);
               $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-200
    public function TitleFontWeightChangedTo200ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight200);

        $callToActionTitleFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $callToActionTitleFontWeight200OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight200FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $callToActionTitleFontWeight200FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $callToActionTitleFontWeight200OnPage);
               $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-300
    public function TitleFontWeightChangedTo300ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight300);

        $callToActionTitleFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $callToActionTitleFontWeight300OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight300FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $callToActionTitleFontWeight300FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $callToActionTitleFontWeight300OnPage);
               $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-400
    public function TitleFontWeightChangedTo400ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);

        $callToActionTitleFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $callToActionTitleFontWeight400OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight400FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $callToActionTitleFontWeight400FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $callToActionTitleFontWeight400OnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);

    }

        Test for Title Font Weight-500
    public function TitleFontWeightChangedTo500ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight500);

        $callToActionTitleFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $callToActionTitleFontWeight500OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight500FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $callToActionTitleFontWeight500FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $callToActionTitleFontWeight500OnPage);
               $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-600
    public function TitleFontWeightChangedTo600ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight600);

        $callToActionTitleFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $callToActionTitleFontWeight600OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight600FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $callToActionTitleFontWeight600FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $callToActionTitleFontWeight600OnPage);
               $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-700
    public function TitleFontWeightChangedTo700ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight700);

        $callToActionTitleFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $callToActionTitleFontWeight700OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight700FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $callToActionTitleFontWeight700FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $callToActionTitleFontWeight700OnPage);
                $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-800
    public function TitleFontWeightChangedTo800ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight800);

        $callToActionTitleFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $callToActionTitleFontWeight800OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight800FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $callToActionTitleFontWeight800FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $callToActionTitleFontWeight800OnPage);
               $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        Test for Title Font Weight-900
    public function TitleFontWeightChangedTo900ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight900);

        $callToActionTitleFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $callToActionTitleFontWeight900OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleFontWeight900FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $callToActionTitleFontWeight900FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $callToActionTitleFontWeight900OnPage);
                $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
    }

        Test for Title Line Height
    public function ChnagesMadeInLineHeightShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->pressKey($blockEditorAdOns->callToActionTitleLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionTitleLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionTitleLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('line-height');
            });
        $I->assertEquals('88px', $callToActionTitleLineHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTitleLineHeightFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('line-height');
            });
        $I->assertEquals('88px', $callToActionTitleLineHeightFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTitleLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('line-height');
            });
        $I->assertEquals('88px', $callToActionTitleLineHeightOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTitleTypographyBtn);
        $I->pressKey($blockEditorAdOns->callToActionTitleLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionTitleLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Description Line Height
    public function ChnagesMadeInLineHeightOfDescriptionShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                    Page\LoginPage $loginPage,
                                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->callToActionDescriptionLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionDescriptionLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionDescriptionLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('line-height');
            });
        $I->assertEquals('84px', $callToActionDescriptionLineHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionLineHeightFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('line-height');
            });
        $I->assertEquals('84px', $callToActionDescriptionLineHeightFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('line-height');
            });
        $I->assertEquals('84px', $callToActionDescriptionLineHeightOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->pressKey($blockEditorAdOns->callToActionDescriptionLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionDescriptionLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Description Font Weight-100
    public function DescriptionFontWeightChangedTo100ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight100);

        $callToActionDescriptionFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $callToActionDescriptionFontWeight100OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight100FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $callToActionDescriptionFontWeight100FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $callToActionDescriptionFontWeight100OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Description Font Weight-200
    public function DescriptionFontWeightChangedTo200ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight200);

        $callToActionDescriptionFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $callToActionDescriptionFontWeight200OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight200FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $callToActionDescriptionFontWeight200FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $callToActionDescriptionFontWeight200OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Description Font Weight-300
    public function DescriptionFontWeightChangedTo300ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight300);

        $callToActionDescriptionFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $callToActionDescriptionFontWeight300OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight300FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $callToActionDescriptionFontWeight300FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $callToActionDescriptionFontWeight300OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Description Font Weight-400
    public function DescriptionFontWeightChangedTo400ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);

        $callToActionDescriptionFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $callToActionDescriptionFontWeight400OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight400FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $callToActionDescriptionFontWeight400FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $callToActionDescriptionFontWeight400OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Description Font Weight-500
    public function DescriptionFontWeightChangedTo500ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight500);

        $callToActionDescriptionFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $callToActionDescriptionFontWeight500OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight500FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $callToActionDescriptionFontWeight500FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $callToActionDescriptionFontWeight500OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Description Font Weight-600
    public function DescriptionFontWeightChangedTo600ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight600);

        $callToActionDescriptionFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $callToActionDescriptionFontWeight600OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight600FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $callToActionDescriptionFontWeight600FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $callToActionDescriptionFontWeight600OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Description Font Weight-700
    public function DescriptionFontWeightChangedTo700ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight700);

        $callToActionDescriptionFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $callToActionDescriptionFontWeight700OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight700FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $callToActionDescriptionFontWeight700FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $callToActionDescriptionFontWeight700OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Description Font Weight-800
    public function DescriptionFontWeightChangedTo800ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight800);

        $callToActionDescriptionFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $callToActionDescriptionFontWeight800OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight800FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $callToActionDescriptionFontWeight800FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $callToActionDescriptionFontWeight800OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Description Font Weight-900
    public function DescriptionFontWeightChangedTo900ShouldAlsoRefelectInFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight900);

        $callToActionDescriptionFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $callToActionDescriptionFontWeight900OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionDescriptionFontWeight900FrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $callToActionDescriptionFontWeight900FrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionDescriptionFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $callToActionDescriptionFontWeight900OnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionDescriptionFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Text Color Change
    public function ChangesMadeInTextColorShouldShouldAlsoReflectInFontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTextColorBtn);
        $I->click($blockEditorAdOns->callToActionTextColorRedSelect);

        $callToActionTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionTextColorRedSelectFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionTextColorRedSelectFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionTextColorRedSelectOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionTypographyBtn);
        $I->click($blockEditorAdOns->callToActionTextColorBtn);
        $I->click($blockEditorAdOns->callToActionTextColorClearBtn);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Background Color Change
    public function IfBackgroundTypeIsSelectedToColorThenItShouldAlsoChangedInFontEnd(AcceptanceTester $I,
                                                                                      Page\LoginPage $loginPage,
                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionBackgroundOptionsBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeColor);
        $I->click($blockEditorAdOns->callToActionBackgroundColorBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundColorRedSelect);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundColorOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $callToActionBackgroundColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.9)', $callToActionBackgroundColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionBackgroundColorRedSelectFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.9)', $callToActionBackgroundColorRedSelectFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionBackgroundColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 0.9)', $callToActionBackgroundColorRedSelectOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionBackgroundOptionsBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeColor);
        $I->click($blockEditorAdOns->callToActionBackgroundColorBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundColorClear);
        $I->click($blockEditorAdOns->callToActionBackgroundColorOpacityClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Box Shadow-Inset Position
    public function ChangesMadeInBoxShadowAndPositionToOutsetItShouldAlsoChangedInFontEnd(AcceptanceTester $I,
                                                                                          Page\LoginPage $loginPage,
                                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowIcon);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowColorRedSelect);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowPositionOutset);

        $callToActionGeneralBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 2px 2px 2px 2px', $callToActionGeneralBoxShadowOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionGeneralBoxShadowOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 2px 2px 2px 2px', $callToActionGeneralBoxShadowOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionGeneralBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 2px 2px 2px 2px', $callToActionGeneralBoxShadowOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowIcon);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowColorClearBtn);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowHorizontalReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowVerticalReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowBlurReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowSpreadReset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test for Box Shadow-Outset Position
    public function ChangesMadeInBoxShadowAndPositionToInsetItShouldAlsoChangedInFontEnd(AcceptanceTester $I,
                                                                                         Page\LoginPage $loginPage,
                                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowIcon);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowColorRedSelect);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowHorizontal,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowVertical,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowBlur,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionGeneralBoxShadowSpread,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowPositionInset);

        $callToActionGeneralBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 2px 2px 2px 2px inset', $callToActionGeneralBoxShadowOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionGeneralBoxShadowOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 2px 2px 2px 2px inset', $callToActionGeneralBoxShadowOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionGeneralBoxShadowOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('box-shadow');
            });
        $I->assertEquals('rgb(205, 38, 83) 2px 2px 2px 2px inset', $callToActionGeneralBoxShadowOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowIcon);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowColorClearBtn);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowHorizontalReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowVerticalReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowBlurReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowSpreadReset);
        $I->click($blockEditorAdOns->callToActionGeneralBoxShadowPositionOutset);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Text Alignment Left
    public function IfTextAlignmentChangedToLeftThenItShouldAlsoChangeForFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignButton);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignLeft);
        $callToActionGeneralTextAlignLeftOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $callToActionGeneralTextAlignLeftOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionGeneralTextAlignLeftOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $callToActionGeneralTextAlignLeftOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionGeneralTextAlignLeftOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $callToActionGeneralTextAlignLeftOnPage);

        $I->click($blockEditorAdOns->callToActionGeneralTextAlignButton);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignCenter);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test Case for Text Alignment Right
    public function IfTextAlignmentChangedToRightThenItShouldAlsoChangeForFontEnd(AcceptanceTester $I,
                                                                                 Page\LoginPage $loginPage,
                                                                                 Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignButton);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignRight);
        $callToActionGeneralTextAlignRightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $callToActionGeneralTextAlignRightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionGeneralTextAlignRightOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $callToActionGeneralTextAlignRightOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionGeneralTextAlignRightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $callToActionGeneralTextAlignRightOnPage);

        $I->click($blockEditorAdOns->callToActionGeneralTextAlignButton);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignCenter);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Text Alignment Center
    public function IfTextAlignmentChangedToCenterThenItShouldAlsoChangeForFontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignButton);
       // $I->click($blockEditorAdOns->callToActionGeneralTextAlignCenter);
        $callToActionGeneralTextAlignCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $callToActionGeneralTextAlignCenterOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionGeneralTextAlignCenterOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $callToActionGeneralTextAlignCenterOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionGeneralTextAlignCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $callToActionGeneralTextAlignCenterOnPage);

        $I->click($blockEditorAdOns->callToActionGeneralTextAlignButton);
        $I->click($blockEditorAdOns->callToActionGeneralTextAlignCenter);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test for Background type Gradient
    public function IfBackgroundTypeIsSelectedToGradientThenItShouldAlsoChangedInFontEnd(AcceptanceTester $I,
                                                                                         Page\LoginPage $loginPage,
                                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionBackgroundOptionsBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeGradient);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeGradientColor1RedSelected);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeGradientColor2LightBrownSelect);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $callToActionBackgroundGradientSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $callToActionBackgroundGradientSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionBackgroundColorRedSelectFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $callToActionBackgroundColorRedSelectFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionBackgroundGradientSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $callToActionBackgroundGradientSelectOnPage);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionBackgroundOptionsBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeGradient);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeGradientColor1ClearBtn);
        $I->click($blockEditorAdOns->callToActionBackgroundTypeGradientColor2ClearBtn);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientColorLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for spacing(Padding)
    public function ChangesMadeInSpacingShouldAlsoChangedInFontEnd(AcceptanceTester $I,
                                                                   Page\LoginPage $loginPage,
                                                                   Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionSpacingBtn);
        $I->click($blockEditorAdOns->callToActionSpacingSpacingTab);

        $I->pressKey($blockEditorAdOns->callToActionSpacingTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->callToActionSpacingBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->callToActionSpacingLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->callToActionSpacingRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('padding');
            });
        $I->assertEquals('24px 23px 23px 24px', $callToActionSpacingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionSpacingFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-responsive-block-editor-addons-cta.responsive-block-editor-addons-block-cta.responsive-block-editor-addons-font-size-16 >div:first-child'))->getCSSValue('padding');
            });
        $I->assertEquals('24px 23px 23px 24px', $callToActionSpacingFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('#editor > div > div > div.components-navigate-regions > div > div.interface-interface-skeleton__body > div.interface-interface-skeleton__content > div.edit-post-visual-editor.editor-styles-wrapper > div.block-editor__typewriter > div > div > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div > div.block-editor-block-list__block.wp-block.is-selected.wp-block > div > div'))->getCSSValue('padding');
            });
        $I->assertEquals('24px 23px 23px 24px', $callToActionSpacingOnPage);

        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionSpacingBtn);
        $I->click($blockEditorAdOns->callToActionSpacingSpacingTab);
        $I->click($blockEditorAdOns->callToActionSpacingTopPaddingResetBtn);
        $I->click($blockEditorAdOns->callToActionSpacingBottomPaddingResetBtn);
        $I->click($blockEditorAdOns->callToActionSpacingLeftPaddingResetBtn);
        $I->click($blockEditorAdOns->callToActionSpacingRightPaddingResetBtn);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Margin
    public function ChangesMadeInMarginShouldAlsoChangedInFontEnd(AcceptanceTester $I,
                                                                  Page\LoginPage $loginPage,
                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionSpacingBtn);
        $I->click($blockEditorAdOns->callToActionSpacingMarginTab);

        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginTitleBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginTitleBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginTitleBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginTitleBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginDiscriptionBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginDiscriptionBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginDiscriptionBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

//        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginButtonBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginButtonBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginButtonBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
//        $I->pressKey($blockEditorAdOns->callToActionSpacingMarginButtonBottomMargin,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionSpacingMarginTitleBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('29px', $callToActionSpacingMarginTitleBottomMarginOnPage);

        $callToActionSpacingMarginDiscriptionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('31px', $callToActionSpacingMarginDiscriptionBottomMarginOnPage);

//        $callToActionSpacingMarginButtonBottomMarginOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('margin-bottom');
//            });
//        $I->assertEquals('31.8798px', $callToActionSpacingMarginButtonBottomMarginOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionSpacingMarginTitleBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('29px', $callToActionSpacingMarginTitleBottomMarginOnFrontEnd);

        $callToActionSpacingMarginDiscriptionBottomMarginOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('31px', $callToActionSpacingMarginDiscriptionBottomMarginOnFrontEnd);

//        $callToActionSpacingMarginButtonBottomMarginOnFrontEnd = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('margin-bottom');
//            });
//        $I->assertEquals('31.8798px', $callToActionSpacingMarginButtonBottomMarginOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionSpacingMarginTitleBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > h2'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('29px', $callToActionSpacingMarginTitleBottomMarginOnPage);

        $callToActionSpacingMarginDiscriptionBottomMarginOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-content > div'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('31px', $callToActionSpacingMarginDiscriptionBottomMarginOnPage);

//        $callToActionSpacingMarginButtonBottomMarginOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('margin-bottom');
//            });
//        $I->assertEquals('31.8798px', $callToActionSpacingMarginButtonBottomMarginOnPage);

        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionSpacingBtn);
        $I->click($blockEditorAdOns->callToActionSpacingMarginTab);
        $I->click($blockEditorAdOns->callToActionSpacingMarginTitleBottomMarginResetBtn);
        $I->click($blockEditorAdOns->callToActionSpacingMarginDiscriptionBottomMarginResetBtn);
        //$I->click($blockEditorAdOns->callToActionSpacingMarginButtonBottomMarginResetBtn);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Button Link in Next Tab
    public function ButtonLinkShouldOpenInNewWindow(AcceptanceTester $I,
                                                    Page\LoginPage $loginPage,
                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsOpneLinkInNewWindow);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->scrollTo($blockEditorAdOns->callToActionFrontEndSubHeadingSelector);
        $I->wait(2);
        $I->click($blockEditorAdOns->callToActionFrontEndButtonSelector);
        $I->wait(4);
        $I->switchToNextTab();
        $I->wait(3);
        $I->switchToPreviousTab();
        $I->wait(2);
        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsOpneLinkInNewWindow);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);

    }

        //Test case for small button size
    public function ButtonSizeIsSelectedToSmallShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeSmall);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeSmallClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeSmallClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeSmallClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeMedium);
        $I->dontSeeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeSmallClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Medium button size
    public function ButtonSizeIsSelectedToMediumShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeMedium);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeMediumClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeMediumClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeMediumClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Large button size
    public function ButtonSizeIsSelectedToLargeShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                           Page\LoginPage $loginPage,
                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeLarge);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeLargeClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeLargeClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeLargeClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeMedium);
        $I->dontSeeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeLargeClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test case for Button-size extra large
    public function ButtonTypeIsSelectedToExtraLargeShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeExtraLarge);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeExtraLargeClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeExtraLargeClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeExtraLargeClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonSizeMedium);
        $I->dontSeeElement($blockEditorAdOns->callToActionButtonOptionsButtonSizeExtraLargeClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Button Type Squre
    public function ButtonShapeIsSelectedToSquareShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonShapeSquare);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeSquareClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeSquareClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeSquareClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonShapeRoundedSquare);
        $I->dontSeeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeSquareClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

         //Test case for Button Type Rounded-Square
    public function ButtonShapeIsSelectedToRoundedSquareShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                    Page\LoginPage $loginPage,
                                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonShapeRoundedSquare);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeRoundedSquareClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeRoundedSquareClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeRoundedSquareClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Button Type Circular
    public function ButtonShapeIsSelectedToCircularShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                               Page\LoginPage $loginPage,
                                                                               Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonShapeCircular);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeCircularClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeCircularClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeCircularClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonShapeRoundedSquare);
        $I->dontSeeElement($blockEditorAdOns->callToActionButtonOptionsButtonShapeCircularClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

    //Test case for Button Type Selected to Text
    public function ButtonTypeIsSelectedToTextShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsTypeText);
        $I->fillField($blockEditorAdOns->callToActionButtonOptionsTypeTextClass,'Text_Text');
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsTypeTextClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsTypeTextFrontEndClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsTypeTextClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsTypeButton);
        $I->dontSeeElement($blockEditorAdOns->callToActionButtonOptionsTypeTextClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Button Type Selected to Button
    public function ButtonTypeIsSelectedToButtonShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsTypeButton);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsTypeButtonClass);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);
        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsTypeButtonClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $I->seeElement($blockEditorAdOns->callToActionButtonOptionsTypeButtonClass);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Border Style is selected to Solid
    public function BorderStyleIsSelectedToSolidShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleSolid);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionButtonOptionsBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $callToActionButtonOptionsBorderStyleSolidOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 14px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $callToActionButtonOptionsBorderStyleSolidOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $callToActionButtonOptionsBorderStyleSolidOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Border Style is selected to Dotted
    public function BorderStyleIsSelectedToDottedShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                            Page\LoginPage $loginPage,
                                                                            Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleDotted);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionButtonOptionsBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $callToActionButtonOptionsBorderStyleDottedOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 14px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsBorderStyleDottedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $callToActionButtonOptionsBorderStyleDottedOnFrontEnd);

        $callToActionButtonOptionsBorderThicknessOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnFrontEnd);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $callToActionButtonOptionsBorderStyleDottedOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleDotted);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Border Style is selected to Dashed
    public function BorderStyleIsSelectedToDashedShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleDashed);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionButtonOptionsBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $callToActionButtonOptionsBorderStyleDashedOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 14px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsBorderStyleDashedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $callToActionButtonOptionsBorderStyleDashedOnFrontEnd);

        $callToActionButtonOptionsBorderThicknessOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnFrontEnd);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBorderStyleDashedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $callToActionButtonOptionsBorderStyleDashedOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleDashed);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Border Style is selected to Double
    public function BorderStyleIsSelectedToDoubleShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                             Page\LoginPage $loginPage,
                                                                             Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleDouble);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionButtonOptionsBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $callToActionButtonOptionsBorderStyleDoubleOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 14px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsBorderStyleDoubleOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $callToActionButtonOptionsBorderStyleDoubleOnFrontEnd);

        $callToActionButtonOptionsBorderThicknessOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnFrontEnd);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $callToActionButtonOptionsBorderStyleDoubleOnPage);

        $callToActionButtonOptionsBorderThicknessOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-width');
            });
        $I->assertEquals('3px', $callToActionButtonOptionsBorderThicknessOnPage);

//        $callToActionButtonOptionsBorderPaddingOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('padding');
//            });
//        $I->assertEquals('12px 16px', $callToActionButtonOptionsBorderPaddingOnPage);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBorderBtn);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderThickness,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderHorizontalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBorderVerticalPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        //$I->click($blockEditorAdOns->callToActionButtonOptionsBorderStyleDouble);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Border Style is selected to Dashed
    public function ButtonTextColorIsSelectedToRedShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                              Page\LoginPage $loginPage,
                                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextColorBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextColorRedSelect);

        $callToActionButtonOptionsButtonTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-button'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsButtonTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsButtonTextColorRedSelectFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsButtonTextColorRedSelectFrontEnd);


        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsButtonTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-button'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsButtonTextColorRedSelectOnPage);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextColorBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test case for Button Text hover Color
    public function ButtonTextHoverColorIsSelectedToLightBrownShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                          Page\LoginPage $loginPage,
                                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextHoverColorBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextHoverColorLightBrownSelect);
        $I->moveMouseOver('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-button');
        $I->wait(4);

        $callToActionButtonOptionsButtonTextHoverColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-button'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(220, 215, 202, 1)', $callToActionButtonOptionsButtonTextHoverColorOnPage);

//        $I->click($blockEditorAdOns->updateBtn);
//        $I->wait(4);
//
//        $I->amOnPage('/');
//        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
//        $I->click($blockEditorAdOns->calltoactionPage);
//        $I->wait(3);
//
//        $callToActionButtonOptionsButtonTextHoverColorFrontEnd = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('color');
//            });
//        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsButtonTextHoverColorFrontEnd);
//
//
//        $I->click($blockEditorAdOns->editPageLink);
//        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
//        $I->reloadPage();
//        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
//
//        $callToActionButtonOptionsButtonTextColorRedSelectOnPage = $I->executeInSelenium(
//            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
//                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-cta-button'))->getCSSValue('color');
//            });
//        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsButtonTextColorRedSelectOnPage);
//        $I->click($blockEditorAdOns->callToActionSelectClass);
//        $I->click($blockEditorAdOns->callToActionGeneralButton);
//        $I->click($blockEditorAdOns->callToActionButtonOptions);
//        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextColorBtn);
//        $I->click($blockEditorAdOns->callToActionButtonOptionsButtonTextColorClear);
//        $I->click($blockEditorAdOns->updateBtn);
    }

        //Test Case for Button Background Color
    public function ButtonBackgroundColorIsSelectedToRedShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                    Page\LoginPage $loginPage,
                                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsColorOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonColorDropdown);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelect);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsColorOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonColorDropdown);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Button Border Color
    public function ButtonBorderColorSelectedToBlackShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                Page\LoginPage $loginPage,
                                                                                Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsColorOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorDropdown);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorBlackSelect);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-bottom-color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-bottom-color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);

        $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('border-bottom-color');
            });
        $I->assertEquals('rgba(0, 0, 0, 1)', $callToActionButtonOptionsBackgroundColorSettingsButtonColorRedSelectOnPage);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsColorOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorDropdown);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsButtonBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Button Color To Gradient
    public function ButtonColorGradientSelectedToBlackShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                  Page\LoginPage $loginPage,
                                                                                  Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsColorOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBackgroundTypeGradient);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientColor1RedSelect);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientColor2RedSelect);

        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $callToActionBackgroundGradientSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $callToActionBackgroundGradientSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionBackgroundGradientSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $callToActionBackgroundGradientSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionBackgroundGradientSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button-wrapper.responsive-block-editor-addons-cta-button-shape-rounded.responsive-block-editor-addons-cta-button-size-medium'))->getCSSValue('background-image');
            });
        $I->assertEquals('linear-gradient(80deg, rgb(205, 38, 83) 10%, rgb(220, 215, 202) 90%)', $callToActionBackgroundGradientSelectOnPage);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionButtonOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBtn);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsColorOptions);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundColorSettingsBackgroundTypeGradient);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientColor1Clear);
        $I->click($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientColor2Clear);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation1,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientLocation2,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionButtonOptionsBackgroundTypeGradientDirection,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }

        //Test Case for Icon Position To make before text
    public function WhenIconPositionIsChangedToBeforeTextThenItShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                           Page\LoginPage $loginPage,
                                                                                           Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionIconSettings);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIconDropdown);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIcon);
        $I->click($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeText);
        $I->SeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeTextClass);
        $I->dontSeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionAfterTextClass);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->SeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeTextClass);
        $I->dontSeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionAfterTextClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $I->SeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeTextClass);
        $I->dontSeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionAfterTextClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionIconSettings);
        $I->click($blockEditorAdOns->callToActionIconSettingsDeleteIcon);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }


        //Test Case for Icon Position To make after text
    public function WhenIconPositionIsChangedToAfterTextThenItShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                          Page\LoginPage $loginPage,
                                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionIconSettings);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIconDropdown);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIcon);
        $I->click($blockEditorAdOns->callToActionIconSettingsIconPositionAfterText);
        $I->seeElement($blockEditorAdOns->callToActionIconSettingsIconPositionAfterTextClass);
        $I->dontSeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeTextClass);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->seeElement($blockEditorAdOns->callToActionIconSettingsIconPositionAfterTextClass);
        $I->dontSeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeTextClass);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $I->seeElement($blockEditorAdOns->callToActionIconSettingsIconPositionAfterTextClass);
        $I->dontSeeElement($blockEditorAdOns->callToActionIconSettingsIconPositionBeforeTextClass);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionIconSettings);
        $I->click($blockEditorAdOns->callToActionIconSettingsDeleteIcon);
        $I->click($blockEditorAdOns->updateBtn);
        $loginPage->userLogout($I);
    }
*/
        //Test Case for Icon spacing
    public function WhenIconSpacingIsChangedByUserThenItShouldAlsoChangeForFrontEnd(AcceptanceTester $I,
                                                                                    Page\LoginPage $loginPage,
                                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionIconSettings);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIconDropdown);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIcon);
        $I->pressKey($blockEditorAdOns->callToActionIconSettingsIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->callToActionIconSettingsIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $callToActionIconSettingsIconSpacingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button__icon.responsive-block-editor-addons-cta-button__icon-position-after'))->getCSSValue('margin-left');
            });
        $I->assertEquals('10px', $callToActionIconSettingsIconSpacingOnPage);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->calltoactionPage, 20);
        $I->click($blockEditorAdOns->calltoactionPage);
        $I->wait(3);

        $callToActionIconSettingsIconSpacingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button__icon.responsive-block-editor-addons-cta-button__icon-position-after'))->getCSSValue('margin-left');
            });
        $I->assertEquals('10px', $callToActionIconSettingsIconSpacingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->callToActionSelectClass, 20);
        $I->click($blockEditorAdOns->callToActionSelectClass);

        $callToActionBackgroundGradientSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-cta-button__icon.responsive-block-editor-addons-cta-button__icon-position-after'))->getCSSValue('margin-left');
            });
        $I->assertEquals('10px', $callToActionBackgroundGradientSelectOnFrontEnd);

        $I->click($blockEditorAdOns->callToActionSelectClass);
        $I->click($blockEditorAdOns->callToActionGeneralButton);
        $I->click($blockEditorAdOns->callToActionIconSettings);
        $I->click($blockEditorAdOns->callToActionIconSettingsSelectIconDropdown);
        $I->click($blockEditorAdOns->callToActionIconSettingsDeleteIcon);
//        $I->pressKey($blockEditorAdOns->callToActionIconSettingsIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->pressKey($blockEditorAdOns->callToActionIconSettingsIconSpacing,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
       $I->click($blockEditorAdOns->callToActionIconSettingsIconSpacingResetBtn);
       $I->click($blockEditorAdOns->updateBtn);
       $loginPage->userLogout($I);
    }

}
