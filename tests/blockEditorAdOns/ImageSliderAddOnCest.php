<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class ImageSliderAddOnCest
{/*
    public function BorderColorOfImagesInSliderShouldChangeAsPerSelection(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderColorRedSelect);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $imageSliderBorderColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $imageSliderBorderColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfBorderTypeSolidSelectedShouldAlsoChaneForFrontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderStyleSolid);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $imageSliderBorderColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $imageSliderBorderColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfBorderTypeDottedSelectedShouldAlsoChaneForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderStyleDotted);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $imageSliderBorderColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $imageSliderBorderColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfBorderTypeDashedSelectedShouldAlsoChaneForFrontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderStyleDashed);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $imageSliderBorderColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $imageSliderBorderColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);

        $imageSliderBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-gallery--item.is-selected'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $imageSliderBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderBorderBtn);
        $I->click($blockEditorAdOns->imageSliderBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInArrorColorAndArrowBackgroundColorShouldAlsoChangedOnFrontEnd(AcceptanceTester $I,
                                                                                              Page\LoginPage $loginPage,
                                                                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {////
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderArrowBtn);
        //$I->click($blockEditorAdOns->imageSliderArrowColorRedSelect);
        $I->click($blockEditorAdOns->imageSliderArrowBackGroundColorBlackSelect);
        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $imageSliderArrowColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.flickity-button-icon'))->getCSSValue('color');
            });
        $I->assertEquals('dashed', $imageSliderArrowColorRedSelectOnPage);
    }
*/
    public function SliderSettingsWorkingTest(AcceptanceTester $I,
                                              Page\LoginPage $loginPage,
                                              Page\BlockEditorAdOns $blockEditorAdOns)
    {////
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderSliderSettingsBtn);
        $I->click($blockEditorAdOns->imageSliderSliderSettingsAutoplayBtn);
        //$I->click($blockEditorAdOns->imageSliderSliderSettingsDragableBtn);
//        $I->click($blockEditorAdOns->imageSliderSliderSettingsFressScrollBtn);
//        $I->click($blockEditorAdOns->imageSliderSliderSettingsArrowNavigationBtn);
//        $I->click($blockEditorAdOns->imageSliderSliderSettingsDotNavigationBtn);
//        $I->click($blockEditorAdOns->imageSliderSliderSettingsAlignCellsBtn);
        //

        $I->SeeElement('.components-form-toggle.is-checked');
    }

  /*  public function ImageSizeShouldBeAsPerTheSelection(AcceptanceTester $I,
                                                       Page\LoginPage $loginPage,
                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->imageSliderPage, 20);
        $I->click($blockEditorAdOns->imageSliderPage);
        $I->waitForElement($blockEditorAdOns->imageSliderPage);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->imageSliderSelectClass, 20);
        $I->click($blockEditorAdOns->imageSliderSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->imageSliderImageCarausalBtn);
        $I->click($blockEditorAdOns->imageSliderImageCarausalSizeMedium);

        $imageSliderImageCarausalSizeMediumOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.components-resizable-box__container.has-show-handle.is-selected > div > div > div > div > div > div.responsive-block-editor-addons-gallery--item.is-selected:first-child'))->getCSSValue('width');
            });

        $stringMedium=$imageSliderImageCarausalSizeMediumOnPage;
        $mediumWidth=substr($stringMedium, 0, -2);
        $mediumWidthNumber=number_format($mediumWidth);

        $I->click($blockEditorAdOns->imageSliderImageCarausalSizeLarge);

        $imageSliderImageCarausalSizeLargeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.components-resizable-box__container.has-show-handle.is-selected > div > div > div > div > div > div.responsive-block-editor-addons-gallery--item.is-selected:first-child'))->getCSSValue('width');
            });

        $string=$imageSliderImageCarausalSizeLargeOnPage;
        $largeWidth=substr($string, 0, -2);
        $largeWidthNumber=number_format($largeWidth);

        $I->click($blockEditorAdOns->imageSliderImageCarausalSizeSmall);

        $imageSliderImageCarausalSizeSmallOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.components-resizable-box__container.has-show-handle.is-selected > div > div > div > div > div > div.responsive-block-editor-addons-gallery--item.is-selected:first-child'))->getCSSValue('width');
            });

        $string=$imageSliderImageCarausalSizeSmallOnPage;
        $smallWidth=substr($string, 0, -2);
        $smallWidthNumber=number_format($smallWidth);

        $I->click($blockEditorAdOns->imageSliderImageCarausalSizeLarge);

        $imageSliderImageCarausalSizeExtraLargeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('div.components-resizable-box__container.has-show-handle.is-selected > div > div > div > div > div > div.responsive-block-editor-addons-gallery--item.is-selected:first-child'))->getCSSValue('width');
            });

        $string=$imageSliderImageCarausalSizeExtraLargeOnPage;
        $extraLargeWidth=substr($string, 0, -2);
        $extraLargeWidthNumber=number_format($extraLargeWidth);


        $smallMedium=$smallWidthNumber<=$mediumWidthNumber;
        $MediumLarge=$mediumWidthNumber<=$largeWidthNumber;
        $largeExtraLarge=$mediumWidthNumber<=$largeWidthNumber;
        $I->assertEquals(1,$smallMedium);
        $I->assertEquals(1,$MediumLarge);
        $I->assertEquals(1,$largeExtraLarge);

    }
*/
}