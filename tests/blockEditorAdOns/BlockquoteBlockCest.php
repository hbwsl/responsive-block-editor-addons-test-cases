<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;

class BlockquoteBlockCest
{/*
    public function ChangesMadeInFontSizeShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $blockquoteGeneralFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('22px', $blockquoteGeneralFontSizeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontSizeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-size');
            });
        $I->assertEquals('22px', $blockquoteGeneralFontSizeOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontSizeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-size');
            });
        $I->assertEquals('22px', $blockquoteGeneralFontSizeOnPage);

        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralFontSize,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInFontWeightShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                       Page\LoginPage $loginPage,
                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight100);

        $blockquoteGeneralFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $blockquoteGeneralFontWeight100OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight100OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $blockquoteGeneralFontWeight100OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight100OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('100', $blockquoteGeneralFontWeight100OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight200);

        $blockquoteGeneralFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $blockquoteGeneralFontWeight200OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight200OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $blockquoteGeneralFontWeight200OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight200OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('200', $blockquoteGeneralFontWeight200OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight300);

        $blockquoteGeneralFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $blockquoteGeneralFontWeight300OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight300OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $blockquoteGeneralFontWeight300OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight300OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('300', $blockquoteGeneralFontWeight300OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight400);

        $blockquoteGeneralFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $blockquoteGeneralFontWeight400OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight400OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $blockquoteGeneralFontWeight400OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight400OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('400', $blockquoteGeneralFontWeight400OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight500);

        $blockquoteGeneralFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $blockquoteGeneralFontWeight500OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight500OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $blockquoteGeneralFontWeight500OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight500OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('500', $blockquoteGeneralFontWeight500OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight600);

        $blockquoteGeneralFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $blockquoteGeneralFontWeight600OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight600OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $blockquoteGeneralFontWeight600OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight600OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('600', $blockquoteGeneralFontWeight600OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight700);

        $blockquoteGeneralFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $blockquoteGeneralFontWeight700OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight700OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $blockquoteGeneralFontWeight700OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight700OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('700', $blockquoteGeneralFontWeight700OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight800);

        $blockquoteGeneralFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $blockquoteGeneralFontWeight800OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight800OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $blockquoteGeneralFontWeight800OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight800OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('800', $blockquoteGeneralFontWeight800OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight900);

        $blockquoteGeneralFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $blockquoteGeneralFontWeight900OnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralFontWeight900OnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $blockquoteGeneralFontWeight900OnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralFontWeight900OnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('font-weight');
            });
        $I->assertEquals('900', $blockquoteGeneralFontWeight900OnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralFontWeight400);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ChangesMadeInLineHeightShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $blockquoteGeneralLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('90px', $blockquoteGeneralLineHeightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralLineHeightOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('line-height');
            });
        $I->assertEquals('90px', $blockquoteGeneralLineHeightOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralLineHeightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('line-height');
            });
        $I->assertEquals('90px', $blockquoteGeneralLineHeightOnPage);

        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteGeneralLineHeight,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInFontAlignmentShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                       Page\LoginPage $loginPage,
                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralAlignmentLeft);

        $blockquoteGeneralAlignmentLeftOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $blockquoteGeneralAlignmentLeftOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralAlignmentLeftOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $blockquoteGeneralAlignmentLeftOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralAlignmentLeftOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('text-align');
            });
        $I->assertEquals('left', $blockquoteGeneralAlignmentLeftOnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralAlignmentCenter);

        $blockquoteGeneralAlignmentCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $blockquoteGeneralAlignmentCenterOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralAlignmentCenterOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $blockquoteGeneralAlignmentCenterOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralAlignmentCenterOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('text-align');
            });
        $I->assertEquals('center', $blockquoteGeneralAlignmentCenterOnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralAlignmentRight);

        $blockquoteGeneralAlignmentRightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $blockquoteGeneralAlignmentRightOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteGeneralAlignmentRightOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $blockquoteGeneralAlignmentRightOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteGeneralAlignmentRightOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('text-align');
            });
        $I->assertEquals('right', $blockquoteGeneralAlignmentRightOnPage);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteGeneralBtn);
        $I->click($blockEditorAdOns->blockquoteGeneralAlignmentLeft);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ChangesMadeInTextColorShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                      Page\LoginPage $loginPage,
                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteColorSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteTextColorRedSelect);
        $blockquoteTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $blockquoteTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteTextColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-block-blockquote-text'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $blockquoteTextColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);

        $blockquoteTextColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-block-blockquote-text.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $blockquoteTextColorRedSelectOnPage);

        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteColorSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteTextColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInPaddingsShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                     Page\LoginPage $loginPage,
                                                                     Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteSpacingBtn);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->blockquoteRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $blockquotePaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('padding');
            });
        $I->assertEquals('74px 63px 74px 64px', $blockquotePaddingOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteTopPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('padding-bottom');
            });
        $I->assertEquals('74px', $blockquoteTopPaddingOnFrontEnd);

        $blockquoteTopPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('padding-top');
            });
        $I->assertEquals('74px', $blockquoteTopPaddingOnFrontEnd);

        $blockquoteTopPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('padding-right');
            });
        $I->assertEquals('63px', $blockquoteTopPaddingOnFrontEnd);

        $blockquoteTopPaddingOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('padding-left');
            });
        $I->assertEquals('64px', $blockquoteTopPaddingOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquotePaddingOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('padding');
            });
        $I->assertEquals('74px 63px 74px 64px', $blockquotePaddingOnPage);

        $I->click($blockEditorAdOns->blockquoteSpacingBtn);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteTopPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBottomPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteLeftPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteRightPadding,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToSolidShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleSolid);

        $blockquoteBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $blockquoteBorderStyleSolidOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleSolidOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $blockquoteBorderStyleSolidOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleSolidOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('solid', $blockquoteBorderStyleSolidOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToDottedShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleDotted);

        $blockquoteBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $blockquoteBorderStyleDottedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleDottedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $blockquoteBorderStyleDottedOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dotted', $blockquoteBorderStyleDottedOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToDashedShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleDashed);

        $blockquoteBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $blockquoteBorderStyleDottedOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleDottedOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $blockquoteBorderStyleDottedOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleDottedOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('dashed', $blockquoteBorderStyleDottedOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToDoubleShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleDouble);

        $blockquoteBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $blockquoteBorderStyleDoubleOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleDoubleOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $blockquoteBorderStyleDoubleOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleDoubleOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('double', $blockquoteBorderStyleDoubleOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToGrooveShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleGroove);

        $blockquoteBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $blockquoteBorderStyleGrooveOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleGrooveOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $blockquoteBorderStyleGrooveOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleGrooveOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('groove', $blockquoteBorderStyleGrooveOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToInsetShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleInset);

        $blockquoteBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $blockquoteBorderStyleInsetOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleInsetOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $blockquoteBorderStyleInsetOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleInsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('inset', $blockquoteBorderStyleInsetOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToOutsetShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleOutset);

        $blockquoteBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $blockquoteBorderStyleOutsetOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleOutsetOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $blockquoteBorderStyleOutsetOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleOutsetOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('outset', $blockquoteBorderStyleOutsetOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToRidgeShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                          Page\LoginPage $loginPage,
                                                                          Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleRidge);

        $blockquoteBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $blockquoteBorderStyleRidgeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleRidgeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $blockquoteBorderStyleRidgeOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $blockquoteBorderStyleRidgeOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function BorderStyleChangedToRidgeShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleRidge);

        $blockquoteBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $blockquoteBorderStyleRidgeOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderStyleRidgeOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $blockquoteBorderStyleRidgeOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderStyleRidgeOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-style');
            });
        $I->assertEquals('ridge', $blockquoteBorderStyleRidgeOnPage);

        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInLineHeightShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                       Page\LoginPage $loginPage,
                                                                       Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleSolid);

        $I->pressKey($blockEditorAdOns->blockquoteBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBorderWidth,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $blockquoteBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('5px', $blockquoteBorderWidthOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderWidthOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.right.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-width');
            });
        $I->assertEquals('5px', $blockquoteBorderWidthOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderWidthOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-width');
            });
        $I->assertEquals('5px', $blockquoteBorderWidthOnPage);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderWidthReset);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function ChangesMadeInBorderColorShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                        Page\LoginPage $loginPage,
                                                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleSolid);
        $I->click($blockEditorAdOns->blockquoteBorderColorRedSelect);

        $blockquoteBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $blockquoteBorderColorRedSelectOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderColorRedSelectOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.left-aligned.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $blockquoteBorderColorRedSelectOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderColorRedSelectOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-color');
            });
        $I->assertEquals('rgb(205, 38, 83)', $blockquoteBorderColorRedSelectOnPage);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderColorClear);
        $I->click($blockEditorAdOns->updateBtn);
    }
*/
    public function ChangesMadeInBorderRadiusShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        $I->click($blockEditorAdOns->blockquoteBorderStyleSolid);

        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        //$I->wait(4);
        $blockquoteBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $blockquoteBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderRadiusOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.left-aligned.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $blockquoteBorderRadiusOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('border-radius');
            });
        $I->assertEquals('4px', $blockquoteBorderRadiusOnPage);
        $I->click($blockEditorAdOns->blockquoteBorderSettingsBtn);
        //$I->wait(4);
//        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
//        $I->pressKey($blockEditorAdOns->blockquoteBorderRadius, \Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->click($blockEditorAdOns->blockquoteBorderRadiusResetButton);
        $I->click($blockEditorAdOns->blockquoteBorderStyleNone);
        $I->click($blockEditorAdOns->updateBtn);
    }
/*
    public function IfBackgroundTypeIsChangesToColorThenItShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                                      Page\LoginPage $loginPage,
                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBackgroundBtn);
        $I->click($blockEditorAdOns->blockquoteBackgroundTypeColor);
        $I->click($blockEditorAdOns->blockquoteBackgroundColorBrownSelect);

        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $blockquoteBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(220, 215, 202, 0.92)', $blockquoteBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderRadiusOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.left-aligned.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(220, 215, 202, 0.92)', $blockquoteBorderRadiusOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(220, 215, 202, 0.92)', $blockquoteBorderRadiusOnPage);
        $I->click($blockEditorAdOns->blockquoteBackgroundBtn);
        $I->click($blockEditorAdOns->blockquoteBackgroundTypeColor);
        $I->click($blockEditorAdOns->blockquoteBackgroundColorClearBtn);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        //$I->click($blockEditorAdOns->blockquoteBackgroundOpacityReset);
        $I->click($blockEditorAdOns->updateBtn);
    }

    public function IfBackgroundTypeIsChangesToColorThenItShouldAsloRefelectInFontEnd(AcceptanceTester $I,
                                                                                      Page\LoginPage $loginPage,
                                                                                      Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->waitForElement($blockEditorAdOns->blockquoteClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);
        $I->click($blockEditorAdOns->blockquoteBackgroundBtn);
        $I->click($blockEditorAdOns->blockquoteBackgroundTypeColor);
        $I->click($blockEditorAdOns->blockquoteBackgroundColorBrownSelect);

        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

        $blockquoteBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(220, 215, 202, 0.92)', $blockquoteBorderRadiusOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->blockquotePage, 20);
        $I->click($blockEditorAdOns->blockquotePage);
        $I->wait(3);

        $blockquoteBorderRadiusOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.wp-block-responsive-block-editor-addons-blockquote.left-aligned.responsive-block-editor-addons-font-size-18.responsive-block-editor-addons-block-quote'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(220, 215, 202, 0.92)', $blockquoteBorderRadiusOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->blockquoteSelectClass, 20);
        $I->click($blockEditorAdOns->blockquoteSelectClass);

        $blockquoteBorderRadiusOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.block-editor-block-list__block.wp-block.is-selected.wp-block > div:first-child'))->getCSSValue('background-color');
            });
        $I->assertEquals('rgba(220, 215, 202, 0.92)', $blockquoteBorderRadiusOnPage);
        $I->click($blockEditorAdOns->blockquoteBackgroundBtn);
        $I->click($blockEditorAdOns->blockquoteBackgroundTypeColor);
        $I->click($blockEditorAdOns->blockquoteBackgroundColorClearBtn);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->blockquoteBackgroundOpacity,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        //$I->click($blockEditorAdOns->blockquoteBackgroundOpacityReset);
        $I->click($blockEditorAdOns->updateBtn);
    }
    */
}