<?php

use Facebook\WebDriver\WebDriverBy;
use Codeception\Module\Assert;
class ExpandAddOnCest
{
  /*  public function GeneralSettingsTest(AcceptanceTester $I,
                                        Page\LoginPage $loginPage,
                                        Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->expandPage, 20);
        $I->click($blockEditorAdOns->expandPage);
        $I->waitForElement($blockEditorAdOns->expandPageSomeShortTextClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor,20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);
        $I->click($blockEditorAdOns->expandSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandGenaralBtn);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandGeneralTitleToggleBtn);

        $I->dontSeeElement($blockEditorAdOns->expandTitleBlockClassSelector);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->expandPage, 20);
        $I->click($blockEditorAdOns->expandPage);
        $I->waitForElement($blockEditorAdOns->expandPageSomeShortTextClass);
        $I->wait(4);

        $I->dontSeeElement($blockEditorAdOns->expandTitleBlockClassSelector);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);

        $I->dontSeeElement($blockEditorAdOns->expandTitleBlockClassSelector);

        $I->click($blockEditorAdOns->expandSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandGenaralBtn);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandGeneralTitleToggleBtn);

        $I->SeeElement($blockEditorAdOns->expandTitleBlockClassSelector);
        $I->click($blockEditorAdOns->updateBtn);
    }


    public function SpacingGivenByUserShouldAlsoReftectedAtFrontEnd(AcceptanceTester $I,
                                                                    Page\LoginPage $loginPage,
                                                                    Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->expandPage, 20);
        $I->click($blockEditorAdOns->expandPage);
        $I->waitForElement($blockEditorAdOns->expandPageSomeShortTextClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);
        $I->click($blockEditorAdOns->expandSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandSpacingBtn);

        $I->pressKey($blockEditorAdOns->expandSpacingTitleMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->expandSpacingTitleMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->expandSpacingTextMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->expandSpacingTextMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $I->pressKey($blockEditorAdOns->expandSpacingLinkMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);
        $I->pressKey($blockEditorAdOns->expandSpacingLinkMarginBottom,\Facebook\WebDriver\WebDriverKeys::ARROW_UP);

        $titleMarginBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('30px', $titleMarginBottomOnPage);

        $textMarginBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-less-text.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('22px', $textMarginBottomOnPage);

        $linkMarginBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-more-toggle-text.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('20px', $linkMarginBottomOnPage);

        $I->click($blockEditorAdOns->updateBtn);
        $I->wait(4);

        $I->amOnPage('/');
        $I->waitForElement($blockEditorAdOns->expandPage, 20);
        $I->click($blockEditorAdOns->expandPage);
        $I->waitForElement($blockEditorAdOns->expandPageSomeShortTextClass);
        $I->wait(4);

        $titleMarginBottomOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-expand-title'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('30px', $titleMarginBottomOnFrontEnd);

        $textMarginBottomOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-expand-less-text'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('22px', $textMarginBottomOnFrontEnd);

        $linkMarginBottomOnFrontEnd = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.responsive-block-editor-addons-expand-more-toggle-text'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('20px', $linkMarginBottomOnFrontEnd);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);
        $I->reloadPage();
        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);

        $titleMarginBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-title.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('30px', $titleMarginBottomOnPage);

        $textMarginBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-less-text.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('22px', $textMarginBottomOnPage);

        $linkMarginBottomOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-more-toggle-text.keep-placeholder-on-focus'))->getCSSValue('margin-bottom');
            });
        $I->assertEquals('20px', $linkMarginBottomOnPage);

        $I->click($blockEditorAdOns->expandSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandSpacingBtn);

        $I->click($blockEditorAdOns->expandSpacingTitleMarginResetBottom);
        $I->wait(1);
        $I->click($blockEditorAdOns->expandSpacingTextMarginResetBottom);
        $I->wait(1);
        $I->click($blockEditorAdOns->expandSpacingLinkMarginResetBottom);
        $I->click($blockEditorAdOns->updateBtn);

    }

    public function ColorSettingGivenByUserShouldAlsoReftectedAtFrontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->expandPage, 20);
        $I->click($blockEditorAdOns->expandPage);
        $I->waitForElement($blockEditorAdOns->expandPageSomeShortTextClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);
        $I->click($blockEditorAdOns->expandSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandColorSettingBtn);

        $I->click($blockEditorAdOns->expandColorSettingTextColorRedSelect);
        $I->click($blockEditorAdOns->expandColorSettingLinkColorBrownSelect);
        $I->click($blockEditorAdOns->expandColorSettingTitleColorBlackSelect);

        $textColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-less-text.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(205, 38, 83, 1)', $textColorOnPage);

        $linkColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-more-toggle-text.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgba(109, 109, 109, 1)', $linkColorOnPage);

        $titleColorOnPage = $I->executeInSelenium(
            function (\Facebook\WebDriver\Remote\RemoteWebDriver $webdriver) {
                return $webdriver->findElement(WebDriverBy::cssSelector('.rich-text.block-editor-rich-text__editable.responsive-block-editor-addons-expand-title.keep-placeholder-on-focus'))->getCSSValue('color');
            });
        $I->assertEquals('rgb(0, 0, 0, 1)', $titleColorOnPage);

    }
*/
    public function ColorSettingGivenByUserShouldAlsoReftectedAtFrontEnd(AcceptanceTester $I,
                                                                         Page\LoginPage $loginPage,
                                                                         Page\BlockEditorAdOns $blockEditorAdOns)
    {
        $loginPage->userLogin($I);

        $I->waitForElement($blockEditorAdOns->expandPage, 20);
        $I->click($blockEditorAdOns->expandPage);
        $I->waitForElement($blockEditorAdOns->expandPageSomeShortTextClass);
        $I->wait(3);

        $I->click($blockEditorAdOns->editPageLink);
        $I->waitForElement($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor, 20);
        $I->click($blockEditorAdOns->cancelBtnForWelcomeToBlockEditor);

        $I->waitForElement($blockEditorAdOns->expandSelectClass, 20);
        $I->click($blockEditorAdOns->expandSelectClass);
        $I->wait(2);
        $I->click($blockEditorAdOns->expandTypographyBtn);

    }
}